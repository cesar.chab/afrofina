-- phpMyAdmin SQL Dump
-- version 4.9.5
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost:3306
-- Tiempo de generación: 27-03-2021 a las 14:06:10
-- Versión del servidor: 10.3.28-MariaDB-log-cll-lve
-- Versión de PHP: 7.3.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `afroimyf_peges`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `app_settings`
--

CREATE TABLE `app_settings` (
  `id` int(10) UNSIGNED NOT NULL,
  `key` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `app_settings`
--

INSERT INTO `app_settings` (`id`, `key`, `value`) VALUES
(7, 'date_format', 'l jS F Y (H:i:s)'),
(8, 'language', 'es'),
(17, 'is_human_date_format', '1'),
(18, 'app_name', 'Afro Eat'),
(19, 'app_short_description', 'Manage Mobile Application'),
(20, 'mail_driver', 'smtp'),
(21, 'mail_host', 'smtp.hostinger.com'),
(22, 'mail_port', '587'),
(23, 'mail_username', 'productdelivery@smartersvision.com'),
(24, 'mail_password', 'NnvAwk&&E7'),
(25, 'mail_encryption', 'ssl'),
(26, 'mail_from_address', 'productdelivery@smartersvision.com'),
(27, 'mail_from_name', 'Smarter Vision'),
(30, 'timezone', 'America/Montserrat'),
(32, 'theme_contrast', 'light'),
(33, 'theme_color', 'success'),
(34, 'app_logo', '2c969be4-868f-4d13-859b-cdff00472cb4'),
(35, 'nav_color', 'navbar-dark bg-success'),
(38, 'logo_bg_color', 'bg-white'),
(66, 'default_role', 'admin'),
(68, 'facebook_app_id', '518416208939727'),
(69, 'facebook_app_secret', '93649810f78fa9ca0d48972fee2a75cd'),
(71, 'twitter_app_id', 'twitter'),
(72, 'twitter_app_secret', 'twitter 1'),
(74, 'google_app_id', '527129559488-roolg8aq110p8r1q952fqa9tm06gbloe.apps.googleusercontent.com'),
(75, 'google_app_secret', 'FpIi8SLgc69ZWodk-xHaOrxn'),
(77, 'enable_google', '1'),
(78, 'enable_facebook', '1'),
(93, 'enable_stripe', '1'),
(94, 'stripe_key', 'pk_live_51Hh4fGFsZTRg2tXvovaEMxS6dgDbkeqt0LM083AyYXCtFBiZv9r1UM36vKsKmtEqaFwTXor4fwTAVB55b1uGtzGh00TvFVhz9r'),
(95, 'stripe_secret', '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'),
(101, 'custom_field_models.0', 'App\\Models\\User'),
(104, 'default_tax', '0'),
(107, 'default_currency', '$'),
(108, 'fixed_header', '0'),
(109, 'fixed_footer', '0'),
(110, 'fcm_key', 'AAAA-BQwryI:APA91bGuPnmZq98ONcvNxckpLGyyE_fwvT1gbm6S2-so8ZT2DQGRpOlBaIFfRIVyvoqun_-euff-djAoewMtwStZRSa3XaWMwRwhkRYP27X5ZVM-CfEM0Nv2dUyZxPs3xgxh8l1aOesb'),
(111, 'enable_notifications', '1'),
(112, 'paypal_username', 'info.afrofina@gmail.com'),
(113, 'paypal_password', 'AfroBusiness112!'),
(114, 'paypal_secret', 'EFRDJYVVhnQ_cslaKx4ofEMr77NBE8Caq9E8QqV21O2mRqIfsrq0pzGhZTSiXsBHqYopA2tzT9EESgBF'),
(115, 'enable_paypal', '1'),
(116, 'main_color', '#28a745'),
(117, 'main_dark_color', '#28a745'),
(118, 'second_color', '#043832'),
(119, 'second_dark_color', '#ccccdd'),
(120, 'accent_color', '#8c98a8'),
(121, 'accent_dark_color', '#9999aa'),
(122, 'scaffold_dark_color', '#2c2c2c'),
(123, 'scaffold_color', '#fafafa'),
(124, 'google_maps_key', 'AIzaSyAkbeJt8YkyCJAU0zrs1QODGk3vfbinvd4'),
(125, 'mobile_language', 'en'),
(126, 'app_version', '1.9.0'),
(127, 'enable_version', '1'),
(128, 'default_currency_id', '1'),
(129, 'default_currency_code', 'USD'),
(130, 'default_currency_decimal_digits', '2'),
(131, 'default_currency_rounding', '0'),
(132, 'currency_right', '0'),
(170, 'paypal_app_id', '0'),
(169, 'paypal_mode', '1'),
(168, 'home_section_12', 'recent_reviews'),
(167, 'home_section_11', 'recent_reviews_heading'),
(166, 'home_section_10', 'popular'),
(165, 'home_section_9', 'popular_heading'),
(164, 'home_section_8', 'categories'),
(163, 'home_section_7', 'categories_heading'),
(162, 'home_section_6', 'trending_week'),
(161, 'home_section_5', 'trending_week_heading'),
(160, 'home_section_4', 'top_markets'),
(159, 'home_section_3', 'top_markets_heading'),
(158, 'home_section_2', 'slider'),
(157, 'home_section_1', 'search'),
(171, 'firebase_api_key', 'AIzaSyATYWohLtfcb8nqNgZspQ1JdvcInTuGjuQ'),
(172, 'firebase_auth_domain', 'deliveryfood-1bfce.firebaseapp.com'),
(173, 'firebase_database_url', 'https://deliveryfood-1bfce.firebaseio.com'),
(174, 'firebase_project_id', 'deliveryfood-1bfce'),
(175, 'firebase_storage_bucket', 'deliveryfood-1bfce.appspot.com'),
(176, 'firebase_messaging_sender_id', '1065490624290'),
(177, 'firebase_app_id', '1:1065490624290:web:bd445351d03dadec3cff7b'),
(178, 'firebase_measurement_id', 'G-R9GC112JFE'),
(179, 'distance_unit', 'km');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `carts`
--

CREATE TABLE `carts` (
  `id` int(10) UNSIGNED NOT NULL,
  `product_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `quantity` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `carts`
--

INSERT INTO `carts` (`id`, `product_id`, `user_id`, `quantity`, `created_at`, `updated_at`) VALUES
(28, 0, 0, 0, '2021-01-11 21:36:26', '2021-01-11 21:36:26'),
(38, 28, 3, 2, '2021-01-15 15:40:44', '2021-01-18 11:03:31'),
(29, 0, 0, 0, '2021-01-11 21:39:34', '2021-01-11 21:39:34'),
(40, 41, 20, 1, '2021-03-24 11:53:01', '2021-03-24 11:53:01');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cart_options`
--

CREATE TABLE `cart_options` (
  `option_id` int(10) UNSIGNED NOT NULL,
  `cart_id` int(10) UNSIGNED NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categories`
--

CREATE TABLE `categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(127) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `categories`
--

INSERT INTO `categories` (`id`, `name`, `description`, `created_at`, `updated_at`) VALUES
(1, 'Fruits', '<span style=\"color: rgb(77, 81, 86); font-family: arial, sans-serif; font-size: 14px;\">Eating&nbsp;</span><span style=\"font-weight: bold; color: rgb(95, 99, 104); font-family: arial, sans-serif; font-size: 14px;\">fruit</span><span style=\"color: rgb(77, 81, 86); font-family: arial, sans-serif; font-size: 14px;\">&nbsp;on a regular basis can boost health. However, not all&nbsp;</span><span style=\"font-weight: bold; color: rgb(95, 99, 104); font-family: arial, sans-serif; font-size: 14px;\">fruits</span><span style=\"color: rgb(77, 81, 86); font-family: arial, sans-serif; font-size: 14px;\">&nbsp;are created equal. Some of them provide unique health benefits.</span>', '2020-11-07 23:48:19', '2020-11-08 18:47:34'),
(2, 'Foods', '<span style=\"color: rgb(77, 81, 86); font-family: arial, sans-serif; font-size: 14px;\">The best&nbsp;</span><span style=\"font-weight: bold; color: rgb(95, 99, 104); font-family: arial, sans-serif; font-size: 14px;\">restaurants</span><span style=\"color: rgb(77, 81, 86); font-family: arial, sans-serif; font-size: 14px;\">&nbsp;for breakfast, lunch, dinner, snacks, and traditional dining. Discover the best places to eat&nbsp;</span>', '2020-11-07 23:48:19', '2020-11-08 18:37:29'),
(3, 'Groceries', '<span style=\"color: rgb(77, 81, 86); font-family: arial, sans-serif; font-size: 14px;\"> the food and other items that you buy in a food store or supermarket</span>', '2020-11-07 23:48:19', '2020-11-08 18:47:49'),
(4, 'Drinks', '<span style=\"color: rgb(77, 81, 86); font-family: arial, sans-serif; font-size: 14px;\">Margarita with Blood Orange&nbsp;</span><span style=\"font-weight: bold; color: rgb(95, 99, 104); font-family: arial, sans-serif; font-size: 14px;\">Juice</span><span style=\"color: rgb(77, 81, 86); font-family: arial, sans-serif; font-size: 14px;\">. withsaltandwit.com. Vodka Martini with Blueberry&nbsp;</span><span style=\"font-weight: bold; color: rgb(95, 99, 104); font-family: arial, sans-serif; font-size: 14px;\">Juice so much more&nbsp;</span>', '2020-11-07 23:48:19', '2020-11-08 18:41:07'),
(6, 'Vegetables', '<span style=\"color: rgb(77, 81, 86); font-family: arial, sans-serif; font-size: 14px;\">Most&nbsp;</span><span style=\"font-weight: bold; color: rgb(95, 99, 104); font-family: arial, sans-serif; font-size: 14px;\">vegetables</span><span style=\"color: rgb(77, 81, 86); font-family: arial, sans-serif; font-size: 14px;\">&nbsp;are naturally low in fat and calories. ·&nbsp;</span><span style=\"font-weight: bold; color: rgb(95, 99, 104); font-family: arial, sans-serif; font-size: 14px;\">Vegetables</span><span style=\"color: rgb(77, 81, 86); font-family: arial, sans-serif; font-size: 14px;\">&nbsp;are important sources of many nutrients, including potassium, dietary fiber, folate etc</span>', '2020-11-07 23:48:19', '2020-11-08 18:49:14');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `coupons`
--

CREATE TABLE `coupons` (
  `id` int(10) UNSIGNED NOT NULL,
  `code` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `discount` double(8,2) NOT NULL DEFAULT 0.00,
  `discount_type` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'percent',
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `currencies`
--

CREATE TABLE `currencies` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(127) COLLATE utf8mb4_unicode_ci NOT NULL,
  `symbol` varchar(63) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code` varchar(5) COLLATE utf8mb4_unicode_ci NOT NULL,
  `decimal_digits` tinyint(3) UNSIGNED DEFAULT NULL,
  `rounding` tinyint(3) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `currencies`
--

INSERT INTO `currencies` (`id`, `name`, `symbol`, `code`, `decimal_digits`, `rounding`, `created_at`, `updated_at`) VALUES
(1, 'US Dollar', '$', 'USD', 2, 0, '2019-10-22 19:50:48', '2019-10-22 19:50:48'),
(2, 'Euro', '€', 'EUR', 2, 0, '2019-10-22 19:51:39', '2019-10-22 19:51:39'),
(3, 'Indian Rupee', 'টকা', 'INR', 2, 0, '2019-10-22 19:52:50', '2019-10-22 19:52:50'),
(4, 'Indonesian Rupiah', 'Rp', 'IDR', 0, 0, '2019-10-22 19:53:22', '2019-10-22 19:53:22'),
(5, 'Brazilian Real', 'R$', 'BRL', 2, 0, '2019-10-22 19:54:00', '2019-10-22 19:54:00'),
(6, 'Cambodian Riel', '៛', 'KHR', 2, 0, '2019-10-22 19:55:51', '2019-10-22 19:55:51'),
(7, 'Vietnamese Dong', '₫', 'VND', 0, 0, '2019-10-22 19:56:26', '2019-10-22 19:56:26');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `custom_fields`
--

CREATE TABLE `custom_fields` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(127) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(56) COLLATE utf8mb4_unicode_ci NOT NULL,
  `values` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `disabled` tinyint(1) DEFAULT NULL,
  `required` tinyint(1) DEFAULT NULL,
  `in_table` tinyint(1) DEFAULT NULL,
  `bootstrap_column` tinyint(4) DEFAULT NULL,
  `order` tinyint(4) DEFAULT NULL,
  `custom_field_model` varchar(127) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `custom_fields`
--

INSERT INTO `custom_fields` (`id`, `name`, `type`, `values`, `disabled`, `required`, `in_table`, `bootstrap_column`, `order`, `custom_field_model`, `created_at`, `updated_at`) VALUES
(4, 'phone', 'text', NULL, 0, 0, 0, 6, 2, 'App\\Models\\User', '2019-09-07 01:30:00', '2019-09-07 01:31:47'),
(5, 'bio', 'textarea', NULL, 0, 0, 0, 6, 1, 'App\\Models\\User', '2019-09-07 01:43:58', '2019-09-07 01:43:58'),
(6, 'address', 'text', NULL, 0, 0, 0, 6, 3, 'App\\Models\\User', '2019-09-07 01:49:22', '2019-09-07 01:49:22');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `custom_field_values`
--

CREATE TABLE `custom_field_values` (
  `id` int(10) UNSIGNED NOT NULL,
  `value` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `view` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `custom_field_id` int(10) UNSIGNED NOT NULL,
  `customizable_type` varchar(127) COLLATE utf8mb4_unicode_ci NOT NULL,
  `customizable_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `custom_field_values`
--

INSERT INTO `custom_field_values` (`id`, `value`, `view`, `custom_field_id`, `customizable_type`, `customizable_id`, `created_at`, `updated_at`) VALUES
(29, '+136 226 5669', '+136 226 5669', 4, 'App\\Models\\User', 2, '2019-09-07 01:52:30', '2019-09-07 01:52:30'),
(30, 'Lobortis mattis aliquam faucibus purus. Habitasse platea dictumst vestibulum rhoncus est pellentesque elit. Nunc vel risus commodo viverra maecenas accumsan lacus vel.', 'Lobortis mattis aliquam faucibus purus. Habitasse platea dictumst vestibulum rhoncus est pellentesque elit. Nunc vel risus commodo viverra maecenas accumsan lacus vel.', 5, 'App\\Models\\User', 2, '2019-09-07 01:52:30', '2019-10-16 23:32:35'),
(31, '2911 Corpening Drive South Lyon, MI 48178', '2911 Corpening Drive South Lyon, MI 48178', 6, 'App\\Models\\User', 2, '2019-09-07 01:52:30', '2019-10-16 23:32:35'),
(32, '+136 226 5660', '+136 226 5660', 4, 'App\\Models\\User', 1, '2019-09-07 01:53:58', '2019-09-27 12:12:04'),
(33, 'Faucibus ornare suspendisse sed nisi lacus sed. Pellentesque sit amet porttitor eget dolor morbi non arcu. Eu scelerisque felis imperdiet proin fermentum leo vel orci porta', 'Faucibus ornare suspendisse sed nisi lacus sed. Pellentesque sit amet porttitor eget dolor morbi non arcu. Eu scelerisque felis imperdiet proin fermentum leo vel orci porta', 5, 'App\\Models\\User', 1, '2019-09-07 01:53:58', '2019-10-16 23:23:53'),
(34, '569 Braxton Street Cortland, IL 60112', '569 Braxton Street Cortland, IL 60112', 6, 'App\\Models\\User', 1, '2019-09-07 01:53:58', '2019-10-16 23:23:53'),
(35, '+1 098-6543-236', '+1 098-6543-236', 4, 'App\\Models\\User', 3, '2019-10-15 21:21:32', '2019-10-18 03:21:43'),
(36, 'Aliquet porttitor lacus luctus accumsan tortor posuere ac ut. Tortor pretium viverra suspendisse', 'Aliquet porttitor lacus luctus accumsan tortor posuere ac ut. Tortor pretium viverra suspendisse', 5, 'App\\Models\\User', 3, '2019-10-15 21:21:32', '2019-10-18 03:21:12'),
(37, '1850 Big Elm Kansas City, MO 64106', '1850 Big Elm Kansas City, MO 64106', 6, 'App\\Models\\User', 3, '2019-10-15 21:21:32', '2019-10-18 03:21:43'),
(38, '+1 248-437-7610', '+1 248-437-7610', 4, 'App\\Models\\User', 4, '2019-10-16 23:31:46', '2019-10-16 23:31:46'),
(39, 'Faucibus ornare suspendisse sed nisi lacus sed. Pellentesque sit amet porttitor eget dolor morbi non arcu. Eu scelerisque felis imperdiet proin fermentum leo vel orci porta', 'Faucibus ornare suspendisse sed nisi lacus sed. Pellentesque sit amet porttitor eget dolor morbi non arcu. Eu scelerisque felis imperdiet proin fermentum leo vel orci porta', 5, 'App\\Models\\User', 4, '2019-10-16 23:31:46', '2019-10-16 23:31:46'),
(40, '1050 Frosty Lane Sidney, NY 13838', '1050 Frosty Lane Sidney, NY 13838', 6, 'App\\Models\\User', 4, '2019-10-16 23:31:46', '2019-10-16 23:31:46'),
(41, '+136 226 5669', '+136 226 5669', 4, 'App\\Models\\User', 5, '2019-12-15 23:49:44', '2019-12-15 23:49:44'),
(42, '<p>Short Bio</p>', 'Short Bio', 5, 'App\\Models\\User', 5, '2019-12-15 23:49:44', '2019-12-15 23:49:44'),
(43, '4722 Villa Drive', '4722 Villa Drive', 6, 'App\\Models\\User', 5, '2019-12-15 23:49:44', '2019-12-15 23:49:44'),
(44, '+136 955 6525', '+136 955 6525', 4, 'App\\Models\\User', 6, '2020-03-29 21:28:04', '2020-03-29 21:28:04'),
(45, '<p>Short bio for this driver</p>', 'Short bio for this driver', 5, 'App\\Models\\User', 6, '2020-03-29 21:28:05', '2020-03-29 21:28:05'),
(46, '4722 Villa Drive', '4722 Villa Drive', 6, 'App\\Models\\User', 6, '2020-03-29 21:28:05', '2020-03-29 21:28:05'),
(47, '5138579509', '5138579509', 4, 'App\\Models\\User', 9, '2020-11-10 03:34:51', '2020-11-10 03:35:38'),
(48, NULL, '', 5, 'App\\Models\\User', 9, '2020-11-10 03:34:51', '2020-11-10 03:34:51'),
(49, '45 senate drive', '45 senate drive', 6, 'App\\Models\\User', 9, '2020-11-10 03:34:51', '2020-11-10 03:35:38'),
(50, '0701 243 1929', '0701 243 1929', 4, 'App\\Models\\User', 14, '2021-02-01 05:17:01', '2021-02-01 05:17:01'),
(51, 'trader', 'trader', 5, 'App\\Models\\User', 14, '2021-02-01 05:17:01', '2021-02-01 05:17:01'),
(52, 'bishop Johnson street', 'bishop Johnson street', 6, 'App\\Models\\User', 14, '2021-02-01 05:17:01', '2021-02-01 05:17:01'),
(53, '4849950728', '4849950728', 4, 'App\\Models\\User', 15, '2021-02-08 05:21:42', '2021-02-08 05:21:42'),
(54, '<p>Tracy Jacobs currently live in PA within the Delaware county area.&nbsp;</p>', 'Tracy Jacobs currently live in PA within the Delaware county area.&nbsp;', 5, 'App\\Models\\User', 15, '2021-02-08 05:21:42', '2021-02-08 05:21:42'),
(55, '7005 Cleveland Rd Upper Darby Pa 19082', '7005 Cleveland Rd Upper Darby Pa 19082', 6, 'App\\Models\\User', 15, '2021-02-08 05:21:42', '2021-02-08 05:21:42'),
(56, '215-487-8474', '215-487-8474', 4, 'App\\Models\\User', 16, '2021-02-08 05:45:07', '2021-02-08 05:45:07'),
(57, '<p>Owner of Bindu Market location in South West Philadelphia&nbsp;</p>', 'Owner of Bindu Market location in South West Philadelphia&nbsp;', 5, 'App\\Models\\User', 16, '2021-02-08 05:45:07', '2021-02-08 05:45:07'),
(58, '1948 S. 65TH Street Philadelphia pa 19142', '1948 S. 65TH Street Philadelphia pa 19142', 6, 'App\\Models\\User', 16, '2021-02-08 05:45:07', '2021-02-08 05:45:07'),
(59, '267-210-0240', '267-210-0240', 4, 'App\\Models\\User', 17, '2021-02-16 08:58:29', '2021-02-16 08:58:29'),
(60, NULL, '', 5, 'App\\Models\\User', 17, '2021-02-16 08:58:29', '2021-02-16 08:58:29'),
(61, '7282 Woodland Ave, Philadelphia Pa 19142', '7282 Woodland Ave, Philadelphia Pa 19142', 6, 'App\\Models\\User', 17, '2021-02-16 08:58:29', '2021-02-16 08:58:29'),
(62, '4847643762', '4847643762', 4, 'App\\Models\\User', 18, '2021-02-22 01:38:05', '2021-02-22 01:38:05'),
(63, NULL, '', 5, 'App\\Models\\User', 18, '2021-02-22 01:38:05', '2021-02-22 01:38:05'),
(64, NULL, NULL, 6, 'App\\Models\\User', 18, '2021-02-22 01:38:05', '2021-02-22 01:38:05'),
(65, '0245976497', '0245976497', 4, 'App\\Models\\User', 20, '2021-03-24 11:53:45', '2021-03-24 11:53:45'),
(66, 'mecccccccccccc', 'mecccccccccccc', 5, 'App\\Models\\User', 20, '2021-03-24 11:53:45', '2021-03-24 11:53:45'),
(67, 'accra', 'accra', 6, 'App\\Models\\User', 20, '2021-03-24 11:53:45', '2021-03-24 11:53:45'),
(68, '+233240526207', '+233240526207', 4, 'App\\Models\\User', 19, '2021-03-25 01:30:34', '2021-03-25 01:30:34'),
(69, 'student', 'student', 5, 'App\\Models\\User', 19, '2021-03-25 01:30:34', '2021-03-25 01:30:34'),
(70, 'Dansoman', 'Dansoman', 6, 'App\\Models\\User', 19, '2021-03-25 01:30:34', '2021-03-25 01:30:34'),
(71, '484-326-7243', '484-326-7243', 4, 'App\\Models\\User', 22, '2021-03-25 01:35:22', '2021-03-25 01:35:22'),
(72, 'I am shopper.', 'I am shopper.', 5, 'App\\Models\\User', 22, '2021-03-25 01:35:22', '2021-03-25 01:35:22'),
(73, '2030 S. 65th Street Philadelphia Pa 19142', '2030 S. 65th Street Philadelphia Pa 19142', 6, 'App\\Models\\User', 22, '2021-03-25 01:35:22', '2021-03-25 01:35:22');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `delivery_addresses`
--

CREATE TABLE `delivery_addresses` (
  `id` int(10) UNSIGNED NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `latitude` varchar(24) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `longitude` varchar(24) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_default` tinyint(1) DEFAULT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `delivery_addresses`
--

INSERT INTO `delivery_addresses` (`id`, `description`, `address`, `latitude`, `longitude`, `is_default`, `user_id`, `created_at`, `updated_at`) VALUES
(2, 'Suscipit libero mollitia dicta omnis quos incidunt.', '2565 Goyette Junctions\nProhaskamouth, AL 44810-7050', '14.670474', '-123.014265', 1, 5, '2020-11-07 23:48:19', '2020-11-07 23:48:19'),
(3, 'Sunt ex amet veniam odio optio aspernatur sit dolorem.', '59492 Heber Mount Suite 963\nBrakusview, OK 80324', '-64.890648', '-117.666045', 0, 2, '2020-11-07 23:48:19', '2020-11-07 23:48:19'),
(4, 'Molestias ipsam et impedit dicta dolorum magnam.', '4322 Lenny Creek\nLubowitzside, HI 78622', '68.156849', '73.731127', 0, 6, '2020-11-07 23:48:19', '2020-11-07 23:48:19'),
(5, 'Expedita nemo aliquam vitae doloribus quisquam non.', '4092 Kurt Ridge\nSouth Nelson, VT 01697-3200', '85.297592', '49.505005', 1, 2, '2020-11-07 23:48:19', '2020-11-07 23:48:19'),
(6, 'Autem ut nulla minus vero totam.', '169 Gabriel Stream Apt. 648\nEast Corbin, MT 33994', '-42.818148', '-0.612499', 1, 6, '2020-11-07 23:48:19', '2020-11-07 23:48:19'),
(7, 'Necessitatibus est iure quia reprehenderit sint doloribus aut.', '667 Johnson Crest\nPort Camren, WY 36804-9667', '28.677563', '-38.570805', 1, 5, '2020-11-07 23:48:19', '2020-11-07 23:48:19'),
(11, 'Pariatur praesentium omnis excepturi aut et.', '310 Karlie Land Apt. 752\nNorth Lailaville, NV 79953', '-19.796305', '-17.87752', 0, 1, '2020-11-07 23:48:19', '2020-11-07 23:48:19'),
(12, 'Veniam maxime distinctio sit perspiciatis vel.', '77044 Bergnaum Fort Suite 265\nNew Cayla, NE 77726-5976', '-89.942657', '65.159908', 0, 6, '2020-11-07 23:48:19', '2020-11-07 23:48:19'),
(13, 'Possimus nihil impedit repellat eum voluptate.', '3898 Schuster Lodge Apt. 440\nEast Dewayne, LA 50186-5223', '-42.243124', '145.982595', 0, 1, '2020-11-07 23:48:19', '2020-11-07 23:48:19'),
(17, NULL, '4020 Redden Rd, Drexel Hill, PA 19026, USA', '39.93982251399989', '-75.29816597700119', 0, 4, '2020-11-28 03:41:16', '2020-11-28 03:41:16'),
(18, 'home', 'Fourty feet Road, Samejabad, Multan, Punjab, Pakistan', '30.19875380254077', '71.52287498116493', 1, 3, '2021-01-06 14:45:32', '2021-01-10 23:40:03'),
(19, NULL, 'Emiliano Zapata 14, Lomas de San Pablo, 54930 San Pablo de las Salinas, Méx., Mexico', '19.672202674941325', '-99.09308921545744', 0, 3, '2021-01-10 23:56:18', '2021-01-10 23:56:18');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `discountables`
--

CREATE TABLE `discountables` (
  `id` int(10) UNSIGNED NOT NULL,
  `coupon_id` int(10) UNSIGNED NOT NULL,
  `discountable_type` varchar(127) COLLATE utf8mb4_unicode_ci NOT NULL,
  `discountable_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `drivers`
--

CREATE TABLE `drivers` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `delivery_fee` double(5,2) NOT NULL DEFAULT 0.00,
  `total_orders` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `earning` double(9,2) NOT NULL DEFAULT 0.00,
  `available` tinyint(1) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `drivers`
--

INSERT INTO `drivers` (`id`, `user_id`, `delivery_fee`, `total_orders`, `earning`, `available`, `created_at`, `updated_at`) VALUES
(1, 5, 0.00, 1, 0.00, 0, '2020-11-08 23:07:44', '2020-11-28 02:24:11'),
(2, 6, 0.00, 0, 0.00, 0, '2020-11-08 23:07:44', '2020-11-08 23:07:44'),
(3, 15, 0.00, 0, 0.00, 0, '2021-02-08 05:21:42', '2021-02-08 05:21:42'),
(4, 18, 0.00, 0, 0.00, 0, '2021-02-22 01:38:05', '2021-02-22 01:38:05');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `drivers_payouts`
--

CREATE TABLE `drivers_payouts` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `method` varchar(127) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `amount` double(9,2) NOT NULL DEFAULT 0.00,
  `paid_date` datetime DEFAULT NULL,
  `note` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `driver_markets`
--

CREATE TABLE `driver_markets` (
  `user_id` int(10) UNSIGNED NOT NULL,
  `market_id` int(10) UNSIGNED NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `driver_markets`
--

INSERT INTO `driver_markets` (`user_id`, `market_id`) VALUES
(5, 1),
(5, 2),
(5, 3),
(5, 4),
(5, 10),
(6, 1),
(6, 2),
(6, 3),
(6, 4),
(6, 10),
(15, 11),
(15, 12);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `earnings`
--

CREATE TABLE `earnings` (
  `id` int(10) UNSIGNED NOT NULL,
  `market_id` int(10) UNSIGNED NOT NULL,
  `total_orders` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `total_earning` double(9,2) NOT NULL DEFAULT 0.00,
  `admin_earning` double(9,2) NOT NULL DEFAULT 0.00,
  `market_earning` double(9,2) NOT NULL DEFAULT 0.00,
  `delivery_fee` double(9,2) NOT NULL DEFAULT 0.00,
  `tax` double(9,2) NOT NULL DEFAULT 0.00,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `earnings`
--

INSERT INTO `earnings` (`id`, `market_id`, `total_orders`, `total_earning`, `admin_earning`, `market_earning`, `delivery_fee`, `tax`, `created_at`, `updated_at`) VALUES
(1, 2, 1, 45.28, 4.53, 40.75, 5.00, 0.00, '2020-11-18 19:08:10', '2020-11-28 02:45:31'),
(2, 1, 1, 30.05, 4.08, 25.97, 0.00, 0.00, '2020-11-27 04:24:41', '2020-11-28 02:24:11'),
(3, 3, 0, 0.00, 0.00, 0.00, 0.00, 0.00, '2020-11-27 04:29:24', '2020-11-27 04:29:24'),
(4, 10, 0, 0.00, 0.00, 0.00, 0.00, 0.00, '2020-11-27 04:31:48', '2020-11-27 04:31:48'),
(5, 11, 0, 0.00, 0.00, 0.00, 0.00, 0.00, '2021-02-08 05:37:44', '2021-02-08 05:37:44'),
(6, 12, 0, 0.00, 0.00, 0.00, 0.00, 0.00, '2021-02-16 09:12:02', '2021-02-16 09:12:02');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `faqs`
--

CREATE TABLE `faqs` (
  `id` int(10) UNSIGNED NOT NULL,
  `question` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `answer` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `faq_category_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `faqs`
--

INSERT INTO `faqs` (`id`, `question`, `answer`, `faq_category_id`, `created_at`, `updated_at`) VALUES
(5, 'How can I put special requests on my online order?', 'Whenever you have a special request, click the EDIT button next to your item and type your request. If you have a food allergy, please note that, too. Requesting extras may cost more and will be added to your total when necessary.', 1, '2020-11-07 23:48:19', '2020-11-08 18:26:38'),
(9, 'Do you have a delivery fee?', 'Yes, our delivery fee varies depending on the vendor & locations', 3, '2020-11-07 23:48:19', '2020-11-08 18:21:09'),
(10, 'Who do I call if there is a mistake with my order?', 'Please call our phone number within thirty minutes of delivery (even if it is after office hours) if there is anything missing or incorrect. We will get incorrect items replaced; missing items delivered or arrange for a credit towards your next order. Credits cannot be applied to online orders. To use your credit, please call the office to place your order. Thank you for reporting your concerns to us so we can continue improving our service.', 2, '2020-11-07 23:48:19', '2020-11-08 18:27:31'),
(14, 'What happens if I am missing items from my order when it is delivered?', 'Call us within one hour of delivery, so we may contact the vendor about the shortage. Any credits will be based upon the vendor\'s decision. Our drivers do not open up containers to verify your food.', 1, '2020-11-07 23:48:19', '2020-11-08 18:28:12'),
(15, 'What is your Refund Policy?', 'The customer is financially responsible for payment once an order is submitted. If you want to change your order, we will attempt to accommodate such wishes within the time constraints and the good will of the participating restaurants.', 1, '2020-11-07 23:48:19', '2020-11-08 18:29:34'),
(16, 'How long does it take for delivery?', 'Our normal delivery time is between 45 minutes and 1 hour, however certain situations such as traffic, weather, and vendor preparation time require extra time. Please know that we are always working hard to get your item delivered as quickly as possible. We appreciate your patience. Placing orders in advance is appreciated.', 3, '2020-11-07 23:48:19', '2020-11-08 18:25:28'),
(25, 'Are the prices different than at the vendors?', 'We do have a service fee charges on transactions on our platforms', 1, '2020-11-07 23:48:19', '2020-11-08 18:24:07'),
(27, 'How does this work?', 'We deliver to our customers from anywhere our customers would like. We take your order via the internet and place it with the restaurant or vendor. We dispatch a meal courier to pick up the food, pack it in an insulated carrier, and deliver it directly to you.', 1, '2020-11-07 23:48:19', '2020-11-08 18:19:53');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `faq_categories`
--

CREATE TABLE `faq_categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(127) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `faq_categories`
--

INSERT INTO `faq_categories` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Products', '2019-08-31 16:31:52', '2019-08-31 16:31:52'),
(2, 'Services', '2019-08-31 16:32:03', '2019-08-31 16:32:03'),
(3, 'Delivery', '2019-08-31 16:32:11', '2019-08-31 16:32:11');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `favorites`
--

CREATE TABLE `favorites` (
  `id` int(10) UNSIGNED NOT NULL,
  `product_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `favorites`
--

INSERT INTO `favorites` (`id`, `product_id`, `user_id`, `created_at`, `updated_at`) VALUES
(1, 14, 1, '2020-11-07 23:48:19', '2020-11-07 23:48:19'),
(2, 13, 5, '2020-11-07 23:48:19', '2020-11-07 23:48:19'),
(3, 4, 4, '2020-11-07 23:48:19', '2020-11-07 23:48:19'),
(4, 6, 5, '2020-11-07 23:48:19', '2020-11-07 23:48:19'),
(5, 27, 1, '2020-11-07 23:48:19', '2020-11-07 23:48:19'),
(6, 9, 2, '2020-11-07 23:48:19', '2020-11-07 23:48:19'),
(7, 4, 3, '2020-11-07 23:48:19', '2020-11-07 23:48:19'),
(8, 10, 5, '2020-11-07 23:48:19', '2020-11-07 23:48:19'),
(9, 20, 1, '2020-11-07 23:48:19', '2020-11-07 23:48:19'),
(10, 28, 5, '2020-11-07 23:48:19', '2020-11-07 23:48:19'),
(11, 30, 5, '2020-11-07 23:48:19', '2020-11-07 23:48:19'),
(12, 30, 6, '2020-11-07 23:48:19', '2020-11-07 23:48:19'),
(13, 8, 4, '2020-11-07 23:48:19', '2020-11-07 23:48:19'),
(14, 16, 3, '2020-11-07 23:48:19', '2020-11-07 23:48:19'),
(15, 6, 6, '2020-11-07 23:48:19', '2020-11-07 23:48:19'),
(16, 24, 4, '2020-11-07 23:48:19', '2020-11-07 23:48:19'),
(17, 13, 6, '2020-11-07 23:48:19', '2020-11-07 23:48:19'),
(18, 10, 5, '2020-11-07 23:48:19', '2020-11-07 23:48:19'),
(19, 13, 2, '2020-11-07 23:48:19', '2020-11-07 23:48:19'),
(20, 6, 5, '2020-11-07 23:48:19', '2020-11-07 23:48:19'),
(21, 3, 3, '2020-11-07 23:48:19', '2020-11-07 23:48:19'),
(22, 2, 6, '2020-11-07 23:48:19', '2020-11-07 23:48:19'),
(23, 2, 5, '2020-11-07 23:48:19', '2020-11-07 23:48:19'),
(24, 27, 3, '2020-11-07 23:48:19', '2020-11-07 23:48:19'),
(25, 28, 1, '2020-11-07 23:48:19', '2020-11-07 23:48:19'),
(26, 27, 6, '2020-11-07 23:48:19', '2020-11-07 23:48:19'),
(27, 8, 3, '2020-11-07 23:48:19', '2020-11-07 23:48:19'),
(28, 15, 6, '2020-11-07 23:48:19', '2020-11-07 23:48:19'),
(29, 1, 6, '2020-11-07 23:48:19', '2020-11-07 23:48:19'),
(30, 19, 6, '2020-11-07 23:48:19', '2020-11-07 23:48:19'),
(31, 1, 3, '2020-11-28 02:09:25', '2020-11-28 02:09:25');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `favorite_options`
--

CREATE TABLE `favorite_options` (
  `option_id` int(10) UNSIGNED NOT NULL,
  `favorite_id` int(10) UNSIGNED NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `favorite_options`
--

INSERT INTO `favorite_options` (`option_id`, `favorite_id`) VALUES
(1, 1),
(1, 5),
(2, 6),
(3, 2),
(5, 3);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `fields`
--

CREATE TABLE `fields` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(127) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `fields`
--

INSERT INTO `fields` (`id`, `name`, `description`, `created_at`, `updated_at`) VALUES
(1, 'Grocery', '<span style=\"color: rgb(77, 81, 86); font-family: arial, sans-serif; font-size: 14px;\">The&nbsp;</span><span style=\"font-weight: bold; color: rgb(95, 99, 104); font-family: arial, sans-serif; font-size: 14px;\">Grocery Shop</span><span style=\"color: rgb(77, 81, 86); font-family: arial, sans-serif; font-size: 14px;\">™ is the easiest and fastest way to have your&nbsp;</span><span style=\"font-weight: bold; color: rgb(95, 99, 104); font-family: arial, sans-serif; font-size: 14px;\">groceries</span><span style=\"color: rgb(77, 81, 86); font-family: arial, sans-serif; font-size: 14px;\">&nbsp;delivered to your doorstep</span>', '2020-04-11 19:03:21', '2020-11-08 21:51:22'),
(3, 'Restaurant', '<span style=\"color: rgb(77, 81, 86); font-family: arial, sans-serif; font-size: 14px;\">A&nbsp;</span><span style=\"font-weight: bold; color: rgb(95, 99, 104); font-family: arial, sans-serif; font-size: 14px;\">restaurant</span><span style=\"color: rgb(77, 81, 86); font-family: arial, sans-serif; font-size: 14px;\">&nbsp;or an eatery, is a business that prepares and serves food and drinks to customers. Meals are generally served and eaten on the premises</span>', '2020-04-11 19:03:21', '2020-11-08 19:11:28');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `galleries`
--

CREATE TABLE `galleries` (
  `id` int(10) UNSIGNED NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `market_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `galleries`
--

INSERT INTO `galleries` (`id`, `description`, `market_id`, `created_at`, `updated_at`) VALUES
(1, 'Odio quo reiciendis enim aliquid sed pariatur dignissimos.', 3, '2020-11-07 23:48:19', '2020-11-07 23:48:19'),
(2, 'Eveniet omnis est eveniet consequatur hic et dolorem dolor.', 1, '2020-11-07 23:48:19', '2020-11-07 23:48:19'),
(3, 'Afro Eat 4', 10, '2020-11-07 23:48:19', '2020-11-27 04:34:02'),
(4, 'Nobis consequuntur sed et maiores consequatur optio ab mollitia.', 7, '2020-11-07 23:48:19', '2020-11-07 23:48:19'),
(5, 'Qui tempora ad officia iusto cum officiis.', 5, '2020-11-07 23:48:19', '2020-11-07 23:48:19'),
(6, 'Libero dolor qui quisquam eum nulla voluptatem porro.', 8, '2020-11-07 23:48:19', '2020-11-07 23:48:19'),
(7, 'Est iusto optio vel.', 1, '2020-11-07 23:48:19', '2020-11-07 23:48:19'),
(8, 'Quia in ut molestiae totam accusantium tempora.', 2, '2020-11-07 23:48:19', '2020-11-07 23:48:19'),
(9, 'Numquam aut optio sunt sed.', 8, '2020-11-07 23:48:19', '2020-11-07 23:48:19'),
(10, 'Ipsum nobis sed aspernatur voluptatem.', 9, '2020-11-07 23:48:19', '2020-11-07 23:48:19'),
(11, 'Qui laudantium quo quo iste odio consequatur.', 5, '2020-11-07 23:48:19', '2020-11-07 23:48:19'),
(12, 'Natus aspernatur iure minima voluptas quis est aut ab.', 7, '2020-11-07 23:48:19', '2020-11-07 23:48:19'),
(13, 'Autem sit pariatur nihil laborum recusandae magni exercitationem.', 8, '2020-11-07 23:48:19', '2020-11-07 23:48:19'),
(14, 'Accusamus repudiandae est necessitatibus.', 3, '2020-11-07 23:48:19', '2020-11-07 23:48:19'),
(15, 'Aut laboriosam atque quia sunt similique doloremque voluptate.', 7, '2020-11-07 23:48:19', '2020-11-07 23:48:19'),
(16, 'Ex cumque magnam enim ipsum nam et minus itaque.', 7, '2020-11-07 23:48:19', '2020-11-07 23:48:19'),
(17, 'Aut illum et a velit est molestias.', 8, '2020-11-07 23:48:19', '2020-11-07 23:48:19'),
(18, 'Vendor 4&nbsp;', 10, '2020-11-07 23:48:19', '2020-11-27 04:34:58'),
(19, 'Consequatur voluptatibus doloribus iusto est commodi id.', 6, '2020-11-07 23:48:19', '2020-11-07 23:48:19'),
(20, 'Et qui perspiciatis voluptate tempora sed omnis odio quis.', 2, '2020-11-07 23:48:19', '2020-11-07 23:48:19');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `markets`
--

CREATE TABLE `markets` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(127) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT '',
  `address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `latitude` varchar(24) COLLATE utf8mb4_unicode_ci NOT NULL,
  `longitude` varchar(24) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mobile` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `information` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `admin_commission` double(8,2) DEFAULT 0.00,
  `delivery_fee` double(8,2) DEFAULT 0.00,
  `delivery_range` double(8,2) DEFAULT 0.00,
  `default_tax` double(8,2) DEFAULT 0.00,
  `closed` tinyint(1) DEFAULT 0,
  `active` tinyint(1) DEFAULT 0,
  `available_for_delivery` tinyint(1) DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `markets`
--

INSERT INTO `markets` (`id`, `name`, `description`, `address`, `latitude`, `longitude`, `phone`, `mobile`, `information`, `admin_commission`, `delivery_fee`, `delivery_range`, `default_tax`, `closed`, `active`, `available_for_delivery`, `created_at`, `updated_at`) VALUES
(11, 'Bendu Food Market', '<p>African Shop:</p><p>Bindu Market is an African Market located in South West Philadelphia</p><p><br></p>', '1948 S. 65 Street Philadelphia Pa 9142', '39.927840', '-75.237380', '215-487-8474', NULL, '<p>We specialized in dry products (fish, turkey, meat) and Loan Star Fufu</p>', 0.75, 25.00, 50.00, NULL, 0, 1, 1, '2021-02-08 05:37:44', '2021-02-16 09:13:52'),
(12, 'West African International Grocery', '<p>African food Market in Philadelphia. </p><p>Specialized in Dry meat, fish, Lone Start Fufu and everyday Fresh African food. </p>', '7282 Woodland Ave, Philadelphia Pa 19142', '39.917221', '-75.245537', '267-210-0240', NULL, NULL, 0.75, 25.00, NULL, NULL, 0, 1, 1, '2021-02-16 09:12:02', '2021-02-16 09:14:42');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `markets_payouts`
--

CREATE TABLE `markets_payouts` (
  `id` int(10) UNSIGNED NOT NULL,
  `market_id` int(10) UNSIGNED NOT NULL,
  `method` varchar(127) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `amount` double(9,2) NOT NULL DEFAULT 0.00,
  `paid_date` datetime DEFAULT NULL,
  `note` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `market_fields`
--

CREATE TABLE `market_fields` (
  `field_id` int(10) UNSIGNED NOT NULL,
  `market_id` int(10) UNSIGNED NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `market_fields`
--

INSERT INTO `market_fields` (`field_id`, `market_id`) VALUES
(1, 2),
(1, 3),
(1, 7),
(1, 9),
(1, 11),
(1, 12),
(2, 7),
(3, 1),
(3, 3),
(3, 6),
(3, 10),
(5, 8),
(6, 5);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `market_reviews`
--

CREATE TABLE `market_reviews` (
  `id` int(10) UNSIGNED NOT NULL,
  `review` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `rate` tinyint(3) UNSIGNED NOT NULL DEFAULT 0,
  `user_id` int(10) UNSIGNED NOT NULL,
  `market_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `media`
--

CREATE TABLE `media` (
  `id` int(10) UNSIGNED NOT NULL,
  `model_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) UNSIGNED NOT NULL,
  `collection_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `file_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mime_type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `disk` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `size` int(10) UNSIGNED NOT NULL,
  `manipulations` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `custom_properties` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `responsive_images` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `order_column` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `media`
--

INSERT INTO `media` (`id`, `model_type`, `model_id`, `collection_name`, `name`, `file_name`, `mime_type`, `disk`, `size`, `manipulations`, `custom_properties`, `responsive_images`, `order_column`, `created_at`, `updated_at`) VALUES
(1, 'App\\Models\\Upload', 1, 'app_logo', 'asfrogreem', 'asfrogreem.png', 'image/png', 'public', 11308, '[]', '{\"uuid\":\"4c2e28ca-7c91-487c-ab62-5719ea5a2e9d\",\"user_id\":1,\"generated_conversions\":{\"thumb\":true,\"icon\":true}}', '[]', 1, '2020-11-07 23:52:33', '2020-11-07 23:52:33'),
(2, 'App\\Models\\Upload', 2, 'app_logo', 'AfroEatOfficiallogo', 'AfroEatOfficiallogo.png', 'image/png', 'public', 157811, '[]', '{\"uuid\":\"2c969be4-868f-4d13-859b-cdff00472cb4\",\"user_id\":1,\"generated_conversions\":{\"thumb\":true,\"icon\":true}}', '[]', 2, '2020-11-07 23:53:11', '2020-11-07 23:53:12'),
(3, 'App\\Models\\User', 7, 'avatar', 'avatar_default_temp', 'avatar_default_temp.png', 'image/png', 'public', 2011, '[]', '{\"uuid\":\"$2y$10$ggkXCxXj1XZOeCJpE5os9eDpYXWaNE9o76J3EiDEirH\\/jpz0MkfEW\",\"generated_conversions\":{\"thumb\":true,\"icon\":true}}', '[]', 3, '2020-11-08 01:17:45', '2020-11-08 01:17:45'),
(4, 'App\\Models\\Upload', 3, 'image', '016-healthy food', '016-healthy-food.png', 'image/png', 'public', 28501, '[]', '{\"uuid\":\"f629d5f9-7702-469c-ba86-3e3b413a8346\",\"user_id\":1,\"generated_conversions\":{\"thumb\":true,\"icon\":true}}', '[]', 4, '2020-11-08 18:35:44', '2020-11-08 18:35:44'),
(12, 'App\\Models\\Upload', 7, 'image', '001-bag', '001-bag.png', 'image/png', 'public', 6625, '[]', '{\"uuid\":\"a34ed564-42f0-45ae-8508-862cc5017653\",\"user_id\":1,\"generated_conversions\":{\"thumb\":true,\"icon\":true}}', '[]', 12, '2020-11-08 18:47:47', '2020-11-08 18:47:47'),
(6, 'App\\Models\\Upload', 4, 'image', '024-sweet food', '024-sweet-food.png', 'image/png', 'public', 13649, '[]', '{\"uuid\":\"1074e857-1571-4a0c-8efa-769fece9803a\",\"user_id\":1,\"generated_conversions\":{\"thumb\":true,\"icon\":true}}', '[]', 6, '2020-11-08 18:37:27', '2020-11-08 18:37:28'),
(7, 'App\\Models\\Category', 2, 'image', '024-sweet food', '024-sweet-food.png', 'image/png', 'public', 13649, '[]', '{\"uuid\":\"1074e857-1571-4a0c-8efa-769fece9803a\",\"user_id\":1,\"generated_conversions\":{\"thumb\":true,\"icon\":true}}', '[]', 7, '2020-11-08 18:37:29', '2020-11-08 18:37:29'),
(8, 'App\\Models\\Upload', 5, 'image', '028-sauce', '028-sauce.png', 'image/png', 'public', 11739, '[]', '{\"uuid\":\"f4c612e1-0b48-467f-a3cc-e6038fdf26a7\",\"user_id\":1,\"generated_conversions\":{\"thumb\":true,\"icon\":true}}', '[]', 8, '2020-11-08 18:41:02', '2020-11-08 18:41:02'),
(9, 'App\\Models\\Category', 4, 'image', '028-sauce', '028-sauce.png', 'image/png', 'public', 11739, '[]', '{\"uuid\":\"f4c612e1-0b48-467f-a3cc-e6038fdf26a7\",\"user_id\":1,\"generated_conversions\":{\"thumb\":true,\"icon\":true}}', '[]', 9, '2020-11-08 18:41:07', '2020-11-08 18:41:07'),
(10, 'App\\Models\\Upload', 6, 'image', '016-healthy food', '016-healthy-food.png', 'image/png', 'public', 28501, '[]', '{\"uuid\":\"222e4518-c1ac-4d9d-8d1d-b2cd93f606f0\",\"user_id\":1,\"generated_conversions\":{\"thumb\":true,\"icon\":true}}', '[]', 10, '2020-11-08 18:47:32', '2020-11-08 18:47:32'),
(11, 'App\\Models\\Category', 1, 'image', '016-healthy food', '016-healthy-food.png', 'image/png', 'public', 28501, '[]', '{\"uuid\":\"222e4518-c1ac-4d9d-8d1d-b2cd93f606f0\",\"user_id\":1,\"generated_conversions\":{\"thumb\":true,\"icon\":true}}', '[]', 11, '2020-11-08 18:47:34', '2020-11-08 18:47:34'),
(13, 'App\\Models\\Category', 3, 'image', '001-bag', '001-bag.png', 'image/png', 'public', 6625, '[]', '{\"uuid\":\"a34ed564-42f0-45ae-8508-862cc5017653\",\"user_id\":1,\"generated_conversions\":{\"thumb\":true,\"icon\":true}}', '[]', 13, '2020-11-08 18:47:49', '2020-11-08 18:47:49'),
(14, 'App\\Models\\Upload', 8, 'image', '013-vegetable', '013-vegetable.png', 'image/png', 'public', 22768, '[]', '{\"uuid\":\"9822404c-b525-4937-910a-856617321649\",\"user_id\":1,\"generated_conversions\":{\"thumb\":true,\"icon\":true}}', '[]', 14, '2020-11-08 18:49:11', '2020-11-08 18:49:11'),
(15, 'App\\Models\\Category', 6, 'image', '013-vegetable', '013-vegetable.png', 'image/png', 'public', 22768, '[]', '{\"uuid\":\"9822404c-b525-4937-910a-856617321649\",\"user_id\":1,\"generated_conversions\":{\"thumb\":true,\"icon\":true}}', '[]', 15, '2020-11-08 18:49:14', '2020-11-08 18:49:14'),
(16, 'App\\Models\\Upload', 9, 'image', 'pexels-dana-tentis-262959', 'pexels-dana-tentis-262959.jpg', 'image/jpeg', 'public', 214138, '[]', '{\"uuid\":\"d4fd9c49-a322-40ff-93dd-0d81fa43b8a1\",\"user_id\":1,\"generated_conversions\":{\"thumb\":true,\"icon\":true}}', '[]', 16, '2020-11-08 19:11:09', '2020-11-08 19:11:09'),
(17, 'App\\Models\\Field', 3, 'image', 'pexels-dana-tentis-262959', 'pexels-dana-tentis-262959.jpg', 'image/jpeg', 'public', 214138, '[]', '{\"uuid\":\"d4fd9c49-a322-40ff-93dd-0d81fa43b8a1\",\"user_id\":1,\"generated_conversions\":{\"thumb\":true,\"icon\":true}}', '[]', 17, '2020-11-08 19:11:28', '2020-11-08 19:11:28'),
(18, 'App\\Models\\Upload', 10, 'image', 'pexels-ponyo-sakana-4194610', 'pexels-ponyo-sakana-4194610.jpg', 'image/jpeg', 'public', 84152, '[]', '{\"uuid\":\"ce86d062-5a2c-46de-865f-7cd5ec51a3bc\",\"user_id\":1,\"generated_conversions\":{\"thumb\":true,\"icon\":true}}', '[]', 18, '2020-11-08 21:50:56', '2020-11-08 21:50:56'),
(19, 'App\\Models\\Field', 1, 'image', 'pexels-ponyo-sakana-4194610', 'pexels-ponyo-sakana-4194610.jpg', 'image/jpeg', 'public', 84152, '[]', '{\"uuid\":\"ce86d062-5a2c-46de-865f-7cd5ec51a3bc\",\"user_id\":1,\"generated_conversions\":{\"thumb\":true,\"icon\":true}}', '[]', 19, '2020-11-08 21:51:22', '2020-11-08 21:51:22'),
(20, 'App\\Models\\Upload', 11, 'image', 'pexels-rauf-allahverdiyev-1367243', 'pexels-rauf-allahverdiyev-1367243.jpg', 'image/jpeg', 'public', 191724, '[]', '{\"uuid\":\"ab0652dd-e077-4020-8046-4e8f4c163199\",\"user_id\":1,\"generated_conversions\":{\"thumb\":true,\"icon\":true}}', '[]', 20, '2020-11-27 04:06:05', '2020-11-27 04:06:06'),
(21, 'App\\Models\\Slide', 1, 'image', 'pexels-rauf-allahverdiyev-1367243', 'pexels-rauf-allahverdiyev-1367243.jpg', 'image/jpeg', 'public', 191724, '[]', '{\"uuid\":\"ab0652dd-e077-4020-8046-4e8f4c163199\",\"user_id\":1,\"generated_conversions\":{\"thumb\":true,\"icon\":true}}', '[]', 21, '2020-11-27 04:07:13', '2020-11-27 04:07:13'),
(22, 'App\\Models\\Upload', 12, 'image', 'pexels-oziel-gómez-1578445', 'pexels-oziel-gómez-1578445.jpg', 'image/jpeg', 'public', 265224, '[]', '{\"uuid\":\"6cace509-37f0-40e5-ba5d-53f823734bec\",\"user_id\":1,\"generated_conversions\":{\"thumb\":true,\"icon\":true}}', '[]', 22, '2020-11-27 04:10:38', '2020-11-27 04:10:38'),
(24, 'App\\Models\\Upload', 13, 'image', 'pexels-pixabay-511088', 'pexels-pixabay-511088.jpg', 'image/jpeg', 'public', 188219, '[]', '{\"uuid\":\"eaa1fc64-6b9e-493c-9f49-f54cd1c2bba5\",\"user_id\":1,\"generated_conversions\":{\"thumb\":true,\"icon\":true}}', '[]', 23, '2020-11-27 04:14:40', '2020-11-27 04:14:41'),
(47, 'App\\Models\\Upload', 22, 'image', 'DSC08599', 'DSC08599.jpg', 'image/jpeg', 'public', 637021, '[]', '{\"uuid\":\"7d3f1aa6-e986-4e13-bdb8-2e861c19cbc2\",\"user_id\":1,\"generated_conversions\":{\"thumb\":true,\"icon\":true}}', '[]', 46, '2021-02-09 03:17:34', '2021-02-09 03:17:36'),
(26, 'App\\Models\\Upload', 14, 'image', 'pexels-robin-stickel-70497', 'pexels-robin-stickel-70497.jpg', 'image/jpeg', 'public', 192713, '[]', '{\"uuid\":\"3b41a861-0511-49fa-b520-ba269be34dd4\",\"user_id\":1,\"generated_conversions\":{\"thumb\":true,\"icon\":true}}', '[]', 25, '2020-11-27 04:20:17', '2020-11-27 04:20:17'),
(48, 'App\\Models\\Product', 42, 'image', 'DSC08599', 'DSC08599.jpg', 'image/jpeg', 'public', 637021, '[]', '{\"uuid\":\"7d3f1aa6-e986-4e13-bdb8-2e861c19cbc2\",\"user_id\":1,\"generated_conversions\":{\"thumb\":true,\"icon\":true}}', '[]', 47, '2021-02-09 03:21:26', '2021-02-09 03:21:26'),
(28, 'App\\Models\\Upload', 15, 'image', 'pexels-flickr-156114 (1)', 'pexels-flickr-156114-(1).jpg', 'image/jpeg', 'public', 329669, '[]', '{\"uuid\":\"107b0505-b852-4687-bd4f-ee51851ebef3\",\"user_id\":1,\"generated_conversions\":{\"thumb\":true,\"icon\":true}}', '[]', 27, '2020-11-27 04:24:14', '2020-11-27 04:24:14'),
(45, 'App\\Models\\Upload', 21, 'image', 'Plava Sauce image', 'Plava-Sauce-image.jpg', 'image/jpeg', 'public', 166491, '[]', '{\"uuid\":\"bcde08af-04fe-4253-b31a-548ce7d3685a\",\"user_id\":1,\"generated_conversions\":{\"thumb\":true,\"icon\":true}}', '[]', 44, '2021-02-08 06:07:56', '2021-02-08 06:07:57'),
(30, 'App\\Models\\Upload', 16, 'image', 'pexels-gratisography-4621', 'pexels-gratisography-4621.jpg', 'image/jpeg', 'public', 156052, '[]', '{\"uuid\":\"da7767b7-67bb-454c-b531-18fa07009a1b\",\"user_id\":1,\"generated_conversions\":{\"thumb\":true,\"icon\":true}}', '[]', 29, '2020-11-27 04:26:33', '2020-11-27 04:26:34'),
(44, 'App\\Models\\User', 14, 'avatar', 'avatar_default_temp', 'avatar_default_temp.png', 'image/png', 'public', 2011, '[]', '{\"uuid\":\"$2y$10$KsVtItfYN8lmNNsX1A3XXuW1wCmrOWKsh5XPYiUSyHz99PT9A6\\/cy\",\"generated_conversions\":{\"thumb\":true,\"icon\":true}}', '[]', 43, '2021-02-01 05:14:44', '2021-02-01 05:14:44'),
(32, 'App\\Models\\Upload', 17, 'image', 'pexels-polina-kovaleva-5644971', 'pexels-polina-kovaleva-5644971.jpg', 'image/jpeg', 'public', 222990, '[]', '{\"uuid\":\"852ff76c-0c8f-4771-b32d-9fa1a9ee02d1\",\"user_id\":1,\"generated_conversions\":{\"thumb\":true,\"icon\":true}}', '[]', 31, '2020-11-27 04:29:20', '2020-11-27 04:29:20'),
(43, 'App\\Models\\User', 13, 'avatar', 'avatar_default_temp', 'avatar_default_temp.png', 'image/png', 'public', 2011, '[]', '{\"uuid\":\"$2y$10$3LDK\\/JnsjaLUyr7isYQuh.hlwDDk.DEnKSadPb4t1JaOCUoii9siu\",\"generated_conversions\":{\"thumb\":true,\"icon\":true}}', '[]', 42, '2021-01-31 12:17:06', '2021-01-31 12:17:06'),
(34, 'App\\Models\\Upload', 18, 'image', 'pexels-nerfee-mirandilla-3186654', 'pexels-nerfee-mirandilla-3186654.jpg', 'image/jpeg', 'public', 341618, '[]', '{\"uuid\":\"351496a0-2a3f-443b-92b2-5c31fd196e48\",\"user_id\":1,\"generated_conversions\":{\"thumb\":true,\"icon\":true}}', '[]', 33, '2020-11-27 04:31:43', '2020-11-27 04:31:44'),
(42, 'App\\Models\\User', 12, 'avatar', 'avatar_default_temp', 'avatar_default_temp.png', 'image/png', 'public', 2011, '[]', '{\"uuid\":\"$2y$10$bvWIHo9ucaWgNhStRACpv.EltwUYSou5ZYVTunaWOYRR.20uciV8m\",\"generated_conversions\":{\"thumb\":true,\"icon\":true}}', '[]', 41, '2021-01-30 23:21:08', '2021-01-30 23:21:09'),
(36, 'App\\Models\\Upload', 19, 'image', 'pexels-nerfee-mirandilla-3186654', 'pexels-nerfee-mirandilla-3186654.jpg', 'image/jpeg', 'public', 341618, '[]', '{\"uuid\":\"dee3209f-9498-471d-b910-b1be3c12ab83\",\"user_id\":1,\"generated_conversions\":{\"thumb\":true,\"icon\":true}}', '[]', 35, '2020-11-27 04:33:58', '2020-11-27 04:33:58'),
(37, 'App\\Models\\Gallery', 3, 'image', 'pexels-nerfee-mirandilla-3186654', 'pexels-nerfee-mirandilla-3186654.jpg', 'image/jpeg', 'public', 341618, '[]', '{\"uuid\":\"dee3209f-9498-471d-b910-b1be3c12ab83\",\"user_id\":1,\"generated_conversions\":{\"thumb\":true,\"icon\":true}}', '[]', 36, '2020-11-27 04:34:02', '2020-11-27 04:34:02'),
(38, 'App\\Models\\Upload', 20, 'image', 'pexels-nerfee-mirandilla-3186654', 'pexels-nerfee-mirandilla-3186654.jpg', 'image/jpeg', 'public', 341618, '[]', '{\"uuid\":\"bb8388fa-db6f-4805-9b4d-b06da49142e7\",\"user_id\":1,\"generated_conversions\":{\"thumb\":true,\"icon\":true}}', '[]', 37, '2020-11-27 04:34:52', '2020-11-27 04:34:52'),
(39, 'App\\Models\\Gallery', 18, 'image', 'pexels-nerfee-mirandilla-3186654', 'pexels-nerfee-mirandilla-3186654.jpg', 'image/jpeg', 'public', 341618, '[]', '{\"uuid\":\"bb8388fa-db6f-4805-9b4d-b06da49142e7\",\"user_id\":1,\"generated_conversions\":{\"thumb\":true,\"icon\":true}}', '[]', 38, '2020-11-27 04:34:58', '2020-11-27 04:34:58'),
(40, 'App\\Models\\User', 10, 'avatar', 'avatar_default_temp', 'avatar_default_temp.png', 'image/png', 'public', 2011, '[]', '{\"uuid\":\"$2y$10$vY9CeTtTGzflnmBYCdYKKuvuaYYKapMHGhsj0mdP0gIJn6Lx0vP8i\",\"generated_conversions\":{\"thumb\":true,\"icon\":true}}', '[]', 39, '2021-01-24 02:38:12', '2021-01-24 02:38:12'),
(41, 'App\\Models\\User', 11, 'avatar', 'avatar_default_temp', 'avatar_default_temp.png', 'image/png', 'public', 2011, '[]', '{\"uuid\":\"$2y$10$fTHqSkfbeR8H297you.ye.hyY\\/zhZUO.7cfk0V1oBZ4qYTimC7tDW\",\"generated_conversions\":{\"thumb\":true,\"icon\":true}}', '[]', 40, '2021-01-27 11:13:10', '2021-01-27 11:13:10'),
(46, 'App\\Models\\Product', 41, 'image', 'Plava Sauce image', 'Plava-Sauce-image.jpg', 'image/jpeg', 'public', 166491, '[]', '{\"uuid\":\"bcde08af-04fe-4253-b31a-548ce7d3685a\",\"user_id\":1,\"generated_conversions\":{\"thumb\":true,\"icon\":true}}', '[]', 45, '2021-02-08 06:10:37', '2021-02-08 06:10:37'),
(49, 'App\\Models\\Upload', 23, 'image', 'DSC08603_600x600', 'DSC08603_600x600.jpg', 'image/jpeg', 'public', 125095, '[]', '{\"uuid\":\"ac1f91b3-6f66-45dc-8b22-cdc55279eb7c\",\"user_id\":1,\"generated_conversions\":{\"thumb\":true,\"icon\":true}}', '[]', 48, '2021-02-09 03:27:54', '2021-02-09 03:27:54'),
(50, 'App\\Models\\Product', 43, 'image', 'DSC08603_600x600', 'DSC08603_600x600.jpg', 'image/jpeg', 'public', 125095, '[]', '{\"uuid\":\"ac1f91b3-6f66-45dc-8b22-cdc55279eb7c\",\"user_id\":1,\"generated_conversions\":{\"thumb\":true,\"icon\":true}}', '[]', 49, '2021-02-09 03:31:57', '2021-02-09 03:31:57'),
(51, 'App\\Models\\Upload', 24, 'image', 'Bindu Market', 'Bindu-Market.jpg', 'image/jpeg', 'public', 148456, '[]', '{\"uuid\":\"f01102c0-f87c-4a3d-8d03-c76e1b4dc767\",\"user_id\":1,\"generated_conversions\":{\"thumb\":true,\"icon\":true}}', '[]', 50, '2021-02-09 03:39:40', '2021-02-09 03:39:40'),
(52, 'App\\Models\\Upload', 25, 'image', 'Bindu Market', 'Bindu-Market.jpg', 'image/jpeg', 'public', 148456, '[]', '{\"uuid\":\"31a59d34-384e-4e72-95fc-658909bed002\",\"user_id\":1,\"generated_conversions\":{\"thumb\":true,\"icon\":true}}', '[]', 51, '2021-02-09 03:40:55', '2021-02-09 03:40:55'),
(54, 'App\\Models\\Upload', 26, 'image', 'Bindu 2', 'Bindu-2.jpg', 'image/jpeg', 'public', 185170, '[]', '{\"uuid\":\"04c0a9af-5780-4a10-8d94-82415816ba1f\",\"user_id\":1,\"generated_conversions\":{\"thumb\":true,\"icon\":true}}', '[]', 52, '2021-02-09 03:43:30', '2021-02-09 03:43:30'),
(55, 'App\\Models\\Market', 11, 'image', 'Bindu 2', 'Bindu-2.jpg', 'image/jpeg', 'public', 185170, '[]', '{\"uuid\":\"04c0a9af-5780-4a10-8d94-82415816ba1f\",\"user_id\":1,\"generated_conversions\":{\"thumb\":true,\"icon\":true}}', '[]', 53, '2021-02-09 03:43:42', '2021-02-09 03:43:42'),
(56, 'App\\Models\\Upload', 27, 'image', 'praise', 'praise.jpg', 'image/jpeg', 'public', 107524, '[]', '{\"uuid\":\"ce0dc6e3-8a60-453b-a993-82a463ac7779\",\"user_id\":1,\"generated_conversions\":{\"thumb\":true,\"icon\":true}}', '[]', 54, '2021-02-09 03:55:55', '2021-02-09 03:55:55'),
(57, 'App\\Models\\Product', 44, 'image', 'praise', 'praise.jpg', 'image/jpeg', 'public', 107524, '[]', '{\"uuid\":\"ce0dc6e3-8a60-453b-a993-82a463ac7779\",\"user_id\":1,\"generated_conversions\":{\"thumb\":true,\"icon\":true}}', '[]', 55, '2021-02-09 03:58:07', '2021-02-09 03:58:07'),
(58, 'App\\Models\\Upload', 28, 'image', 'cos cos', 'cos-cos.jpg', 'image/jpeg', 'public', 125099, '[]', '{\"uuid\":\"0dc8fdec-132b-49ea-b859-3b0c7424908a\",\"user_id\":1,\"generated_conversions\":{\"thumb\":true,\"icon\":true}}', '[]', 56, '2021-02-22 01:55:21', '2021-02-22 01:55:21'),
(59, 'App\\Models\\Product', 45, 'image', 'cos cos', 'cos-cos.jpg', 'image/jpeg', 'public', 125099, '[]', '{\"uuid\":\"0dc8fdec-132b-49ea-b859-3b0c7424908a\",\"user_id\":1,\"generated_conversions\":{\"thumb\":true,\"icon\":true}}', '[]', 57, '2021-02-22 01:59:51', '2021-02-22 01:59:51'),
(60, 'App\\Models\\Upload', 29, 'image', '890', '890.jpg', 'image/jpeg', 'public', 128847, '[]', '{\"uuid\":\"64c29af8-6683-4327-8215-5f8c1cd6879f\",\"user_id\":1,\"generated_conversions\":{\"thumb\":true,\"icon\":true}}', '[]', 58, '2021-02-22 02:06:39', '2021-02-22 02:06:39'),
(61, 'App\\Models\\Product', 46, 'image', '890', '890.jpg', 'image/jpeg', 'public', 128847, '[]', '{\"uuid\":\"64c29af8-6683-4327-8215-5f8c1cd6879f\",\"user_id\":1,\"generated_conversions\":{\"thumb\":true,\"icon\":true}}', '[]', 59, '2021-02-22 02:11:38', '2021-02-22 02:11:38'),
(62, 'App\\Models\\Upload', 30, 'image', '891_600x600', '891_600x600.jpg', 'image/jpeg', 'public', 98821, '[]', '{\"uuid\":\"2ddf6dac-7499-4b12-b25c-72595d2a26fb\",\"user_id\":1,\"generated_conversions\":{\"thumb\":true,\"icon\":true}}', '[]', 60, '2021-02-22 02:13:38', '2021-02-22 02:13:38'),
(63, 'App\\Models\\Product', 47, 'image', '891_600x600', '891_600x600.jpg', 'image/jpeg', 'public', 98821, '[]', '{\"uuid\":\"2ddf6dac-7499-4b12-b25c-72595d2a26fb\",\"user_id\":1,\"generated_conversions\":{\"thumb\":true,\"icon\":true}}', '[]', 61, '2021-02-22 02:15:06', '2021-02-22 02:15:06'),
(64, 'App\\Models\\Upload', 31, 'image', '892_600x600', '892_600x600.jpg', 'image/jpeg', 'public', 147529, '[]', '{\"uuid\":\"57a9262c-d4ed-47f6-8a22-a198b010bddb\",\"user_id\":1,\"generated_conversions\":{\"thumb\":true,\"icon\":true}}', '[]', 62, '2021-02-22 02:16:54', '2021-02-22 02:16:54'),
(65, 'App\\Models\\Product', 48, 'image', '892_600x600', '892_600x600.jpg', 'image/jpeg', 'public', 147529, '[]', '{\"uuid\":\"57a9262c-d4ed-47f6-8a22-a198b010bddb\",\"user_id\":1,\"generated_conversions\":{\"thumb\":true,\"icon\":true}}', '[]', 63, '2021-02-22 02:17:58', '2021-02-22 02:17:58'),
(66, 'App\\Models\\Upload', 32, 'image', '823_600x600', '823_600x600.jpg', 'image/jpeg', 'public', 77831, '[]', '{\"uuid\":\"ddd5815d-5ed8-4a00-a42e-1b47cc216d5d\",\"user_id\":1,\"generated_conversions\":{\"thumb\":true,\"icon\":true}}', '[]', 64, '2021-02-22 02:20:03', '2021-02-22 02:20:03'),
(67, 'App\\Models\\Product', 49, 'image', '823_600x600', '823_600x600.jpg', 'image/jpeg', 'public', 77831, '[]', '{\"uuid\":\"ddd5815d-5ed8-4a00-a42e-1b47cc216d5d\",\"user_id\":1,\"generated_conversions\":{\"thumb\":true,\"icon\":true}}', '[]', 65, '2021-02-22 02:21:11', '2021-02-22 02:21:11'),
(68, 'App\\Models\\Upload', 33, 'image', '894_600x600', '894_600x600.jpg', 'image/jpeg', 'public', 92550, '[]', '{\"uuid\":\"fead46ef-a6b7-4d93-aac4-18ce508aa0f5\",\"user_id\":1,\"generated_conversions\":{\"thumb\":true,\"icon\":true}}', '[]', 66, '2021-02-22 02:22:33', '2021-02-22 02:22:33'),
(69, 'App\\Models\\Product', 50, 'image', '894_600x600', '894_600x600.jpg', 'image/jpeg', 'public', 92550, '[]', '{\"uuid\":\"fead46ef-a6b7-4d93-aac4-18ce508aa0f5\",\"user_id\":1,\"generated_conversions\":{\"thumb\":true,\"icon\":true}}', '[]', 67, '2021-02-22 02:24:31', '2021-02-22 02:24:31'),
(70, 'App\\Models\\Upload', 34, 'image', '895_600x600', '895_600x600.jpg', 'image/jpeg', 'public', 166491, '[]', '{\"uuid\":\"9b74d843-8cdc-469a-942e-0de01bc6bad1\",\"user_id\":1,\"generated_conversions\":{\"thumb\":true,\"icon\":true}}', '[]', 68, '2021-02-22 02:27:21', '2021-02-22 02:27:21'),
(71, 'App\\Models\\Product', 51, 'image', '895_600x600', '895_600x600.jpg', 'image/jpeg', 'public', 166491, '[]', '{\"uuid\":\"9b74d843-8cdc-469a-942e-0de01bc6bad1\",\"user_id\":1,\"generated_conversions\":{\"thumb\":true,\"icon\":true}}', '[]', 69, '2021-02-22 02:29:05', '2021-02-22 02:29:05'),
(72, 'App\\Models\\Upload', 35, 'image', '896_600x600', '896_600x600.jpg', 'image/jpeg', 'public', 102891, '[]', '{\"uuid\":\"3b3cba68-9da4-4bb1-bfaf-fe2b01ae341b\",\"user_id\":1,\"generated_conversions\":{\"thumb\":true,\"icon\":true}}', '[]', 70, '2021-02-22 02:30:54', '2021-02-22 02:30:54'),
(73, 'App\\Models\\Product', 52, 'image', '896_600x600', '896_600x600.jpg', 'image/jpeg', 'public', 102891, '[]', '{\"uuid\":\"3b3cba68-9da4-4bb1-bfaf-fe2b01ae341b\",\"user_id\":1,\"generated_conversions\":{\"thumb\":true,\"icon\":true}}', '[]', 71, '2021-02-22 02:33:43', '2021-02-22 02:33:43'),
(74, 'App\\Models\\Upload', 36, 'image', '897_600x600', '897_600x600.jpg', 'image/jpeg', 'public', 112206, '[]', '{\"uuid\":\"6711d05c-dd15-4970-b211-f11c79688e80\",\"user_id\":1,\"generated_conversions\":{\"thumb\":true,\"icon\":true}}', '[]', 72, '2021-02-22 02:35:05', '2021-02-22 02:35:05'),
(75, 'App\\Models\\Product', 53, 'image', '897_600x600', '897_600x600.jpg', 'image/jpeg', 'public', 112206, '[]', '{\"uuid\":\"6711d05c-dd15-4970-b211-f11c79688e80\",\"user_id\":1,\"generated_conversions\":{\"thumb\":true,\"icon\":true}}', '[]', 73, '2021-02-22 02:36:39', '2021-02-22 02:36:39'),
(76, 'App\\Models\\Upload', 37, 'image', '898_600x600', '898_600x600.jpg', 'image/jpeg', 'public', 91499, '[]', '{\"uuid\":\"d83ac7a4-81cb-496f-a0a3-08b3779c51e3\",\"user_id\":1,\"generated_conversions\":{\"thumb\":true,\"icon\":true}}', '[]', 74, '2021-02-22 02:39:17', '2021-02-22 02:39:17'),
(77, 'App\\Models\\Upload', 38, 'image', '898_600x600', '898_600x600.jpg', 'image/jpeg', 'public', 91499, '[]', '{\"uuid\":\"cbad789c-1435-42d5-adfd-786b6d6d9e98\",\"user_id\":1,\"generated_conversions\":{\"thumb\":true,\"icon\":true}}', '[]', 75, '2021-02-22 02:40:54', '2021-02-22 02:40:54'),
(78, 'App\\Models\\Upload', 39, 'image', '898_600x600', '898_600x600.jpg', 'image/jpeg', 'public', 91499, '[]', '{\"uuid\":\"e5affcc5-cc5e-4e48-9145-14849961143f\",\"user_id\":1,\"generated_conversions\":{\"thumb\":true,\"icon\":true}}', '[]', 76, '2021-02-22 02:41:09', '2021-02-22 02:41:09'),
(79, 'App\\Models\\Product', 54, 'image', '898_600x600', '898_600x600.jpg', 'image/jpeg', 'public', 91499, '[]', '{\"uuid\":\"e5affcc5-cc5e-4e48-9145-14849961143f\",\"user_id\":1,\"generated_conversions\":{\"thumb\":true,\"icon\":true}}', '[]', 77, '2021-02-22 02:41:42', '2021-02-22 02:41:42'),
(80, 'App\\Models\\Upload', 40, 'image', '899_600x600', '899_600x600.jpg', 'image/jpeg', 'public', 158416, '[]', '{\"uuid\":\"5a3200d6-094c-454b-8455-c42816541df8\",\"user_id\":1,\"generated_conversions\":{\"thumb\":true,\"icon\":true}}', '[]', 78, '2021-02-22 02:43:30', '2021-02-22 02:43:30'),
(81, 'App\\Models\\Product', 55, 'image', '899_600x600', '899_600x600.jpg', 'image/jpeg', 'public', 158416, '[]', '{\"uuid\":\"5a3200d6-094c-454b-8455-c42816541df8\",\"user_id\":1,\"generated_conversions\":{\"thumb\":true,\"icon\":true}}', '[]', 79, '2021-02-22 02:44:35', '2021-02-22 02:44:35'),
(82, 'App\\Models\\Upload', 41, 'image', '901_600x600', '901_600x600.jpg', 'image/jpeg', 'public', 92928, '[]', '{\"uuid\":\"f428e12d-3be3-4245-8725-de7b88a472f2\",\"user_id\":1,\"generated_conversions\":{\"thumb\":true,\"icon\":true}}', '[]', 80, '2021-02-22 02:46:33', '2021-02-22 02:46:33'),
(83, 'App\\Models\\Product', 56, 'image', '901_600x600', '901_600x600.jpg', 'image/jpeg', 'public', 92928, '[]', '{\"uuid\":\"f428e12d-3be3-4245-8725-de7b88a472f2\",\"user_id\":1,\"generated_conversions\":{\"thumb\":true,\"icon\":true}}', '[]', 81, '2021-02-22 02:47:55', '2021-02-22 02:47:55'),
(84, 'App\\Models\\Upload', 42, 'image', '902', '902.jpg', 'image/jpeg', 'public', 249843, '[]', '{\"uuid\":\"592c0493-59c1-4264-8e54-569fb8346e0b\",\"user_id\":1,\"generated_conversions\":{\"thumb\":true,\"icon\":true}}', '[]', 82, '2021-02-22 02:49:52', '2021-02-22 02:49:53'),
(86, 'App\\Models\\Upload', 43, 'image', '902_600x600', '902_600x600.jpg', 'image/jpeg', 'public', 77546, '[]', '{\"uuid\":\"196c80fa-53be-4a3e-b126-9b8ee4dda436\",\"user_id\":1,\"generated_conversions\":{\"thumb\":true,\"icon\":true}}', '[]', 83, '2021-02-22 02:55:17', '2021-02-22 02:55:17'),
(87, 'App\\Models\\Product', 57, 'image', '902_600x600', '902_600x600.jpg', 'image/jpeg', 'public', 77546, '[]', '{\"uuid\":\"196c80fa-53be-4a3e-b126-9b8ee4dda436\",\"user_id\":1,\"generated_conversions\":{\"thumb\":true,\"icon\":true}}', '[]', 84, '2021-02-22 02:55:29', '2021-02-22 02:55:29'),
(88, 'App\\Models\\Upload', 44, 'image', '903_600x600', '903_600x600.jpg', 'image/jpeg', 'public', 74041, '[]', '{\"uuid\":\"82a728b0-f579-46a1-9073-dd74b818a2be\",\"user_id\":1,\"generated_conversions\":{\"thumb\":true,\"icon\":true}}', '[]', 85, '2021-02-22 02:57:22', '2021-02-22 02:57:22'),
(89, 'App\\Models\\Product', 58, 'image', '903_600x600', '903_600x600.jpg', 'image/jpeg', 'public', 74041, '[]', '{\"uuid\":\"82a728b0-f579-46a1-9073-dd74b818a2be\",\"user_id\":1,\"generated_conversions\":{\"thumb\":true,\"icon\":true}}', '[]', 86, '2021-02-22 02:57:23', '2021-02-22 02:57:23'),
(90, 'App\\Models\\Upload', 45, 'image', '904_600x600', '904_600x600.jpg', 'image/jpeg', 'public', 72801, '[]', '{\"uuid\":\"09b28615-6bf3-4715-861c-a072a7f0ef60\",\"user_id\":1,\"generated_conversions\":{\"thumb\":true,\"icon\":true}}', '[]', 87, '2021-02-22 02:58:59', '2021-02-22 02:58:59'),
(91, 'App\\Models\\Product', 59, 'image', '904_600x600', '904_600x600.jpg', 'image/jpeg', 'public', 72801, '[]', '{\"uuid\":\"09b28615-6bf3-4715-861c-a072a7f0ef60\",\"user_id\":1,\"generated_conversions\":{\"thumb\":true,\"icon\":true}}', '[]', 88, '2021-02-22 03:03:48', '2021-02-22 03:03:48'),
(92, 'App\\Models\\Upload', 46, 'image', '905_600x600', '905_600x600.jpg', 'image/jpeg', 'public', 50569, '[]', '{\"uuid\":\"fcdf062a-217e-40b6-be1f-31ab555c7ef5\",\"user_id\":1,\"generated_conversions\":{\"thumb\":true,\"icon\":true}}', '[]', 89, '2021-02-22 03:05:03', '2021-02-22 03:05:03'),
(93, 'App\\Models\\Product', 60, 'image', '905_600x600', '905_600x600.jpg', 'image/jpeg', 'public', 50569, '[]', '{\"uuid\":\"fcdf062a-217e-40b6-be1f-31ab555c7ef5\",\"user_id\":1,\"generated_conversions\":{\"thumb\":true,\"icon\":true}}', '[]', 90, '2021-02-22 03:07:28', '2021-02-22 03:07:28'),
(94, 'App\\Models\\Upload', 47, 'image', '906_600x600', '906_600x600.jpg', 'image/jpeg', 'public', 27455, '[]', '{\"uuid\":\"926c47bc-0d5a-4079-8458-618223922fa8\",\"user_id\":1,\"generated_conversions\":{\"thumb\":true,\"icon\":true}}', '[]', 91, '2021-02-22 03:09:01', '2021-02-22 03:09:01'),
(95, 'App\\Models\\Product', 61, 'image', '906_600x600', '906_600x600.jpg', 'image/jpeg', 'public', 27455, '[]', '{\"uuid\":\"926c47bc-0d5a-4079-8458-618223922fa8\",\"user_id\":1,\"generated_conversions\":{\"thumb\":true,\"icon\":true}}', '[]', 92, '2021-02-22 03:11:11', '2021-02-22 03:11:11'),
(96, 'App\\Models\\Upload', 48, 'image', '907_600x600', '907_600x600.jpg', 'image/jpeg', 'public', 59449, '[]', '{\"uuid\":\"2d853c41-b78e-4713-bf66-87f8898ba2ad\",\"user_id\":1,\"generated_conversions\":{\"thumb\":true,\"icon\":true}}', '[]', 93, '2021-02-22 03:14:35', '2021-02-22 03:14:36'),
(97, 'App\\Models\\Product', 62, 'image', '907_600x600', '907_600x600.jpg', 'image/jpeg', 'public', 59449, '[]', '{\"uuid\":\"2d853c41-b78e-4713-bf66-87f8898ba2ad\",\"user_id\":1,\"generated_conversions\":{\"thumb\":true,\"icon\":true}}', '[]', 94, '2021-02-22 03:16:37', '2021-02-22 03:16:37'),
(98, 'App\\Models\\User', 19, 'avatar', 'avatar_default_temp', 'avatar_default_temp.png', 'image/png', 'public', 2011, '[]', '{\"uuid\":\"$2y$10$WSMVgtfqsztJyjqBAcY5u.IuqGXGUWDWZOcDpJF0eFKLrTBIlywKy\",\"generated_conversions\":{\"thumb\":true,\"icon\":true}}', '[]', 95, '2021-03-09 17:07:52', '2021-03-09 17:07:53'),
(99, 'App\\Models\\User', 20, 'avatar', 'avatar_default_temp', 'avatar_default_temp.png', 'image/png', 'public', 2011, '[]', '{\"uuid\":\"$2y$10$1DKRP29t.lBAqT1PqVRr0eWgncNDqkUxC6zukXKrV9ymJA.rLmT3i\",\"generated_conversions\":{\"thumb\":true,\"icon\":true}}', '[]', 96, '2021-03-24 11:52:30', '2021-03-24 11:52:30'),
(100, 'App\\Models\\User', 21, 'avatar', 'avatar_default_temp', 'avatar_default_temp.png', 'image/png', 'public', 2011, '[]', '{\"uuid\":\"$2y$10$pqP9bZIfLklgw.SJpeT3KOFXnMqMQ98rl16bLtu6Hrp3K914gykNO\",\"generated_conversions\":{\"thumb\":true,\"icon\":true}}', '[]', 97, '2021-03-25 01:25:45', '2021-03-25 01:25:45'),
(101, 'App\\Models\\User', 22, 'avatar', 'avatar_default_temp', 'avatar_default_temp.png', 'image/png', 'public', 2011, '[]', '{\"uuid\":\"$2y$10$pQfqlpCGHB6ZTj41smYhU.khXzsFatoxpMUalZjl9Efnl6FWmn9uK\",\"generated_conversions\":{\"thumb\":true,\"icon\":true}}', '[]', 98, '2021-03-25 01:32:11', '2021-03-25 01:32:12');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2018_05_26_175145_create_permission_tables', 1),
(4, '2018_06_12_140344_create_media_table', 1),
(5, '2018_06_13_035117_create_uploads_table', 1),
(6, '2018_07_17_180731_create_settings_table', 1),
(7, '2018_07_24_211308_create_custom_fields_table', 1),
(8, '2018_07_24_211327_create_custom_field_values_table', 1),
(9, '2019_08_29_213820_create_fields_table', 1),
(10, '2019_08_29_213821_create_markets_table', 1),
(11, '2019_08_29_213822_create_categories_table', 1),
(12, '2019_08_29_213826_create_option_groups_table', 1),
(13, '2019_08_29_213829_create_faq_categories_table', 1),
(14, '2019_08_29_213833_create_order_statuses_table', 1),
(15, '2019_08_29_213837_create_products_table', 1),
(16, '2019_08_29_213838_create_options_table', 1),
(17, '2019_08_29_213842_create_galleries_table', 1),
(18, '2019_08_29_213847_create_product_reviews_table', 1),
(19, '2019_08_29_213921_create_payments_table', 1),
(20, '2019_08_29_213922_create_delivery_addresses_table', 1),
(21, '2019_08_29_213926_create_faqs_table', 1),
(22, '2019_08_29_213940_create_market_reviews_table', 1),
(23, '2019_08_30_152927_create_favorites_table', 1),
(24, '2019_08_31_111104_create_orders_table', 1),
(25, '2019_09_04_153857_create_carts_table', 1),
(26, '2019_09_04_153858_create_favorite_options_table', 1),
(27, '2019_09_04_153859_create_cart_options_table', 1),
(28, '2019_09_04_153958_create_product_orders_table', 1),
(29, '2019_09_04_154957_create_product_order_options_table', 1),
(30, '2019_09_04_163857_create_user_markets_table', 1),
(31, '2019_10_22_144652_create_currencies_table', 1),
(32, '2019_12_14_134302_create_driver_markets_table', 1),
(33, '2020_03_25_094752_create_drivers_table', 1),
(34, '2020_03_25_094802_create_earnings_table', 1),
(35, '2020_03_25_094809_create_drivers_payouts_table', 1),
(36, '2020_03_25_094817_create_markets_payouts_table', 1),
(37, '2020_03_27_094855_create_notifications_table', 1),
(38, '2020_04_11_135804_create_market_fields_table', 1),
(39, '2020_08_23_181022_create_coupons_table', 1),
(40, '2020_08_23_181029_create_discountables_table', 1),
(41, '2020_09_01_192732_create_slides_table', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `model_has_permissions`
--

CREATE TABLE `model_has_permissions` (
  `permission_id` int(10) UNSIGNED NOT NULL,
  `model_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `model_has_roles`
--

CREATE TABLE `model_has_roles` (
  `role_id` int(10) UNSIGNED NOT NULL,
  `model_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `model_has_roles`
--

INSERT INTO `model_has_roles` (`role_id`, `model_type`, `model_id`) VALUES
(2, 'App\\Models\\User', 1),
(3, 'App\\Models\\User', 2),
(3, 'App\\Models\\User', 16),
(3, 'App\\Models\\User', 17),
(4, 'App\\Models\\User', 3),
(4, 'App\\Models\\User', 4),
(4, 'App\\Models\\User', 7),
(4, 'App\\Models\\User', 8),
(4, 'App\\Models\\User', 9),
(4, 'App\\Models\\User', 10),
(4, 'App\\Models\\User', 11),
(4, 'App\\Models\\User', 12),
(4, 'App\\Models\\User', 13),
(4, 'App\\Models\\User', 14),
(4, 'App\\Models\\User', 19),
(4, 'App\\Models\\User', 20),
(4, 'App\\Models\\User', 21),
(4, 'App\\Models\\User', 22),
(5, 'App\\Models\\User', 5),
(5, 'App\\Models\\User', 6),
(5, 'App\\Models\\User', 15),
(5, 'App\\Models\\User', 18);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `notifications`
--

CREATE TABLE `notifications` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `notifiable_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `notifiable_id` bigint(20) UNSIGNED NOT NULL,
  `data` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `read_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `notifications`
--

INSERT INTO `notifications` (`id`, `type`, `notifiable_type`, `notifiable_id`, `data`, `read_at`, `created_at`, `updated_at`) VALUES
('e8ff0b3f-511f-43fe-b15f-24957f80232b', 'App\\Notifications\\NewOrder', 'App\\Models\\User', 2, '{\"order_id\":1}', NULL, '2020-11-28 02:10:35', '2020-11-28 02:10:35'),
('2ef239b3-4b3b-4c37-9156-46f0935b98d7', 'App\\Notifications\\StatusChangedOrder', 'App\\Models\\User', 3, '{\"order_id\":1}', NULL, '2020-11-28 02:24:11', '2020-11-28 02:24:11'),
('c291cba7-5e6a-4f84-b72d-9bd146a5cf44', 'App\\Notifications\\AssignedOrder', 'App\\Models\\User', 5, '{\"order_id\":1}', NULL, '2020-11-28 02:24:11', '2020-11-28 02:24:11'),
('9eece747-b3f8-4793-8ffe-4964a26eda43', 'App\\Notifications\\NewOrder', 'App\\Models\\User', 2, '{\"order_id\":2}', NULL, '2020-11-28 02:44:34', '2020-11-28 02:44:34'),
('9139c722-76b3-4e03-b2d0-c2f4bc086bde', 'App\\Notifications\\StatusChangedOrder', 'App\\Models\\User', 3, '{\"order_id\":2}', NULL, '2020-11-28 02:45:31', '2020-11-28 02:45:31'),
('8c727762-c7a2-4633-879e-2831a8883117', 'App\\Notifications\\AssignedOrder', 'App\\Models\\User', 5, '{\"order_id\":2}', NULL, '2020-11-28 02:45:31', '2020-11-28 02:45:31'),
('df528c9c-3df1-4f09-ad7f-256214bc5029', 'App\\Notifications\\StatusChangedOrder', 'App\\Models\\User', 3, '{\"order_id\":2}', NULL, '2020-11-28 02:48:51', '2020-11-28 02:48:51'),
('386f16a7-0651-403f-abb6-5781c2719465', 'App\\Notifications\\NewOrder', 'App\\Models\\User', 2, '{\"order_id\":3}', NULL, '2020-11-28 03:46:38', '2020-11-28 03:46:38'),
('c883234b-0797-415e-b781-25cd43bb694f', 'App\\Notifications\\NewOrder', 'App\\Models\\User', 2, '{\"order_id\":4}', NULL, '2021-01-04 16:14:30', '2021-01-04 16:14:30'),
('6ee583cf-29b1-45ae-b2b9-a286dfd430fc', 'App\\Notifications\\NewOrder', 'App\\Models\\User', 2, '{\"order_id\":5}', NULL, '2021-01-04 16:27:46', '2021-01-04 16:27:46'),
('8e2ecfe1-4ae5-411f-9597-0b5b51e843a8', 'App\\Notifications\\NewOrder', 'App\\Models\\User', 2, '{\"order_id\":6}', NULL, '2021-01-06 14:50:54', '2021-01-06 14:50:54'),
('bf872c2c-7a3d-4f83-95ac-31cc77e5696b', 'App\\Notifications\\NewOrder', 'App\\Models\\User', 2, '{\"order_id\":7}', NULL, '2021-01-06 14:51:47', '2021-01-06 14:51:47'),
('0af1f7d3-a018-404f-be08-467a89f0b729', 'App\\Notifications\\NewOrder', 'App\\Models\\User', 2, '{\"order_id\":8}', NULL, '2021-01-10 23:01:33', '2021-01-10 23:01:33'),
('cf9f2960-cae7-45ed-9e8b-835f7638eacc', 'App\\Notifications\\NewOrder', 'App\\Models\\User', 2, '{\"order_id\":9}', NULL, '2021-01-14 22:42:34', '2021-01-14 22:42:34'),
('1d17665d-711c-477f-ae1e-f5790824f210', 'App\\Notifications\\NewOrder', 'App\\Models\\User', 2, '{\"order_id\":10}', NULL, '2021-01-14 22:46:34', '2021-01-14 22:46:34'),
('66783db4-e118-4254-8c7c-d25f2834fadb', 'App\\Notifications\\NewOrder', 'App\\Models\\User', 2, '{\"order_id\":11}', NULL, '2021-01-14 22:49:07', '2021-01-14 22:49:07'),
('44c1d157-a60d-4762-9493-0ef516c3198b', 'App\\Notifications\\NewOrder', 'App\\Models\\User', 2, '{\"order_id\":19}', NULL, '2021-01-14 23:42:30', '2021-01-14 23:42:30'),
('7f8a06bd-b0c8-4b5f-921b-4ff417d2c206', 'App\\Notifications\\NewOrder', 'App\\Models\\User', 2, '{\"order_id\":21}', NULL, '2021-01-14 23:46:42', '2021-01-14 23:46:42'),
('8ee98f2a-b06e-46bb-89f8-fb1b78ba0d60', 'App\\Notifications\\NewOrder', 'App\\Models\\User', 2, '{\"order_id\":24}', NULL, '2021-01-14 23:49:09', '2021-01-14 23:49:09'),
('3de6121e-3dab-47bd-aae0-7033283319fa', 'App\\Notifications\\NewOrder', 'App\\Models\\User', 2, '{\"order_id\":25}', NULL, '2021-01-14 23:49:55', '2021-01-14 23:49:55'),
('04d2dce1-7749-4a8c-8d3c-48efe6c12f7e', 'App\\Notifications\\NewOrder', 'App\\Models\\User', 2, '{\"order_id\":33}', NULL, '2021-01-15 00:36:21', '2021-01-15 00:36:21'),
('0efa31ea-ef72-43d0-8f10-9b660dec72b7', 'App\\Notifications\\NewOrder', 'App\\Models\\User', 16, '{\"order_id\":34}', NULL, '2021-03-25 01:31:53', '2021-03-25 01:31:53'),
('c924b424-6b9d-4299-9fee-3c526c9c5737', 'App\\Notifications\\NewOrder', 'App\\Models\\User', 16, '{\"order_id\":35}', NULL, '2021-03-25 01:52:32', '2021-03-25 01:52:32'),
('c92cf691-1b9c-4b52-aa41-c3db85a6b338', 'App\\Notifications\\NewOrder', 'App\\Models\\User', 16, '{\"order_id\":36}', NULL, '2021-03-26 04:38:21', '2021-03-26 04:38:21');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `options`
--

CREATE TABLE `options` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(127) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `price` double(8,2) NOT NULL DEFAULT 0.00,
  `product_id` int(10) UNSIGNED NOT NULL,
  `option_group_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `options`
--

INSERT INTO `options` (`id`, `name`, `description`, `price`, `product_id`, `option_group_id`, `created_at`, `updated_at`) VALUES
(1, '500g', 'Atque alias asperiores rem.', 41.82, 14, 2, '2020-11-07 23:48:19', '2020-11-07 23:48:19'),
(2, 'XL', 'Fuga a non consectetur rem.', 36.27, 5, 3, '2020-11-07 23:48:19', '2020-11-07 23:48:19'),
(3, 'White', 'Consequatur repellendus quasi.', 48.70, 27, 3, '2020-11-07 23:48:19', '2020-11-07 23:48:19'),
(4, 'White', 'Officia consequatur consectetur.', 31.06, 2, 2, '2020-11-07 23:48:19', '2020-11-07 23:48:19'),
(5, 'Tomato', 'Commodi molestiae aut.', 26.38, 16, 1, '2020-11-07 23:48:19', '2020-11-07 23:48:19'),
(6, '500g', 'Quaerat est quia aut dolor.', 17.68, 18, 2, '2020-11-07 23:48:19', '2020-11-07 23:48:19'),
(7, '1Kg', 'Deserunt est itaque mollitia.', 26.59, 28, 1, '2020-11-07 23:48:19', '2020-11-07 23:48:19'),
(8, 'S', 'Molestias fugiat illum et sit.', 22.41, 9, 3, '2020-11-07 23:48:19', '2020-11-07 23:48:19'),
(9, 'XL', 'Itaque assumenda doloremque sapiente autem fuga.', 38.26, 1, 2, '2020-11-07 23:48:19', '2020-11-07 23:48:19'),
(10, '2L', 'Maiores ut quia est inventore.', 12.23, 5, 3, '2020-11-07 23:48:19', '2020-11-07 23:48:19'),
(11, 'Red', 'Numquam porro cum voluptates eius.', 20.30, 10, 2, '2020-11-07 23:48:19', '2020-11-07 23:48:19'),
(12, 'Tomato', 'Praesentium fugit consequatur tempore.', 30.08, 30, 4, '2020-11-07 23:48:19', '2020-11-07 23:48:19'),
(13, '500g', 'Commodi assumenda vel eum aut.', 25.36, 22, 4, '2020-11-07 23:48:19', '2020-11-07 23:48:19'),
(14, 'XL', 'Quidem nesciunt et et.', 36.97, 26, 4, '2020-11-07 23:48:19', '2020-11-07 23:48:19'),
(15, 'L', 'Quia sit a.', 42.85, 17, 4, '2020-11-07 23:48:19', '2020-11-07 23:48:19'),
(16, 'White', 'Animi eligendi explicabo nesciunt dolores.', 33.52, 5, 1, '2020-11-07 23:48:19', '2020-11-07 23:48:19'),
(17, 'Red', 'Sunt ut aut ut.', 44.66, 29, 1, '2020-11-07 23:48:19', '2020-11-07 23:48:19'),
(18, 'Red', 'Consequatur quod aut sequi.', 44.43, 6, 4, '2020-11-07 23:48:19', '2020-11-07 23:48:19'),
(19, 'XL', 'Rem fuga recusandae natus voluptatem quod.', 33.88, 1, 3, '2020-11-07 23:48:19', '2020-11-07 23:48:19'),
(20, 'Green', 'Nobis iure eos voluptas aut.', 14.99, 4, 3, '2020-11-07 23:48:19', '2020-11-07 23:48:19'),
(21, 'L', 'Magni ex ipsa saepe placeat.', 34.43, 23, 2, '2020-11-07 23:48:19', '2020-11-07 23:48:19'),
(22, '500g', 'Ut illum modi sed.', 29.00, 9, 1, '2020-11-07 23:48:19', '2020-11-07 23:48:19'),
(23, 'S', 'Incidunt praesentium consequatur.', 12.66, 25, 2, '2020-11-07 23:48:19', '2020-11-07 23:48:19'),
(24, 'XL', 'Modi numquam harum consequatur.', 41.08, 16, 1, '2020-11-07 23:48:19', '2020-11-07 23:48:19'),
(25, '500g', 'Sequi et velit cum temporibus id.', 22.89, 4, 4, '2020-11-07 23:48:19', '2020-11-07 23:48:19'),
(26, 'Green', 'Aperiam voluptatem quibusdam.', 39.70, 11, 2, '2020-11-07 23:48:19', '2020-11-07 23:48:19'),
(27, 'Oil', 'Ab praesentium maxime quia.', 41.61, 16, 2, '2020-11-07 23:48:19', '2020-11-07 23:48:19'),
(28, 'L', 'Tempora facere veniam deleniti sint.', 17.50, 17, 4, '2020-11-07 23:48:19', '2020-11-07 23:48:19'),
(29, '2L', 'Omnis nobis vel dolorum ullam cum.', 40.58, 30, 2, '2020-11-07 23:48:19', '2020-11-07 23:48:19'),
(30, 'Tomato', 'Et et ea consequatur possimus optio.', 13.97, 9, 3, '2020-11-07 23:48:19', '2020-11-07 23:48:19'),
(31, 'L', 'Blanditiis vel laboriosam minima.', 39.24, 14, 1, '2020-11-07 23:48:19', '2020-11-07 23:48:19'),
(32, 'S', 'Fuga totam distinctio iusto voluptate.', 47.87, 7, 2, '2020-11-07 23:48:19', '2020-11-07 23:48:19'),
(33, 'XL', 'Eveniet veritatis esse sed.', 21.76, 13, 1, '2020-11-07 23:48:19', '2020-11-07 23:48:19'),
(34, 'S', 'Quis dolor adipisci.', 46.78, 2, 3, '2020-11-07 23:48:19', '2020-11-07 23:48:19'),
(35, 'Green', 'Modi et non.', 49.25, 8, 3, '2020-11-07 23:48:19', '2020-11-07 23:48:19'),
(36, '500g', 'Voluptas rerum atque maxime et.', 24.87, 6, 4, '2020-11-07 23:48:19', '2020-11-07 23:48:19'),
(37, 'Tomato', 'Nam cum earum quibusdam voluptas pariatur.', 39.48, 27, 4, '2020-11-07 23:48:19', '2020-11-07 23:48:19'),
(38, 'S', 'Deserunt saepe possimus quas.', 33.13, 19, 1, '2020-11-07 23:48:19', '2020-11-07 23:48:19'),
(39, 'Green', 'Illo officiis numquam consectetur.', 25.55, 22, 4, '2020-11-07 23:48:19', '2020-11-07 23:48:19'),
(40, 'Red', 'Omnis recusandae voluptate qui.', 23.86, 6, 3, '2020-11-07 23:48:19', '2020-11-07 23:48:19'),
(41, 'Green', 'Aspernatur sint fugiat quia a.', 30.67, 11, 3, '2020-11-07 23:48:19', '2020-11-07 23:48:19'),
(42, '1Kg', 'Ut aperiam consectetur ut in.', 18.94, 22, 4, '2020-11-07 23:48:19', '2020-11-07 23:48:19'),
(43, '500g', 'Amet similique maiores et.', 18.31, 7, 3, '2020-11-07 23:48:19', '2020-11-07 23:48:19'),
(44, '2L', 'Voluptatum autem et qui iure voluptas.', 35.09, 6, 1, '2020-11-07 23:48:19', '2020-11-07 23:48:19'),
(45, 'Green', 'Quam ad nobis rerum fugiat.', 19.59, 7, 3, '2020-11-07 23:48:19', '2020-11-07 23:48:19'),
(46, 'Oil', 'Perspiciatis qui quae adipisci.', 27.70, 3, 4, '2020-11-07 23:48:19', '2020-11-07 23:48:19'),
(47, 'L', 'Cupiditate assumenda omnis quidem dolore.', 32.04, 24, 4, '2020-11-07 23:48:19', '2020-11-07 23:48:19'),
(48, 'XL', 'Maiores nesciunt ut error quaerat.', 12.16, 23, 1, '2020-11-07 23:48:19', '2020-11-07 23:48:19'),
(49, '1Kg', 'Magni sit magnam ab in.', 28.97, 13, 3, '2020-11-07 23:48:19', '2020-11-07 23:48:19'),
(50, '5L', 'Exercitationem optio modi nobis assumenda et.', 37.09, 17, 4, '2020-11-07 23:48:19', '2020-11-07 23:48:19'),
(51, '500g', 'Animi architecto doloribus at.', 46.43, 1, 1, '2020-11-07 23:48:19', '2020-11-07 23:48:19'),
(52, 'S', 'Aspernatur error aut aut.', 48.77, 28, 4, '2020-11-07 23:48:19', '2020-11-07 23:48:19'),
(53, '5L', 'Labore quos quia suscipit itaque.', 38.00, 3, 2, '2020-11-07 23:48:19', '2020-11-07 23:48:19'),
(54, 'XL', 'Et quis ab saepe.', 20.04, 4, 3, '2020-11-07 23:48:19', '2020-11-07 23:48:19'),
(55, '1Kg', 'Non provident nostrum amet recusandae incidunt.', 29.60, 1, 3, '2020-11-07 23:48:19', '2020-11-07 23:48:19'),
(56, 'Green', 'Et dolorem asperiores unde deleniti nihil.', 18.12, 10, 1, '2020-11-07 23:48:19', '2020-11-07 23:48:19'),
(57, 'White', 'Illo aut asperiores voluptates.', 42.03, 15, 1, '2020-11-07 23:48:19', '2020-11-07 23:48:19'),
(58, '2L', 'Ducimus ipsam molestiae inventore magnam.', 46.52, 6, 1, '2020-11-07 23:48:19', '2020-11-07 23:48:19'),
(59, 'XL', 'Dicta nulla ut.', 30.73, 22, 4, '2020-11-07 23:48:19', '2020-11-07 23:48:19'),
(60, '500g', 'Exercitationem velit rerum iusto nihil.', 35.32, 6, 4, '2020-11-07 23:48:19', '2020-11-07 23:48:19'),
(61, 'Tomato', 'Et voluptate enim et.', 34.48, 24, 3, '2020-11-07 23:48:19', '2020-11-07 23:48:19'),
(62, 'S', 'Et et quia sint et.', 11.43, 18, 1, '2020-11-07 23:48:19', '2020-11-07 23:48:19'),
(63, 'White', 'Commodi nam itaque quo dolores non.', 41.06, 21, 2, '2020-11-07 23:48:19', '2020-11-07 23:48:19'),
(64, 'Red', 'Nam aut aut quia quaerat.', 12.57, 2, 4, '2020-11-07 23:48:19', '2020-11-07 23:48:19'),
(65, '500g', 'Commodi fugiat eius dicta blanditiis est.', 22.15, 25, 3, '2020-11-07 23:48:19', '2020-11-07 23:48:19'),
(66, 'Tomato', 'Iste nemo porro doloremque.', 47.32, 7, 2, '2020-11-07 23:48:19', '2020-11-07 23:48:19'),
(67, '2L', 'Reprehenderit blanditiis omnis sequi.', 34.17, 17, 2, '2020-11-07 23:48:19', '2020-11-07 23:48:19'),
(68, 'Green', 'Commodi deserunt quis harum.', 29.99, 9, 1, '2020-11-07 23:48:19', '2020-11-07 23:48:19'),
(69, '1Kg', 'Ipsam consequatur ratione.', 31.29, 4, 4, '2020-11-07 23:48:19', '2020-11-07 23:48:19'),
(70, 'White', 'Illo neque incidunt qui modi.', 41.12, 3, 2, '2020-11-07 23:48:19', '2020-11-07 23:48:19');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `option_groups`
--

CREATE TABLE `option_groups` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(127) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `option_groups`
--

INSERT INTO `option_groups` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Size', '2019-08-31 14:55:28', '2019-08-31 14:55:28'),
(2, 'Color', '2019-10-09 17:26:28', '2019-10-09 17:26:28'),
(4, 'Taste', '2019-10-09 17:26:28', '2019-10-09 17:26:28');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `orders`
--

CREATE TABLE `orders` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `order_status_id` int(10) UNSIGNED NOT NULL,
  `tax` double(5,2) DEFAULT 0.00,
  `delivery_fee` double(5,2) DEFAULT 0.00,
  `hint` text COLLATE utf8mb4_unicode_ci DEFAULT '',
  `active` tinyint(1) NOT NULL DEFAULT 1,
  `driver_id` int(10) UNSIGNED DEFAULT NULL,
  `delivery_address_id` int(10) UNSIGNED DEFAULT NULL,
  `payment_id` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `orders`
--

INSERT INTO `orders` (`id`, `user_id`, `order_status_id`, `tax`, `delivery_fee`, `hint`, `active`, `driver_id`, `delivery_address_id`, `payment_id`, `created_at`, `updated_at`) VALUES
(1, 3, 5, 0.00, 0.00, NULL, 1, 5, NULL, 1, '2020-11-28 02:10:34', '2020-11-28 02:24:11'),
(2, 3, 5, 0.00, 5.00, NULL, 1, 5, 10, 2, '2020-11-28 02:44:34', '2020-11-28 02:48:51'),
(3, 4, 1, 0.00, 5.00, NULL, 1, NULL, 17, 3, '2020-11-28 03:46:38', '2020-11-28 03:46:38'),
(4, 3, 1, 0.00, 0.00, NULL, 1, NULL, NULL, 4, '2021-01-04 16:14:30', '2021-01-04 16:14:30'),
(5, 3, 1, 0.00, 0.00, NULL, 1, NULL, NULL, 5, '2021-01-04 16:27:46', '2021-01-04 16:27:46'),
(6, 3, 1, 7.66, 5.00, NULL, 1, NULL, 18, 6, '2021-01-06 14:50:54', '2021-01-06 14:50:54'),
(7, 3, 1, 7.66, 0.00, NULL, 1, NULL, 18, 7, '2021-01-06 14:51:47', '2021-01-06 14:51:47'),
(8, 3, 1, 0.00, 0.00, NULL, 1, NULL, NULL, 8, '2021-01-10 23:01:33', '2021-01-10 23:01:33'),
(9, 3, 1, 0.00, 0.00, NULL, 1, NULL, 19, 9, '2021-01-14 22:42:33', '2021-01-14 22:42:34'),
(10, 3, 1, 0.00, 0.00, NULL, 1, NULL, 19, 10, '2021-01-14 22:46:34', '2021-01-14 22:46:34'),
(11, 3, 1, 0.00, 0.00, NULL, 1, NULL, 10, 11, '2021-01-14 22:49:07', '2021-01-14 22:49:07'),
(12, 3, 1, 0.00, 0.00, NULL, 1, NULL, 19, NULL, '2021-01-14 23:06:22', '2021-01-14 23:06:22'),
(13, 3, 1, 0.00, 0.00, NULL, 1, NULL, 19, NULL, '2021-01-14 23:07:24', '2021-01-14 23:07:24'),
(14, 3, 1, 0.00, 0.00, NULL, 1, NULL, 19, NULL, '2021-01-14 23:07:28', '2021-01-14 23:07:28'),
(15, 3, 1, 0.00, 0.00, NULL, 1, NULL, 19, NULL, '2021-01-14 23:07:32', '2021-01-14 23:07:32'),
(16, 3, 1, 0.00, 0.00, NULL, 1, NULL, 19, NULL, '2021-01-14 23:18:26', '2021-01-14 23:18:26'),
(17, 3, 1, 0.00, 0.00, NULL, 1, NULL, 10, NULL, '2021-01-14 23:38:35', '2021-01-14 23:38:35'),
(18, 3, 1, 0.00, 0.00, NULL, 1, NULL, 10, NULL, '2021-01-14 23:39:46', '2021-01-14 23:39:46'),
(19, 3, 1, 0.00, 0.00, NULL, 1, NULL, 19, 12, '2021-01-14 23:42:30', '2021-01-14 23:42:30'),
(20, 3, 1, 0.00, 0.00, NULL, 1, NULL, 10, NULL, '2021-01-14 23:43:17', '2021-01-14 23:43:17'),
(21, 3, 1, 0.00, 0.00, NULL, 1, NULL, 19, 13, '2021-01-14 23:46:41', '2021-01-14 23:46:42'),
(22, 3, 1, 0.00, 0.00, NULL, 1, NULL, 19, NULL, '2021-01-14 23:47:03', '2021-01-14 23:47:03'),
(23, 3, 1, 0.00, 0.00, NULL, 1, NULL, 10, NULL, '2021-01-14 23:48:02', '2021-01-14 23:48:02'),
(24, 3, 1, 0.00, 0.00, NULL, 1, NULL, 19, 14, '2021-01-14 23:49:08', '2021-01-14 23:49:09'),
(25, 3, 1, 0.00, 5.00, NULL, 1, NULL, 19, 15, '2021-01-14 23:49:54', '2021-01-14 23:49:55'),
(26, 3, 1, 0.00, 0.00, NULL, 1, NULL, 10, NULL, '2021-01-14 23:54:18', '2021-01-14 23:54:18'),
(27, 3, 1, 0.00, 0.00, NULL, 1, NULL, 19, NULL, '2021-01-14 23:56:51', '2021-01-14 23:56:51'),
(28, 3, 1, 0.00, 0.00, NULL, 1, NULL, 10, NULL, '2021-01-14 23:59:10', '2021-01-14 23:59:10'),
(29, 3, 1, 0.00, 0.00, NULL, 1, NULL, 10, NULL, '2021-01-15 00:00:42', '2021-01-15 00:00:42'),
(30, 3, 1, 0.00, 0.00, NULL, 1, NULL, 10, NULL, '2021-01-15 00:01:21', '2021-01-15 00:01:21'),
(31, 3, 1, 0.00, 0.00, NULL, 1, NULL, 10, NULL, '2021-01-15 00:01:34', '2021-01-15 00:01:34'),
(32, 3, 1, 0.00, 0.00, NULL, 1, NULL, 10, NULL, '2021-01-15 00:03:49', '2021-01-15 00:03:49'),
(33, 3, 1, 0.00, 0.00, NULL, 1, NULL, 10, 16, '2021-01-15 00:36:17', '2021-01-15 00:36:21'),
(34, 19, 1, 0.00, 0.00, NULL, 1, NULL, NULL, 17, '2021-03-25 01:31:52', '2021-03-25 01:31:52'),
(35, 22, 1, 0.00, 0.00, NULL, 1, NULL, NULL, 18, '2021-03-25 01:52:32', '2021-03-25 01:52:32'),
(36, 22, 1, 0.00, 0.00, NULL, 1, NULL, NULL, 19, '2021-03-26 04:38:21', '2021-03-26 04:38:21');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `order_statuses`
--

CREATE TABLE `order_statuses` (
  `id` int(10) UNSIGNED NOT NULL,
  `status` varchar(127) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `order_statuses`
--

INSERT INTO `order_statuses` (`id`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Order Received', '2019-08-30 20:39:28', '2019-10-15 22:03:14'),
(2, 'Preparing', '2019-10-15 22:03:50', '2019-10-15 22:03:50'),
(3, 'Ready', '2019-10-15 22:04:30', '2019-10-15 22:04:30'),
(4, 'On the Way', '2019-10-15 22:04:13', '2019-10-15 22:04:13'),
(5, 'Delivered', '2019-10-15 22:04:30', '2019-10-15 22:04:30');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `payments`
--

CREATE TABLE `payments` (
  `id` int(10) UNSIGNED NOT NULL,
  `price` double(8,2) NOT NULL DEFAULT 0.00,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `method` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `payments`
--

INSERT INTO `payments` (`id`, `price`, `description`, `user_id`, `status`, `method`, `created_at`, `updated_at`) VALUES
(1, 30.05, 'Order not paid yet', 3, 'Paid', 'Pay on Pickup', '2020-11-28 02:10:34', '2020-11-28 02:24:11'),
(2, 50.28, 'Order not paid yet', 3, 'Paid', 'Cash on Delivery', '2020-11-28 02:44:34', '2020-11-28 02:45:31'),
(3, 34.29, 'Order not paid yet', 4, 'Waiting for Client', 'Cash on Delivery', '2020-11-28 03:46:38', '2020-11-28 03:46:38'),
(4, 30.05, 'Order not paid yet', 3, 'Waiting for Client', 'Pay on Pickup', '2021-01-04 16:14:30', '2021-01-04 16:14:30'),
(5, 30.05, 'Order not paid yet', 3, 'Waiting for Client', 'Pay on Pickup', '2021-01-04 16:27:46', '2021-01-04 16:27:46'),
(6, 42.85, 'Order not paid yet', 3, 'Waiting for Client', 'Cash on Delivery', '2021-01-06 14:50:54', '2021-01-06 14:50:54'),
(7, 74.93, 'Order not paid yet', 3, 'Waiting for Client', 'Pay on Pickup', '2021-01-06 14:51:47', '2021-01-06 14:51:47'),
(8, 30.05, 'Order not paid yet', 3, 'Waiting for Client', 'Pay on Pickup', '2021-01-10 23:01:33', '2021-01-10 23:01:33'),
(9, 1.00, 'Order not paid yet', 3, 'Waiting for Client', 'Credit Card', '2021-01-14 22:42:34', '2021-01-14 22:42:34'),
(10, 1.00, 'Order not paid yet', 3, 'Waiting for Client', 'Credit Card (Stripe Gateway)', '2021-01-14 22:46:34', '2021-01-14 22:46:34'),
(11, 0.50, 'Order not paid yet', 3, 'Waiting for Client', 'Credit Card (Stripe Gateway)', '2021-01-14 22:49:07', '2021-01-14 22:49:07'),
(12, 1.00, 'Order not paid yet', 3, 'Waiting for Client', 'Credit Card', '2021-01-14 23:42:30', '2021-01-14 23:42:30'),
(13, 1.00, 'Order paid successfully', 3, 'succeeded', 'Credit Card (Stripe Gateway)', '2021-01-14 23:46:42', '2021-01-14 23:46:42'),
(14, 1.00, 'Order paid successfully', 3, 'succeeded', 'Credit Card (Stripe Gateway)', '2021-01-14 23:49:09', '2021-01-14 23:49:09'),
(15, 35.05, 'Order paid successfully', 3, 'succeeded', 'Credit Card (Stripe Gateway)', '2021-01-14 23:49:55', '2021-01-14 23:49:55'),
(16, 0.50, 'Order paid successfully', 3, 'succeeded', 'Credit Card (Stripe Gateway)', '2021-01-15 00:36:21', '2021-01-15 00:36:21'),
(17, 25.50, 'Order not paid yet', 19, 'Waiting for Client', 'Pay on Pickup', '2021-03-25 01:31:52', '2021-03-25 01:31:52'),
(18, 3.32, 'Order not paid yet', 22, 'Waiting for Client', 'Pay on Pickup', '2021-03-25 01:52:32', '2021-03-25 01:52:32'),
(19, 1.30, 'Order not paid yet', 22, 'Waiting for Client', 'Pay on Pickup', '2021-03-26 04:38:21', '2021-03-26 04:38:21');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `permissions`
--

CREATE TABLE `permissions` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'web',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `permissions`
--

INSERT INTO `permissions` (`id`, `name`, `guard_name`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'users.profile', 'web', '2020-03-29 18:58:02', '2020-03-29 18:58:02', NULL),
(2, 'dashboard', 'web', '2020-03-29 18:58:02', '2020-03-29 18:58:02', NULL),
(3, 'medias.create', 'web', '2020-03-29 18:58:02', '2020-03-29 18:58:02', NULL),
(4, 'medias.delete', 'web', '2020-03-29 18:58:02', '2020-03-29 18:58:02', NULL),
(5, 'medias', 'web', '2020-03-29 18:58:03', '2020-03-29 18:58:03', NULL),
(6, 'permissions.index', 'web', '2020-03-29 18:58:03', '2020-03-29 18:58:03', NULL),
(7, 'permissions.edit', 'web', '2020-03-29 18:58:03', '2020-03-29 18:58:03', NULL),
(8, 'permissions.update', 'web', '2020-03-29 18:58:03', '2020-03-29 18:58:03', NULL),
(9, 'permissions.destroy', 'web', '2020-03-29 18:58:03', '2020-03-29 18:58:03', NULL),
(10, 'roles.index', 'web', '2020-03-29 18:58:03', '2020-03-29 18:58:03', NULL),
(11, 'roles.edit', 'web', '2020-03-29 18:58:03', '2020-03-29 18:58:03', NULL),
(12, 'roles.update', 'web', '2020-03-29 18:58:03', '2020-03-29 18:58:03', NULL),
(13, 'roles.destroy', 'web', '2020-03-29 18:58:03', '2020-03-29 18:58:03', NULL),
(14, 'customFields.index', 'web', '2020-03-29 18:58:03', '2020-03-29 18:58:03', NULL),
(15, 'customFields.create', 'web', '2020-03-29 18:58:03', '2020-03-29 18:58:03', NULL),
(16, 'customFields.store', 'web', '2020-03-29 18:58:03', '2020-03-29 18:58:03', NULL),
(17, 'customFields.show', 'web', '2020-03-29 18:58:03', '2020-03-29 18:58:03', NULL),
(18, 'customFields.edit', 'web', '2020-03-29 18:58:03', '2020-03-29 18:58:03', NULL),
(19, 'customFields.update', 'web', '2020-03-29 18:58:04', '2020-03-29 18:58:04', NULL),
(20, 'customFields.destroy', 'web', '2020-03-29 18:58:04', '2020-03-29 18:58:04', NULL),
(21, 'users.login-as-user', 'web', '2020-03-29 18:58:04', '2020-03-29 18:58:04', NULL),
(22, 'users.index', 'web', '2020-03-29 18:58:04', '2020-03-29 18:58:04', NULL),
(23, 'users.create', 'web', '2020-03-29 18:58:04', '2020-03-29 18:58:04', NULL),
(24, 'users.store', 'web', '2020-03-29 18:58:04', '2020-03-29 18:58:04', NULL),
(25, 'users.show', 'web', '2020-03-29 18:58:04', '2020-03-29 18:58:04', NULL),
(26, 'users.edit', 'web', '2020-03-29 18:58:04', '2020-03-29 18:58:04', NULL),
(27, 'users.update', 'web', '2020-03-29 18:58:04', '2020-03-29 18:58:04', NULL),
(28, 'users.destroy', 'web', '2020-03-29 18:58:04', '2020-03-29 18:58:04', NULL),
(29, 'app-settings', 'web', '2020-03-29 18:58:04', '2020-03-29 18:58:04', NULL),
(30, 'markets.index', 'web', '2020-03-29 18:58:04', '2020-03-29 18:58:04', NULL),
(31, 'markets.create', 'web', '2020-03-29 18:58:04', '2020-03-29 18:58:04', NULL),
(32, 'markets.store', 'web', '2020-03-29 18:58:04', '2020-03-29 18:58:04', NULL),
(33, 'markets.edit', 'web', '2020-03-29 18:58:04', '2020-03-29 18:58:04', NULL),
(34, 'markets.update', 'web', '2020-03-29 18:58:05', '2020-03-29 18:58:05', NULL),
(35, 'markets.destroy', 'web', '2020-03-29 18:58:05', '2020-03-29 18:58:05', NULL),
(36, 'categories.index', 'web', '2020-03-29 18:58:05', '2020-03-29 18:58:05', NULL),
(37, 'categories.create', 'web', '2020-03-29 18:58:05', '2020-03-29 18:58:05', NULL),
(38, 'categories.store', 'web', '2020-03-29 18:58:05', '2020-03-29 18:58:05', NULL),
(39, 'categories.edit', 'web', '2020-03-29 18:58:05', '2020-03-29 18:58:05', NULL),
(40, 'categories.update', 'web', '2020-03-29 18:58:05', '2020-03-29 18:58:05', NULL),
(41, 'categories.destroy', 'web', '2020-03-29 18:58:05', '2020-03-29 18:58:05', NULL),
(42, 'faqCategories.index', 'web', '2020-03-29 18:58:06', '2020-03-29 18:58:06', NULL),
(43, 'faqCategories.create', 'web', '2020-03-29 18:58:06', '2020-03-29 18:58:06', NULL),
(44, 'faqCategories.store', 'web', '2020-03-29 18:58:06', '2020-03-29 18:58:06', NULL),
(45, 'faqCategories.edit', 'web', '2020-03-29 18:58:06', '2020-03-29 18:58:06', NULL),
(46, 'faqCategories.update', 'web', '2020-03-29 18:58:06', '2020-03-29 18:58:06', NULL),
(47, 'faqCategories.destroy', 'web', '2020-03-29 18:58:06', '2020-03-29 18:58:06', NULL),
(48, 'orderStatuses.index', 'web', '2020-03-29 18:58:06', '2020-03-29 18:58:06', NULL),
(49, 'orderStatuses.show', 'web', '2020-03-29 18:58:06', '2020-03-29 18:58:06', NULL),
(50, 'orderStatuses.edit', 'web', '2020-03-29 18:58:06', '2020-03-29 18:58:06', NULL),
(51, 'orderStatuses.update', 'web', '2020-03-29 18:58:07', '2020-03-29 18:58:07', NULL),
(52, 'products.index', 'web', '2020-03-29 18:58:07', '2020-03-29 18:58:07', NULL),
(53, 'products.create', 'web', '2020-03-29 18:58:07', '2020-03-29 18:58:07', NULL),
(54, 'products.store', 'web', '2020-03-29 18:58:07', '2020-03-29 18:58:07', NULL),
(55, 'products.edit', 'web', '2020-03-29 18:58:07', '2020-03-29 18:58:07', NULL),
(56, 'products.update', 'web', '2020-03-29 18:58:07', '2020-03-29 18:58:07', NULL),
(57, 'products.destroy', 'web', '2020-03-29 18:58:07', '2020-03-29 18:58:07', NULL),
(58, 'galleries.index', 'web', '2020-03-29 18:58:07', '2020-03-29 18:58:07', NULL),
(59, 'galleries.create', 'web', '2020-03-29 18:58:07', '2020-03-29 18:58:07', NULL),
(60, 'galleries.store', 'web', '2020-03-29 18:58:08', '2020-03-29 18:58:08', NULL),
(61, 'galleries.edit', 'web', '2020-03-29 18:58:08', '2020-03-29 18:58:08', NULL),
(62, 'galleries.update', 'web', '2020-03-29 18:58:08', '2020-03-29 18:58:08', NULL),
(63, 'galleries.destroy', 'web', '2020-03-29 18:58:08', '2020-03-29 18:58:08', NULL),
(64, 'productReviews.index', 'web', '2020-03-29 18:58:08', '2020-03-29 18:58:08', NULL),
(65, 'productReviews.create', 'web', '2020-03-29 18:58:08', '2020-03-29 18:58:08', NULL),
(66, 'productReviews.store', 'web', '2020-03-29 18:58:08', '2020-03-29 18:58:08', NULL),
(67, 'productReviews.edit', 'web', '2020-03-29 18:58:08', '2020-03-29 18:58:08', NULL),
(68, 'productReviews.update', 'web', '2020-03-29 18:58:08', '2020-03-29 18:58:08', NULL),
(69, 'productReviews.destroy', 'web', '2020-03-29 18:58:08', '2020-03-29 18:58:08', NULL),
(76, 'options.index', 'web', '2020-03-29 18:58:09', '2020-03-29 18:58:09', NULL),
(77, 'options.create', 'web', '2020-03-29 18:58:09', '2020-03-29 18:58:09', NULL),
(78, 'options.store', 'web', '2020-03-29 18:58:09', '2020-03-29 18:58:09', NULL),
(79, 'options.show', 'web', '2020-03-29 18:58:10', '2020-03-29 18:58:10', NULL),
(80, 'options.edit', 'web', '2020-03-29 18:58:10', '2020-03-29 18:58:10', NULL),
(81, 'options.update', 'web', '2020-03-29 18:58:10', '2020-03-29 18:58:10', NULL),
(82, 'options.destroy', 'web', '2020-03-29 18:58:10', '2020-03-29 18:58:10', NULL),
(83, 'payments.index', 'web', '2020-03-29 18:58:10', '2020-03-29 18:58:10', NULL),
(84, 'payments.show', 'web', '2020-03-29 18:58:10', '2020-03-29 18:58:10', NULL),
(85, 'payments.update', 'web', '2020-03-29 18:58:10', '2020-03-29 18:58:10', NULL),
(86, 'faqs.index', 'web', '2020-03-29 18:58:10', '2020-03-29 18:58:10', NULL),
(87, 'faqs.create', 'web', '2020-03-29 18:58:11', '2020-03-29 18:58:11', NULL),
(88, 'faqs.store', 'web', '2020-03-29 18:58:11', '2020-03-29 18:58:11', NULL),
(89, 'faqs.edit', 'web', '2020-03-29 18:58:11', '2020-03-29 18:58:11', NULL),
(90, 'faqs.update', 'web', '2020-03-29 18:58:11', '2020-03-29 18:58:11', NULL),
(91, 'faqs.destroy', 'web', '2020-03-29 18:58:11', '2020-03-29 18:58:11', NULL),
(92, 'marketReviews.index', 'web', '2020-03-29 18:58:11', '2020-03-29 18:58:11', NULL),
(93, 'marketReviews.create', 'web', '2020-03-29 18:58:11', '2020-03-29 18:58:11', NULL),
(94, 'marketReviews.store', 'web', '2020-03-29 18:58:12', '2020-03-29 18:58:12', NULL),
(95, 'marketReviews.edit', 'web', '2020-03-29 18:58:12', '2020-03-29 18:58:12', NULL),
(96, 'marketReviews.update', 'web', '2020-03-29 18:58:12', '2020-03-29 18:58:12', NULL),
(97, 'marketReviews.destroy', 'web', '2020-03-29 18:58:12', '2020-03-29 18:58:12', NULL),
(98, 'favorites.index', 'web', '2020-03-29 18:58:12', '2020-03-29 18:58:12', NULL),
(99, 'favorites.create', 'web', '2020-03-29 18:58:12', '2020-03-29 18:58:12', NULL),
(100, 'favorites.store', 'web', '2020-03-29 18:58:12', '2020-03-29 18:58:12', NULL),
(101, 'favorites.edit', 'web', '2020-03-29 18:58:12', '2020-03-29 18:58:12', NULL),
(102, 'favorites.update', 'web', '2020-03-29 18:58:12', '2020-03-29 18:58:12', NULL),
(103, 'favorites.destroy', 'web', '2020-03-29 18:58:13', '2020-03-29 18:58:13', NULL),
(104, 'orders.index', 'web', '2020-03-29 18:58:13', '2020-03-29 18:58:13', NULL),
(105, 'orders.create', 'web', '2020-03-29 18:58:13', '2020-03-29 18:58:13', NULL),
(106, 'orders.store', 'web', '2020-03-29 18:58:13', '2020-03-29 18:58:13', NULL),
(107, 'orders.show', 'web', '2020-03-29 18:58:13', '2020-03-29 18:58:13', NULL),
(108, 'orders.edit', 'web', '2020-03-29 18:58:13', '2020-03-29 18:58:13', NULL),
(109, 'orders.update', 'web', '2020-03-29 18:58:13', '2020-03-29 18:58:13', NULL),
(110, 'orders.destroy', 'web', '2020-03-29 18:58:13', '2020-03-29 18:58:13', NULL),
(111, 'notifications.index', 'web', '2020-03-29 18:58:14', '2020-03-29 18:58:14', NULL),
(112, 'notifications.show', 'web', '2020-03-29 18:58:14', '2020-03-29 18:58:14', NULL),
(113, 'notifications.destroy', 'web', '2020-03-29 18:58:14', '2020-03-29 18:58:14', NULL),
(114, 'carts.index', 'web', '2020-03-29 18:58:14', '2020-03-29 18:58:14', NULL),
(115, 'carts.edit', 'web', '2020-03-29 18:58:14', '2020-03-29 18:58:14', NULL),
(116, 'carts.update', 'web', '2020-03-29 18:58:14', '2020-03-29 18:58:14', NULL),
(117, 'carts.destroy', 'web', '2020-03-29 18:58:14', '2020-03-29 18:58:14', NULL),
(118, 'currencies.index', 'web', '2020-03-29 18:58:14', '2020-03-29 18:58:14', NULL),
(119, 'currencies.create', 'web', '2020-03-29 18:58:15', '2020-03-29 18:58:15', NULL),
(120, 'currencies.store', 'web', '2020-03-29 18:58:15', '2020-03-29 18:58:15', NULL),
(121, 'currencies.edit', 'web', '2020-03-29 18:58:15', '2020-03-29 18:58:15', NULL),
(122, 'currencies.update', 'web', '2020-03-29 18:58:15', '2020-03-29 18:58:15', NULL),
(123, 'currencies.destroy', 'web', '2020-03-29 18:58:15', '2020-03-29 18:58:15', NULL),
(124, 'deliveryAddresses.index', 'web', '2020-03-29 18:58:15', '2020-03-29 18:58:15', NULL),
(125, 'deliveryAddresses.create', 'web', '2020-03-29 18:58:15', '2020-03-29 18:58:15', NULL),
(126, 'deliveryAddresses.store', 'web', '2020-03-29 18:58:15', '2020-03-29 18:58:15', NULL),
(127, 'deliveryAddresses.edit', 'web', '2020-03-29 18:58:16', '2020-03-29 18:58:16', NULL),
(128, 'deliveryAddresses.update', 'web', '2020-03-29 18:58:16', '2020-03-29 18:58:16', NULL),
(129, 'deliveryAddresses.destroy', 'web', '2020-03-29 18:58:16', '2020-03-29 18:58:16', NULL),
(130, 'drivers.index', 'web', '2020-03-29 18:58:16', '2020-03-29 18:58:16', NULL),
(131, 'drivers.create', 'web', '2020-03-29 18:58:16', '2020-03-29 18:58:16', NULL),
(132, 'drivers.store', 'web', '2020-03-29 18:58:16', '2020-03-29 18:58:16', NULL),
(133, 'drivers.show', 'web', '2020-03-29 18:58:16', '2020-03-29 18:58:16', NULL),
(134, 'drivers.edit', 'web', '2020-03-29 18:58:16', '2020-03-29 18:58:16', NULL),
(135, 'drivers.update', 'web', '2020-03-29 18:58:16', '2020-03-29 18:58:16', NULL),
(136, 'drivers.destroy', 'web', '2020-03-29 18:58:17', '2020-03-29 18:58:17', NULL),
(137, 'earnings.index', 'web', '2020-03-29 18:58:17', '2020-03-29 18:58:17', NULL),
(138, 'earnings.create', 'web', '2020-03-29 18:58:17', '2020-03-29 18:58:17', NULL),
(139, 'earnings.store', 'web', '2020-03-29 18:58:17', '2020-03-29 18:58:17', NULL),
(140, 'earnings.show', 'web', '2020-03-29 18:58:17', '2020-03-29 18:58:17', NULL),
(141, 'earnings.edit', 'web', '2020-03-29 18:58:17', '2020-03-29 18:58:17', NULL),
(142, 'earnings.update', 'web', '2020-03-29 18:58:17', '2020-03-29 18:58:17', NULL),
(143, 'earnings.destroy', 'web', '2020-03-29 18:58:17', '2020-03-29 18:58:17', NULL),
(144, 'driversPayouts.index', 'web', '2020-03-29 18:58:17', '2020-03-29 18:58:17', NULL),
(145, 'driversPayouts.create', 'web', '2020-03-29 18:58:17', '2020-03-29 18:58:17', NULL),
(146, 'driversPayouts.store', 'web', '2020-03-29 18:58:18', '2020-03-29 18:58:18', NULL),
(147, 'driversPayouts.show', 'web', '2020-03-29 18:58:18', '2020-03-29 18:58:18', NULL),
(148, 'driversPayouts.edit', 'web', '2020-03-29 18:58:18', '2020-03-29 18:58:18', NULL),
(149, 'driversPayouts.update', 'web', '2020-03-29 18:58:18', '2020-03-29 18:58:18', NULL),
(150, 'driversPayouts.destroy', 'web', '2020-03-29 18:58:18', '2020-03-29 18:58:18', NULL),
(151, 'marketsPayouts.index', 'web', '2020-03-29 18:58:18', '2020-03-29 18:58:18', NULL),
(152, 'marketsPayouts.create', 'web', '2020-03-29 18:58:18', '2020-03-29 18:58:18', NULL),
(153, 'marketsPayouts.store', 'web', '2020-03-29 18:58:18', '2020-03-29 18:58:18', NULL),
(154, 'marketsPayouts.show', 'web', '2020-03-29 18:58:18', '2020-03-29 18:58:18', NULL),
(155, 'marketsPayouts.edit', 'web', '2020-03-29 18:58:18', '2020-03-29 18:58:18', NULL),
(156, 'marketsPayouts.update', 'web', '2020-03-29 18:58:19', '2020-03-29 18:58:19', NULL),
(157, 'marketsPayouts.destroy', 'web', '2020-03-29 18:58:19', '2020-03-29 18:58:19', NULL),
(158, 'permissions.create', 'web', '2020-03-29 18:59:15', '2020-03-29 18:59:15', NULL),
(159, 'permissions.store', 'web', '2020-03-29 18:59:15', '2020-03-29 18:59:15', NULL),
(160, 'permissions.show', 'web', '2020-03-29 18:59:15', '2020-03-29 18:59:15', NULL),
(161, 'roles.create', 'web', '2020-03-29 18:59:15', '2020-03-29 18:59:15', NULL),
(162, 'roles.store', 'web', '2020-03-29 18:59:15', '2020-03-29 18:59:15', NULL),
(163, 'roles.show', 'web', '2020-03-29 18:59:16', '2020-03-29 18:59:16', NULL),
(164, 'fields.index', 'web', '2020-04-11 19:04:39', '2020-04-11 19:04:39', NULL),
(165, 'fields.create', 'web', '2020-04-11 19:04:39', '2020-04-11 19:04:39', NULL),
(166, 'fields.store', 'web', '2020-04-11 19:04:39', '2020-04-11 19:04:39', NULL),
(167, 'fields.edit', 'web', '2020-04-11 19:04:39', '2020-04-11 19:04:39', NULL),
(168, 'fields.update', 'web', '2020-04-11 19:04:39', '2020-04-11 19:04:39', NULL),
(169, 'fields.destroy', 'web', '2020-04-11 19:04:40', '2020-04-11 19:04:40', NULL),
(170, 'optionGroups.index', 'web', '2020-04-11 19:04:40', '2020-04-11 19:04:40', NULL),
(171, 'optionGroups.create', 'web', '2020-04-11 19:04:40', '2020-04-11 19:04:40', NULL),
(172, 'optionGroups.store', 'web', '2020-04-11 19:04:40', '2020-04-11 19:04:40', NULL),
(173, 'optionGroups.edit', 'web', '2020-04-11 19:04:40', '2020-04-11 19:04:40', NULL),
(174, 'optionGroups.update', 'web', '2020-04-11 19:04:40', '2020-04-11 19:04:40', NULL),
(175, 'optionGroups.destroy', 'web', '2020-04-11 19:04:40', '2020-04-11 19:04:40', NULL),
(176, 'requestedMarkets.index', 'web', '2020-08-13 18:58:02', '2020-08-13 18:58:02', NULL),
(183, 'coupons.index', 'web', '2020-08-23 18:58:02', '2020-08-23 18:58:02', NULL),
(184, 'coupons.create', 'web', '2020-08-23 18:58:02', '2020-08-23 18:58:02', NULL),
(185, 'coupons.store', 'web', '2020-08-23 18:58:02', '2020-08-23 18:58:02', NULL),
(186, 'coupons.edit', 'web', '2020-08-23 18:58:02', '2020-08-23 18:58:02', NULL),
(187, 'coupons.update', 'web', '2020-08-23 18:58:02', '2020-08-23 18:58:02', NULL),
(188, 'coupons.destroy', 'web', '2020-08-23 18:58:02', '2020-08-23 18:58:02', NULL),
(189, 'slides.index', 'web', '2020-08-23 18:58:02', '2020-08-23 18:58:02', NULL),
(190, 'slides.create', 'web', '2020-08-23 18:58:02', '2020-08-23 18:58:02', NULL),
(191, 'slides.store', 'web', '2020-08-23 18:58:02', '2020-08-23 18:58:02', NULL),
(192, 'slides.edit', 'web', '2020-08-23 18:58:02', '2020-08-23 18:58:02', NULL),
(193, 'slides.update', 'web', '2020-08-23 18:58:02', '2020-08-23 18:58:02', NULL),
(194, 'slides.destroy', 'web', '2020-08-23 18:58:02', '2020-08-23 18:58:02', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `products`
--

CREATE TABLE `products` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(127) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` double(8,2) NOT NULL DEFAULT 0.00,
  `discount_price` double(8,2) DEFAULT 0.00,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT '',
  `capacity` double(9,2) DEFAULT 0.00,
  `package_items_count` double(9,2) DEFAULT 0.00,
  `unit` varchar(127) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `featured` tinyint(1) DEFAULT 0,
  `deliverable` tinyint(1) DEFAULT 1,
  `market_id` int(10) UNSIGNED NOT NULL,
  `category_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `products`
--

INSERT INTO `products` (`id`, `name`, `price`, `discount_price`, `description`, `capacity`, `package_items_count`, `unit`, `featured`, `deliverable`, `market_id`, `category_id`, `created_at`, `updated_at`) VALUES
(41, 'Plava Sauce', 1.66, NULL, '<p>Frozen Minced Molokheya/Plava Sauce</p><p>3 for $5 USD. Keep in a cold area (refrigerator). </p>', 0.00, 1.00, 'kg', 1, 1, 11, 3, '2021-02-08 06:10:37', '2021-02-08 06:12:36'),
(42, 'Fufu (Plantain) Tropiway', 2.99, NULL, '<p>Tropiway Plantain Fufu  </p>', 0.68, 1.00, NULL, 1, 1, 11, 3, '2021-02-09 03:21:26', '2021-02-09 03:33:05'),
(43, 'Maggi Cube (Star Seasoning)', 5.75, 5.00, '<p>Maggi Cube (seasonings).&nbsp;</p>', 0.40, 100.00, NULL, 1, 1, 11, 3, '2021-02-09 03:31:57', '2021-02-09 03:31:57'),
(44, 'Praise (Abemu Dro) Palm Butter', 4.75, 4.25, '<p>Palm Butter/Palm Sauce</p>', NULL, 1.00, NULL, 1, 1, 11, 3, '2021-02-09 03:58:07', '2021-02-09 03:58:07'),
(45, 'Medium Couscous', 3.00, 2.75, '<p>Couscous by Ziyad</p><p>100% Moroccan</p>', 0.98, 1.00, 'Kg', 1, 1, 12, 3, '2021-02-22 01:59:51', '2021-02-22 01:59:51'),
(46, 'Pre-Cooked Palm Nut', 6.75, 6.50, '<p>Azur International </p><p>Graine Precuite</p>', NULL, 1.00, NULL, 1, 1, 12, 3, '2021-02-22 02:11:38', '2021-02-22 22:47:57'),
(47, 'Sawa Sawa Leaf', 5.00, 4.75, '<p>Product of Ivory Coast</p><p>Keep Frozen</p>', NULL, 1.00, NULL, 1, 1, 11, 3, '2021-02-22 02:15:06', '2021-02-22 02:29:24'),
(48, 'Cut Okra', 5.00, 4.75, '<p>James Farm: From our Farm to your Kitchen</p>', NULL, 1.00, NULL, 1, 1, 11, 3, '2021-02-22 02:17:58', '2021-02-22 02:17:58'),
(49, 'Tobogee oil', 5.75, NULL, '<p>Tobogee Oil</p>', NULL, 1.00, NULL, 1, 1, 11, 3, '2021-02-22 02:21:11', '2021-02-22 02:21:11'),
(50, 'Mutton & Goat', 5.00, 4.75, '<p>Bone-In Cubes</p><p>Keep Frozen</p>', 0.99, 8.00, 'kg', 1, 1, 12, 3, '2021-02-22 02:24:31', '2021-02-22 02:25:28'),
(51, 'Basma Plava Sauce', 5.00, 4.75, '<p>Frozen Minced Molokheya</p>', 0.40, 1.00, 'Kg', 1, 1, 11, 3, '2021-02-22 02:29:05', '2021-02-22 02:29:05'),
(52, 'Apetamin', 25.00, 19.99, '<p>Cyproheptadine Lysine and Vitamins Syrup</p>', 200.00, 1.00, 'ml', 1, 1, 11, 4, '2021-02-22 02:33:43', '2021-02-22 03:39:25'),
(53, 'Iyan Pounded Yam', 10.00, 7.75, '<p>Iyan Ado</p>', 1.81, 1.00, 'kg', 1, 1, 11, 3, '2021-02-22 02:36:39', '2021-02-22 03:37:07'),
(54, 'Titus Sardines', 1.50, 1.25, 'Sardines in Soybean Oil', 125.00, 1.00, 'mg', 1, 1, 11, 3, '2021-02-22 02:41:42', '2021-02-22 22:47:18'),
(55, 'Titus Sardines', 1.30, NULL, '<p>Sardines in soybean oil and chili pepper</p>', 125.00, 1.00, 'mg', 1, 1, 11, 3, '2021-02-22 02:44:35', '2021-02-22 03:37:44'),
(56, 'Exeter Corned Beef', 8.99, 5.99, '<p>Product of Brazil</p>', 340.00, 1.00, 'g', 1, 1, 11, 3, '2021-02-22 02:47:55', '2021-02-22 03:33:43'),
(57, 'Roasted Peanut Cake', 3.00, 2.00, '<p>MIK International Foods LLC</p>', 170.25, 3.00, 'g', 1, 1, 11, 3, '2021-02-22 02:52:22', '2021-02-22 03:24:29'),
(58, 'Cannia (Gari) Cake', 3.55, 2.99, '<p>MIK International Foods LLC</p>', 170.25, 4.00, 'g', 1, 1, 11, 3, '2021-02-22 02:57:23', '2021-02-22 03:32:50'),
(59, 'Trofai', 4.50, NULL, '<p>Palmnut Concentrate</p><p>Sauce Graine</p><p>Natural Palmnut Pulp for Soup</p>', 0.80, 1.00, 'kg', 1, 1, 11, 3, '2021-02-22 03:03:48', '2021-02-22 22:45:44'),
(60, 'Pink Salmon', 4.55, NULL, '<p>Bumble Bee Pink Salmon</p><p><br></p>', 0.42, 1.00, 'kg', 1, 1, 11, 3, '2021-02-22 03:07:28', '2021-02-22 03:35:25'),
(61, 'Goya Adoba: All Purpose Seasoning', 4.19, NULL, '<p>All Purpose Seasoning</p>', 0.45, 1.00, 'kg', 1, 1, 11, 3, '2021-02-22 03:11:11', '2021-02-22 03:32:10'),
(62, 'Soumaya & Sons Bakery: White Pita Bread', 3.99, NULL, '<p>White Pita Bread</p><p>Bakery: Soumaya & Sons</p>', 0.39, 6.00, 'kg', 1, 1, 11, 3, '2021-02-22 03:16:37', '2021-02-22 22:52:08');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `product_orders`
--

CREATE TABLE `product_orders` (
  `id` int(10) UNSIGNED NOT NULL,
  `price` double(8,2) NOT NULL DEFAULT 0.00,
  `quantity` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `product_id` int(10) UNSIGNED NOT NULL,
  `order_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `product_orders`
--

INSERT INTO `product_orders` (`id`, `price`, `quantity`, `product_id`, `order_id`, `created_at`, `updated_at`) VALUES
(1, 30.05, 1, 1, 1, '2020-11-28 02:10:34', '2020-11-28 02:10:34'),
(2, 22.64, 2, 23, 2, '2020-11-28 02:44:34', '2020-11-28 02:44:34'),
(3, 29.29, 1, 22, 3, '2020-11-28 03:46:38', '2020-11-28 03:46:38'),
(4, 30.05, 1, 1, 4, '2021-01-04 16:14:30', '2021-01-04 16:14:30'),
(5, 30.05, 1, 1, 5, '2021-01-04 16:27:46', '2021-01-04 16:27:46'),
(6, 34.80, 1, 28, 6, '2021-01-06 14:50:54', '2021-01-06 14:50:54'),
(7, 34.80, 1, 28, 7, '2021-01-06 14:51:47', '2021-01-06 14:51:47'),
(8, 34.80, 1, 28, 7, '2021-01-06 14:51:47', '2021-01-06 14:51:47'),
(9, 30.05, 1, 1, 8, '2021-01-10 23:01:33', '2021-01-10 23:01:33'),
(10, 0.50, 2, 28, 9, '2021-01-14 22:42:33', '2021-01-14 22:42:33'),
(11, 0.50, 2, 28, 10, '2021-01-14 22:46:34', '2021-01-14 22:46:34'),
(12, 0.50, 1, 28, 11, '2021-01-14 22:49:07', '2021-01-14 22:49:07'),
(13, 0.50, 2, 28, 12, '2021-01-14 23:06:22', '2021-01-14 23:06:22'),
(14, 0.50, 2, 28, 13, '2021-01-14 23:07:24', '2021-01-14 23:07:24'),
(15, 0.50, 2, 28, 14, '2021-01-14 23:07:28', '2021-01-14 23:07:28'),
(16, 0.50, 2, 28, 15, '2021-01-14 23:07:32', '2021-01-14 23:07:32'),
(17, 0.50, 2, 28, 16, '2021-01-14 23:18:26', '2021-01-14 23:18:26'),
(18, 0.50, 2, 28, 17, '2021-01-14 23:38:35', '2021-01-14 23:38:35'),
(19, 0.50, 1, 28, 18, '2021-01-14 23:39:46', '2021-01-14 23:39:46'),
(20, 0.50, 2, 28, 19, '2021-01-14 23:42:30', '2021-01-14 23:42:30'),
(21, 0.50, 1, 28, 20, '2021-01-14 23:43:17', '2021-01-14 23:43:17'),
(22, 0.50, 2, 28, 21, '2021-01-14 23:46:41', '2021-01-14 23:46:41'),
(23, 0.50, 1, 28, 22, '2021-01-14 23:47:03', '2021-01-14 23:47:03'),
(24, 0.50, 1, 28, 23, '2021-01-14 23:48:02', '2021-01-14 23:48:02'),
(25, 0.50, 2, 28, 24, '2021-01-14 23:49:08', '2021-01-14 23:49:08'),
(26, 30.05, 1, 1, 25, '2021-01-14 23:49:54', '2021-01-14 23:49:54'),
(27, 0.50, 1, 28, 26, '2021-01-14 23:54:18', '2021-01-14 23:54:18'),
(28, 0.50, 1, 28, 27, '2021-01-14 23:56:51', '2021-01-14 23:56:51'),
(29, 0.50, 1, 28, 28, '2021-01-14 23:59:10', '2021-01-14 23:59:10'),
(30, 0.50, 1, 28, 29, '2021-01-15 00:00:42', '2021-01-15 00:00:42'),
(31, 0.50, 1, 28, 30, '2021-01-15 00:01:21', '2021-01-15 00:01:21'),
(32, 0.50, 1, 28, 31, '2021-01-15 00:01:34', '2021-01-15 00:01:34'),
(33, 0.50, 1, 28, 32, '2021-01-15 00:03:49', '2021-01-15 00:03:49'),
(34, 0.50, 1, 28, 33, '2021-01-15 00:36:17', '2021-01-15 00:36:17'),
(35, 4.25, 5, 44, 34, '2021-03-25 01:31:52', '2021-03-25 01:31:52'),
(36, 4.25, 1, 44, 34, '2021-03-25 01:31:52', '2021-03-25 01:31:52'),
(37, 1.66, 2, 41, 35, '2021-03-25 01:52:32', '2021-03-25 01:52:32'),
(38, 1.30, 1, 55, 36, '2021-03-26 04:38:21', '2021-03-26 04:38:21');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `product_order_options`
--

CREATE TABLE `product_order_options` (
  `product_order_id` int(10) UNSIGNED NOT NULL,
  `option_id` int(10) UNSIGNED NOT NULL,
  `price` double(8,2) NOT NULL DEFAULT 0.00
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `product_reviews`
--

CREATE TABLE `product_reviews` (
  `id` int(10) UNSIGNED NOT NULL,
  `review` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `rate` tinyint(3) UNSIGNED NOT NULL DEFAULT 0,
  `user_id` int(10) UNSIGNED NOT NULL,
  `product_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'web',
  `default` tinyint(1) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `roles`
--

INSERT INTO `roles` (`id`, `name`, `guard_name`, `default`, `created_at`, `updated_at`, `deleted_at`) VALUES
(2, 'admin', 'web', 0, '2018-07-21 20:37:56', '2019-09-08 02:42:01', NULL),
(3, 'manager', 'web', 0, '2019-09-08 02:41:38', '2019-09-08 02:41:38', NULL),
(4, 'client', 'web', 1, '2019-09-08 02:41:54', '2019-09-08 02:41:54', NULL),
(5, 'driver', 'web', 0, '2019-12-15 23:50:21', '2019-12-15 23:50:21', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `role_has_permissions`
--

CREATE TABLE `role_has_permissions` (
  `permission_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `role_has_permissions`
--

INSERT INTO `role_has_permissions` (`permission_id`, `role_id`) VALUES
(1, 2),
(1, 3),
(1, 4),
(1, 5),
(2, 2),
(3, 2),
(3, 3),
(3, 4),
(3, 5),
(4, 2),
(4, 3),
(4, 4),
(4, 5),
(5, 2),
(5, 3),
(6, 2),
(9, 2),
(10, 2),
(14, 2),
(15, 2),
(16, 2),
(17, 2),
(18, 2),
(19, 2),
(20, 2),
(21, 2),
(22, 2),
(23, 2),
(24, 2),
(25, 2),
(26, 2),
(27, 2),
(27, 3),
(27, 4),
(27, 5),
(28, 2),
(29, 2),
(30, 2),
(30, 3),
(30, 4),
(30, 5),
(31, 2),
(31, 3),
(31, 4),
(32, 2),
(32, 3),
(32, 4),
(33, 2),
(33, 3),
(34, 2),
(34, 3),
(35, 2),
(36, 2),
(37, 2),
(38, 2),
(39, 2),
(40, 2),
(41, 2),
(42, 2),
(42, 3),
(43, 2),
(44, 2),
(45, 2),
(46, 2),
(47, 2),
(48, 2),
(48, 3),
(48, 5),
(50, 2),
(51, 2),
(52, 2),
(52, 3),
(52, 4),
(52, 5),
(53, 2),
(53, 3),
(54, 2),
(54, 3),
(55, 2),
(55, 3),
(56, 2),
(56, 3),
(57, 2),
(57, 3),
(58, 2),
(58, 3),
(59, 2),
(59, 3),
(60, 2),
(60, 3),
(61, 2),
(61, 3),
(62, 2),
(62, 3),
(63, 2),
(63, 3),
(64, 2),
(64, 3),
(64, 4),
(64, 5),
(67, 2),
(67, 3),
(67, 4),
(67, 5),
(68, 2),
(68, 3),
(68, 4),
(68, 5),
(69, 2),
(76, 2),
(76, 3),
(77, 2),
(77, 3),
(78, 2),
(78, 3),
(80, 2),
(80, 3),
(81, 2),
(81, 3),
(82, 2),
(82, 3),
(83, 2),
(83, 3),
(83, 4),
(83, 5),
(85, 2),
(86, 2),
(86, 3),
(86, 4),
(86, 5),
(87, 2),
(88, 2),
(89, 2),
(90, 2),
(91, 2),
(92, 2),
(92, 3),
(92, 4),
(92, 5),
(95, 2),
(95, 3),
(95, 4),
(95, 5),
(96, 2),
(96, 3),
(96, 4),
(96, 5),
(97, 2),
(98, 2),
(98, 3),
(98, 4),
(98, 5),
(103, 2),
(103, 3),
(103, 4),
(103, 5),
(104, 2),
(104, 3),
(104, 4),
(104, 5),
(107, 2),
(107, 3),
(107, 4),
(107, 5),
(108, 2),
(108, 3),
(109, 2),
(109, 3),
(110, 2),
(110, 3),
(111, 2),
(111, 3),
(111, 4),
(111, 5),
(112, 2),
(113, 2),
(113, 3),
(113, 4),
(113, 5),
(114, 2),
(114, 3),
(114, 4),
(114, 5),
(117, 2),
(117, 3),
(117, 4),
(117, 5),
(118, 2),
(119, 2),
(120, 2),
(121, 2),
(122, 2),
(123, 2),
(124, 2),
(127, 2),
(128, 2),
(129, 2),
(130, 2),
(130, 3),
(130, 5),
(131, 2),
(134, 2),
(134, 3),
(135, 2),
(135, 3),
(137, 2),
(137, 3),
(138, 2),
(144, 2),
(144, 5),
(145, 2),
(145, 3),
(145, 5),
(146, 2),
(146, 3),
(146, 5),
(148, 2),
(149, 2),
(151, 2),
(151, 3),
(152, 2),
(152, 3),
(153, 2),
(153, 3),
(155, 2),
(156, 2),
(160, 2),
(164, 2),
(164, 3),
(164, 4),
(164, 5),
(165, 2),
(166, 2),
(167, 2),
(168, 2),
(169, 2),
(170, 2),
(170, 3),
(171, 2),
(171, 3),
(172, 2),
(172, 3),
(173, 2),
(174, 2),
(175, 2),
(176, 2),
(176, 3),
(176, 4),
(176, 5),
(183, 2),
(183, 3),
(183, 4),
(183, 5),
(184, 2),
(185, 2),
(186, 2),
(186, 3),
(187, 2),
(187, 3),
(188, 2),
(189, 2),
(190, 2),
(191, 2),
(192, 2),
(193, 2),
(194, 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `slides`
--

CREATE TABLE `slides` (
  `id` int(10) UNSIGNED NOT NULL,
  `order` int(10) UNSIGNED DEFAULT 0,
  `text` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `button` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `text_position` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT 'start',
  `text_color` varchar(36) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `button_color` varchar(36) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `background_color` varchar(36) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `indicator_color` varchar(36) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image_fit` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT 'cover',
  `product_id` int(10) UNSIGNED DEFAULT NULL,
  `market_id` int(10) UNSIGNED DEFAULT NULL,
  `enabled` tinyint(1) DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `slides`
--

INSERT INTO `slides` (`id`, `order`, `text`, `button`, `text_position`, `text_color`, `button_color`, `background_color`, `indicator_color`, `image_fit`, `product_id`, `market_id`, `enabled`, `created_at`, `updated_at`) VALUES
(1, 1, 'Fresh Home Grown  vegetables', NULL, 'bottom_start', '#ffffff', '#28a745', '#ccccdd', '#25d366', 'cover', 41, 11, 1, '2020-11-07 23:48:19', '2021-02-08 06:21:07');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `uploads`
--

CREATE TABLE `uploads` (
  `id` int(10) UNSIGNED NOT NULL,
  `uuid` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `uploads`
--

INSERT INTO `uploads` (`id`, `uuid`, `created_at`, `updated_at`) VALUES
(1, '4c2e28ca-7c91-487c-ab62-5719ea5a2e9d', '2020-11-07 23:52:33', '2020-11-07 23:52:33'),
(2, '2c969be4-868f-4d13-859b-cdff00472cb4', '2020-11-07 23:53:11', '2020-11-07 23:53:11'),
(3, 'f629d5f9-7702-469c-ba86-3e3b413a8346', '2020-11-08 18:35:44', '2020-11-08 18:35:44'),
(4, '1074e857-1571-4a0c-8efa-769fece9803a', '2020-11-08 18:37:27', '2020-11-08 18:37:27'),
(5, 'f4c612e1-0b48-467f-a3cc-e6038fdf26a7', '2020-11-08 18:41:02', '2020-11-08 18:41:02'),
(6, '222e4518-c1ac-4d9d-8d1d-b2cd93f606f0', '2020-11-08 18:47:32', '2020-11-08 18:47:32'),
(7, 'a34ed564-42f0-45ae-8508-862cc5017653', '2020-11-08 18:47:47', '2020-11-08 18:47:47'),
(8, '9822404c-b525-4937-910a-856617321649', '2020-11-08 18:49:11', '2020-11-08 18:49:11'),
(9, 'd4fd9c49-a322-40ff-93dd-0d81fa43b8a1', '2020-11-08 19:11:09', '2020-11-08 19:11:09'),
(10, 'ce86d062-5a2c-46de-865f-7cd5ec51a3bc', '2020-11-08 21:50:56', '2020-11-08 21:50:56'),
(11, 'ab0652dd-e077-4020-8046-4e8f4c163199', '2020-11-27 04:06:05', '2020-11-27 04:06:05'),
(12, '6cace509-37f0-40e5-ba5d-53f823734bec', '2020-11-27 04:10:38', '2020-11-27 04:10:38'),
(13, 'eaa1fc64-6b9e-493c-9f49-f54cd1c2bba5', '2020-11-27 04:14:40', '2020-11-27 04:14:40'),
(14, '3b41a861-0511-49fa-b520-ba269be34dd4', '2020-11-27 04:20:16', '2020-11-27 04:20:16'),
(15, '107b0505-b852-4687-bd4f-ee51851ebef3', '2020-11-27 04:24:14', '2020-11-27 04:24:14'),
(16, 'da7767b7-67bb-454c-b531-18fa07009a1b', '2020-11-27 04:26:33', '2020-11-27 04:26:33'),
(17, '852ff76c-0c8f-4771-b32d-9fa1a9ee02d1', '2020-11-27 04:29:20', '2020-11-27 04:29:20'),
(18, '351496a0-2a3f-443b-92b2-5c31fd196e48', '2020-11-27 04:31:43', '2020-11-27 04:31:43'),
(19, 'dee3209f-9498-471d-b910-b1be3c12ab83', '2020-11-27 04:33:58', '2020-11-27 04:33:58'),
(20, 'bb8388fa-db6f-4805-9b4d-b06da49142e7', '2020-11-27 04:34:52', '2020-11-27 04:34:52'),
(21, 'bcde08af-04fe-4253-b31a-548ce7d3685a', '2021-02-08 06:07:56', '2021-02-08 06:07:56'),
(22, '7d3f1aa6-e986-4e13-bdb8-2e861c19cbc2', '2021-02-09 03:17:34', '2021-02-09 03:17:34'),
(23, 'ac1f91b3-6f66-45dc-8b22-cdc55279eb7c', '2021-02-09 03:27:54', '2021-02-09 03:27:54'),
(24, 'f01102c0-f87c-4a3d-8d03-c76e1b4dc767', '2021-02-09 03:39:40', '2021-02-09 03:39:40'),
(25, '31a59d34-384e-4e72-95fc-658909bed002', '2021-02-09 03:40:55', '2021-02-09 03:40:55'),
(26, '04c0a9af-5780-4a10-8d94-82415816ba1f', '2021-02-09 03:43:30', '2021-02-09 03:43:30'),
(27, 'ce0dc6e3-8a60-453b-a993-82a463ac7779', '2021-02-09 03:55:55', '2021-02-09 03:55:55'),
(28, '0dc8fdec-132b-49ea-b859-3b0c7424908a', '2021-02-22 01:55:21', '2021-02-22 01:55:21'),
(29, '64c29af8-6683-4327-8215-5f8c1cd6879f', '2021-02-22 02:06:39', '2021-02-22 02:06:39'),
(30, '2ddf6dac-7499-4b12-b25c-72595d2a26fb', '2021-02-22 02:13:38', '2021-02-22 02:13:38'),
(31, '57a9262c-d4ed-47f6-8a22-a198b010bddb', '2021-02-22 02:16:54', '2021-02-22 02:16:54'),
(32, 'ddd5815d-5ed8-4a00-a42e-1b47cc216d5d', '2021-02-22 02:20:03', '2021-02-22 02:20:03'),
(33, 'fead46ef-a6b7-4d93-aac4-18ce508aa0f5', '2021-02-22 02:22:33', '2021-02-22 02:22:33'),
(34, '9b74d843-8cdc-469a-942e-0de01bc6bad1', '2021-02-22 02:27:21', '2021-02-22 02:27:21'),
(35, '3b3cba68-9da4-4bb1-bfaf-fe2b01ae341b', '2021-02-22 02:30:54', '2021-02-22 02:30:54'),
(36, '6711d05c-dd15-4970-b211-f11c79688e80', '2021-02-22 02:35:05', '2021-02-22 02:35:05'),
(37, 'd83ac7a4-81cb-496f-a0a3-08b3779c51e3', '2021-02-22 02:39:17', '2021-02-22 02:39:17'),
(38, 'cbad789c-1435-42d5-adfd-786b6d6d9e98', '2021-02-22 02:40:54', '2021-02-22 02:40:54'),
(39, 'e5affcc5-cc5e-4e48-9145-14849961143f', '2021-02-22 02:41:09', '2021-02-22 02:41:09'),
(40, '5a3200d6-094c-454b-8455-c42816541df8', '2021-02-22 02:43:30', '2021-02-22 02:43:30'),
(41, 'f428e12d-3be3-4245-8725-de7b88a472f2', '2021-02-22 02:46:33', '2021-02-22 02:46:33'),
(42, '592c0493-59c1-4264-8e54-569fb8346e0b', '2021-02-22 02:49:52', '2021-02-22 02:49:52'),
(43, '196c80fa-53be-4a3e-b126-9b8ee4dda436', '2021-02-22 02:55:17', '2021-02-22 02:55:17'),
(44, '82a728b0-f579-46a1-9073-dd74b818a2be', '2021-02-22 02:57:22', '2021-02-22 02:57:22'),
(45, '09b28615-6bf3-4715-861c-a072a7f0ef60', '2021-02-22 02:58:59', '2021-02-22 02:58:59'),
(46, 'fcdf062a-217e-40b6-be1f-31ab555c7ef5', '2021-02-22 03:05:03', '2021-02-22 03:05:03'),
(47, '926c47bc-0d5a-4079-8458-618223922fa8', '2021-02-22 03:09:01', '2021-02-22 03:09:01'),
(48, '2d853c41-b78e-4713-bf66-87f8898ba2ad', '2021-02-22 03:14:35', '2021-02-22 03:14:35');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `api_token` char(60) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `device_token` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `stripe_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `card_brand` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `card_last_four` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `trial_ends_at` timestamp NULL DEFAULT NULL,
  `braintree_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `paypal_email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `api_token`, `device_token`, `stripe_id`, `card_brand`, `card_last_four`, `trial_ends_at`, `braintree_id`, `paypal_email`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Afroeat', 'admin@demo.com', '$2y$10$gmbNftREf.JOrANiK4ic5efGhsPqnwG8rbuaFvYuVTy4aQo50.fNu', 'PivvPlsQWxPl1bB5KrbKNBuraJit0PrUZekQUgtLyTRuyBq921atFtoR1HuA', 'cZMMkrNkZgvVHoMxZBhaSq:APA91bFtPCx8aLtwrg08701xwLRrvIizQboo3tF-r0qdYMSsbB0yUEeHVonxb925HhC0crSo7hbzSb4PjxwLBKKVjFC47xV5GW3X48ZxyEqa8PnqVUvaO99SDqp_ji2YYk4jo3mZ1aaT', NULL, NULL, NULL, NULL, NULL, NULL, '1j2aHgwgAPNYVcgoQ0n7nYIETcNeI1eZswam6YssfIM67DVoddeYFQVVPhRz', '2018-08-07 02:58:41', '2021-03-27 17:41:25'),
(2, 'Barbara J. Glanz', 'manager@demo.com', '$2y$10$YOn/Xq6vfvi9oaixrtW8QuM2W0mawkLLqIxL.IoGqrsqOqbIsfBNu', 'tVSfIKRSX2Yn8iAMoUS3HPls84ycS8NAxO2dj2HvePbbr4WHorp4gIFRmFwB', 'eL9KskqGRdqWeMYZn534KP:APA91bFR2Hinb6LMm8rMhR7GS3TUWtWV6ruZue7j2t3vMigo3cqN7pijupw9idpFElLY-J1paU98fO_LxWkdKIE7xm4qAbPdivAgZOCkwpsLW8Sqol1dWjrpMzVp_2NfeOOLcMDIeOzP', NULL, NULL, NULL, NULL, NULL, NULL, '5nysjzVKI4LU92bjRqMUSYdOaIo1EcPC3pIMb6Tcj2KXSUMriGrIQ1iwRdd0', '2018-08-14 21:06:28', '2021-02-02 15:40:48'),
(3, 'Charles W. Abeyta', 'client@demo.com', '$2y$10$EBubVy3wDbqNbHvMQwkj3OTYVitL8QnHvh/zV0ICVOaSbALy5dD0K', 'fXLu7VeYgXDu82SkMxlLPG1mCAXc4EBIx6O5isgYVIKFQiHah0xiOHmzNsBv', 'fCiogkFNJEBPpMZLuWHoo6:APA91bHgZ8VtvmPS5HazeL55eQiH5fvOf1o9aKzpBDEpcOsvrWJpchOJy2k_i02DNpf2lFuNYoPrkpwKMXG5erXljob3ERk377CtAPeWQ03HOSp7mRQ_jHNFeEkdoBhZnEcKkBSQFWkx', NULL, NULL, NULL, NULL, NULL, NULL, '3NXN0rxY7kgb8472KESPQegviSFJUafbEcE0lOPuNwsEIcpnZFDqYag1R15r', '2019-10-13 02:31:26', '2021-02-10 14:48:41'),
(4, 'Robert E. Brock', 'client1@demo.com', '$2y$10$pmdnepS1FhZUMqOaFIFnNO0spltJpziz3j13UqyEwShmLhokmuoei', 'Czrsk9rwD0c75NUPkzNXM2WvbxYHKj8p0nG29pjKT0PZaTgMVzuVyv4hOlte', 'cBO-kv58TgiijYe1uurAcW:APA91bFnlDLWwAEIKP8MvpXL7zpZkOuFDdAUBj1uW7aN50-UP3UROLAm-8kpXhB1JBm-4P7Gm0HkJICPl4ipip1Hts52zHL2H8pQHfCjlepUQXjD99Ns3w3PHGpnYdFCWge2HX4yjBex', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-10-15 21:55:39', '2020-11-28 03:31:00'),
(5, 'Sanchez Roberto', 'driver@demo.com', '$2y$10$T/jwzYDJfC8c9CdD5PbpuOKvEXlpv4.RR1jMT0PgIMT.fzeGw67JO', 'OuMsmU903WMcMhzAbuSFtxBekZVdXz66afifRo3YRCINi38jkXJ8rpN0FcfS', 'eFaNsNanRVeqCU7HKB5I8J:APA91bEVwcgrfmU2vQDA66GqklZ8wlARXUd6AYrjuCCkSVVOic4-16Q87PTXOrpTXA5oPC4Y5mxxBmVaTA52nJZPKnAnkrUaHmDQ1_Jkda5yXNFAoR6QfyVTqYdydO7qmIUweIi6VS6G', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-12-15 23:49:44', '2020-11-28 02:34:56'),
(6, 'John Doe', 'driver1@demo.com', '$2y$10$YF0jCx2WCQtfZOq99hR8kuXsAE0KSnu5OYSomRtI9iCVguXDoDqVm', 'zh9mzfNO2iPtIxj6k4Jpj8flaDyOsxmlGRVUZRnJqOGBr8IuDyhb3cGoncvS', 'ev-ThT1e9kdSvV-lys2JYx:APA91bEZ4PL5o1LYnY_vCPJzq6koC7y5Hjhy_gYdv9outD1o1AiSAb6r7CElB5c6Iyw5kw3eVwJUvJF_5D4qF7tboLvLwFH3sf_M30MI42JKlGS4V77kDYcWZdkvKtp10AQuSpjC74Ku', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-03-29 21:28:04', '2021-01-29 01:19:46'),
(7, 'Emmanuel', 'egod.pablo@gmail.com', '$2y$10$8pj5dcxAhq.wZLktRjU4Ge0Pesiz/WhrchhO4Sj5W2vNDR7D1B6Nm', 'b6M5smhSd3vMZXCYyucDCHRnzjBZyAlAyUHkKtprKN0rU5xVrLyJqAMOigI1', 'eiBrUwejRTKm7uEsc1gD2F:APA91bGXpVgxwdIZ-XM8gcUv_VQO95B18mVfpU0rOiiqGwQ0wDKxTY7HdtJeIVs4eF3ezWkuEZWB_58dgTQvM7TrNTJWigJcAdypAZhmKbhKpM-qTFECJ5Bi2U8M3Jo8spogaipGa9VF', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-11-08 01:17:45', '2020-11-08 05:26:10'),
(8, 'peges inc', 'pegesmedia@gmail.com', '$2y$10$XjICmsvZXvpFvWqPgskkpOxsVmHoV67R1EKz9qzVRm8kp/kIGFPPe', '0zQUjDjdbbDk1sTARks2uy1K8TZgDRKxtvBo93E9rar3PpLEQe1R6cr3bcj3', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-11-08 18:31:49', '2020-11-08 18:31:49'),
(9, 'Hgyu Huyy', 'benydiam@gmail.com', '$2y$10$77gTF4EjgLx0d8dxBCvQreP2mZTZ6TShYBiB/Q0oz8.8hmWRzHqri', 'DBKDPTlpj9hTWLbQnVlqTdqI6GhBaf8MLKxXg9JTjanlWb3mVa13fRvx4UX8', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-11-10 03:34:43', '2020-11-10 03:35:38'),
(10, 'Freeman nhyia', 'info@pegesinc.com', '$2y$10$.Hu/sn49mde8IrGMKlmCmO7.6pLQpAULR1pCLAGRzvPe5qNrujA.O', '47F0NHJT1VAVvBSVhzaDJiVggciBatK3wBSR9ffudu3WOgVW7mNRHoZn8Mzp', 'dsB-MjALQWy0-5sSM4zr2W:APA91bFIf3RrTXK27Yce7-gnbJ0aXDdD2Tw034AOon7BBPmScof7974uH-y4PVBYNokz0tMp-Ck-NvncA5SZlTsVYq2wP1GWTrshTwleTjP6CJBsjhsMyD736iprfGsUXQ_bTYnUmmlV', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-01-24 02:38:12', '2021-01-24 02:38:12'),
(11, 'ali', 'alimurtaza84@hotmail.com', '$2y$10$RCC1vns8tCIw.WPiuZ6uAeFZhWID1pm8MxlvRcqXZ4xikDvSMqPku', 'QJzkJv2acZGivnBmSpFKCOz540VIS7uQzNR5hoDCY6V0SjBnJBv5xvEEQKtb', 'eol_yodZSqWwW1Ju7E887k:APA91bERKvQQPkLMRZhZ5NqgJxgXbBE1nD_kH3cpbc7mN5RYFohRwrT8C6sBilmORdPyt8-SyGW8aZh-C7MCpNxsHmxp1zZn1OuUHSVTlfSHQdxJF2rtgLzMmNczU5YlxRZKsZ-hbK_i', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-01-27 11:13:10', '2021-01-27 11:13:10'),
(12, 'john', 'playstorecnx28@gmail.com', '$2y$10$aWC9BtHAI7hdWo9ZAvpPR.S.pMMcKHc/HP4QvetJHjWMw.9Yj74ZO', 'AS7VWRBSlgS5boHrUCKAJmxgLatmkbHzaW2NAhs2yqcLdCyJO3VmF0fMdRYc', 'fBmfeYghRn-EHDSxGLSiSo:APA91bFP__txDbZc6dsMv_QN-btYuJJQHgk-YZIRZGvmGI6t877tehaIyrUE5cClNojyyii9ou1C3oC0RZB0DpIiA-4UL4KLmbSXD6VFOqB9KIeJ4YWcfomnm03k_ejhnc1juWIAHn7S', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-01-30 23:21:08', '2021-01-30 23:21:08'),
(13, 'lol mol', 'lolamol9999@gmail.com', '$2y$10$USqVt4dKV2LGpEKb1zOTJO058poo7AxrKBqKpeOKaU/K2fHNa8Nve', 'ZhHOiVPdf265R9GBnEEhBFS6mYyQVFsKyqj2bcERBNk31TdRL1uJQ69ROORK', 'cgTwYRyxTXy3DNcWHFGptX:APA91bF_ZR_RqkDW0n25hg2jdF6RKxdCRLDAEY-TsP4oFVWNGiHw8BFbf2eL9Cypp6s49KcjYcFIRTzCfiVRwJPBFykFd2JQy_Xdnr3FxxZepOTbghX3hNRb1lvH8_HxLDhbDsrZzwM4', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-01-31 12:17:05', '2021-01-31 12:17:05'),
(14, 'Daniel radford', 'radforddaniel72@gmail.com', '$2y$10$nVJgqDFMt/.UgQyRtPgVEePKb6CIzerztsDhboqYKQ5gvV94l1ZDy', '0IoL7yz7ouv7q1kQ5nCmha1SLZHXVesG7huYVGa7zMMXW8oaHz58UEmS493x', 'cxc87QxlrE4BtvTFpuANk_:APA91bEHIwE0-2d9M5fQy5vIbJ_RxYAm_jWF5jLqS9xaifO2UcdNfQ2w0Tcv9fzVuGI193emuZWgKG-ZV-p9klR6DfebLk6wRLOIQMq3OEbfpkpwT745vBq_Xag2lUJCa1CFz6X2ESec', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-02-01 05:14:44', '2021-02-01 05:14:44'),
(15, 'Tracy Jacobs', 'tjacobs084@gmail.com', '$2y$10$nz9S80/rq31KinDLyn9MbujWqkYE7AOi9N/UelAiPhzMzPymyWYeC', 'tyKfzhl3mp8QMv9FRavEnjtbHrt1g7qNbqswEMaxGzzIjgkwBoOzVqvyxVNV', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-02-08 05:21:42', '2021-02-08 05:21:42'),
(16, 'Charles Cooper', 'dualamarket@gmail.com', '$2y$10$wxi8iZY2MdHHIAFhRAGz/Odx6FLlUBG6rbvcGBYMiU3IeY5bWceBq', 'QGaEoFuSu5tNvnBYe0jAjYko5eYSCkVd4jvyCtShInEO4yYe9JjtHXwJOKaX', 'cJ7V7jF0CWG9vBI5B8ib5D:APA91bGMI2MYsa7Tpt0rkBfSmAt5pq1gTNB3GmmVif-AWHE6ZFkwLomPPue1PlTCr8uWvzXLKMSOYxhgern64b3QwRVXYKKPnn4swzkl-HEWVA8mtL4Zb6D7Rq4VECQMox4xgJOE-fHX', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-02-08 05:45:07', '2021-02-12 10:18:13'),
(17, 'Prince Horton-Taylor', 'phortontaylor@gmail.com', '$2y$10$YAOXqUS7Rt7s1B9LDz1xKe0gOFJ1WB6NWBYwpwgrNBEelDRLTSl/u', 'Gzrf2EZy9BqUE42agfDvkJcfECO6768LD639uGjq8ei7SQVSNeg7jAierg15', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-02-16 08:58:29', '2021-02-16 08:58:29'),
(18, 'Marvin Logan', 'marvin.logan1@yahoo.com', '$2y$10$HV.lbdd8uXQDwrG6vNjpX.DhEOgA9ZHc2fDjWd3Y/S1xk2YuVhIsm', 'BOBiG4uJDwMFAfHdvFFGyhIbmzbJK2GnMgIclmkf3o540D0Qv8J45D1IsNJX', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-02-22 01:38:05', '2021-02-22 01:38:05'),
(19, 'John Doe', 'sept101990@gmail.com', '$2y$10$XmszoT.TfgN4VvVll5t4M.UIXVXfUfayXpaAx2l3OOHoAKcs6ytEK', '3IF2KzMJzCPpnMqVq68Va66G79rkSlVrXF9QySmjlwPJy7oxIVOmxEGUPfUs', 'd31Us0C590VnpNdYm0d8_3:APA91bFCQVs9mTu4GDZ3eU8Hxhcq6z97g73AUj6h24Oha7ZIYjl-Z5LmKw65_BR7AT1qabd5yYMDM_DfDUD_Wzaw47RAT8yn1WEQSRtoeTXxSjlBVRqnetLDfYDVS_iCCJ55mBE9IlFy', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-09 17:07:52', '2021-03-25 02:44:51'),
(20, 'Kofi gh', 'kofi@gmail.com', '$2y$10$WQQqxlZB79VvaL3tKT3IuOSfxcR4/whXj2HQu6W1jPDNmrswyUDxi', 'HO88v7h1W8bu1fBDXwTkTGsbkSYhvjkb11ui2l31R0NTRcM7dk4tyjd0Qkyb', 'djmM-bnH90tkkdeVQ7tf9d:APA91bH9YS08qnn_iti1jsbggqdRXvo2eGjyMBPTwcfuyplXTejUaeKN-MP7q71W8G3DdvM_rhRHDzP99Az9QzRKhwPv0zvJDUf2PCv7cPgm4mhl_yhPkMxp1peoFtIfQNHc6vAvSjwe', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-24 11:52:30', '2021-03-24 11:52:30'),
(21, 'testee', 'cer@g.com', '$2y$10$8X4nmN.Cq7yLQDB/iWsJFO8PTd6wQ2CcIYmw0UQThJ.1lSi3oSzKC', 'rJgIX6XADd2fZtnPeLst88hM5rS25JVXL6XblYQNcwdAEQqy0x2wfgDRHpFT', 'c7FtxvN-Q3W5Q8hDW9JRDP:APA91bHsqfedU27sM6DNJT8nzBdgZx7wQFqZH2SlwrZzoIX7LLrvxI88IBPLyVZ6gTl-_ejoAvHJGWd2MqxwfXmv0w1HsR7daMNoqaFoCvR1AGgWnWkT_TDen1djCkXjjB-q7U0pO_uB', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-25 01:25:44', '2021-03-25 01:25:44'),
(22, 'Sackor Saydee', 'info.afrofina@gmail.com', '$2y$10$ohy.7yeY9dSlx4rWMRQiZO0/LSKWl5yY3jUuy32yK40sPQU1ur5lm', 'vzSWhNsGSgYzCM44JSiVE1z1jjKmwfwqgP1veErllQOoHBQJYvRh3VVRRrNh', 'dfacPEzruk8VtFsqUCH5I-:APA91bEuVUTdHNPam0CvO1qoA8TBNsOlgPZI6zaNFF-ca_UbF7ovc7N9WZRLHcVpO6ThB0bszQjdBGv8MTscs9483_hwfm-F-wDU4KVuePJoOqdm9anzvGHK26ZxhV9oDYv9lSk1r3rc', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-25 01:32:11', '2021-03-25 01:35:22');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `user_markets`
--

CREATE TABLE `user_markets` (
  `user_id` int(10) UNSIGNED NOT NULL,
  `market_id` int(10) UNSIGNED NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `user_markets`
--

INSERT INTO `user_markets` (`user_id`, `market_id`) VALUES
(1, 5),
(1, 6),
(2, 1),
(2, 2),
(2, 3),
(2, 4),
(2, 10),
(16, 11),
(17, 12);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `app_settings`
--
ALTER TABLE `app_settings`
  ADD PRIMARY KEY (`id`),
  ADD KEY `app_settings_key_index` (`key`);

--
-- Indices de la tabla `carts`
--
ALTER TABLE `carts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `carts_product_id_foreign` (`product_id`),
  ADD KEY `carts_user_id_foreign` (`user_id`);

--
-- Indices de la tabla `cart_options`
--
ALTER TABLE `cart_options`
  ADD PRIMARY KEY (`option_id`,`cart_id`),
  ADD KEY `cart_options_cart_id_foreign` (`cart_id`);

--
-- Indices de la tabla `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `coupons`
--
ALTER TABLE `coupons`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `coupons_code_unique` (`code`);

--
-- Indices de la tabla `currencies`
--
ALTER TABLE `currencies`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `custom_fields`
--
ALTER TABLE `custom_fields`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `custom_field_values`
--
ALTER TABLE `custom_field_values`
  ADD PRIMARY KEY (`id`),
  ADD KEY `custom_field_values_custom_field_id_foreign` (`custom_field_id`);

--
-- Indices de la tabla `delivery_addresses`
--
ALTER TABLE `delivery_addresses`
  ADD PRIMARY KEY (`id`),
  ADD KEY `delivery_addresses_user_id_foreign` (`user_id`);

--
-- Indices de la tabla `discountables`
--
ALTER TABLE `discountables`
  ADD PRIMARY KEY (`id`),
  ADD KEY `discountables_coupon_id_foreign` (`coupon_id`);

--
-- Indices de la tabla `drivers`
--
ALTER TABLE `drivers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `drivers_user_id_foreign` (`user_id`);

--
-- Indices de la tabla `drivers_payouts`
--
ALTER TABLE `drivers_payouts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `drivers_payouts_user_id_foreign` (`user_id`);

--
-- Indices de la tabla `driver_markets`
--
ALTER TABLE `driver_markets`
  ADD PRIMARY KEY (`user_id`,`market_id`),
  ADD KEY `driver_markets_market_id_foreign` (`market_id`);

--
-- Indices de la tabla `earnings`
--
ALTER TABLE `earnings`
  ADD PRIMARY KEY (`id`),
  ADD KEY `earnings_market_id_foreign` (`market_id`);

--
-- Indices de la tabla `faqs`
--
ALTER TABLE `faqs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `faqs_faq_category_id_foreign` (`faq_category_id`);

--
-- Indices de la tabla `faq_categories`
--
ALTER TABLE `faq_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `favorites`
--
ALTER TABLE `favorites`
  ADD PRIMARY KEY (`id`),
  ADD KEY `favorites_product_id_foreign` (`product_id`),
  ADD KEY `favorites_user_id_foreign` (`user_id`);

--
-- Indices de la tabla `favorite_options`
--
ALTER TABLE `favorite_options`
  ADD PRIMARY KEY (`option_id`,`favorite_id`),
  ADD KEY `favorite_options_favorite_id_foreign` (`favorite_id`);

--
-- Indices de la tabla `fields`
--
ALTER TABLE `fields`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `galleries`
--
ALTER TABLE `galleries`
  ADD PRIMARY KEY (`id`),
  ADD KEY `galleries_market_id_foreign` (`market_id`);

--
-- Indices de la tabla `markets`
--
ALTER TABLE `markets`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `markets_payouts`
--
ALTER TABLE `markets_payouts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `markets_payouts_market_id_foreign` (`market_id`);

--
-- Indices de la tabla `market_fields`
--
ALTER TABLE `market_fields`
  ADD PRIMARY KEY (`field_id`,`market_id`),
  ADD KEY `market_fields_market_id_foreign` (`market_id`);

--
-- Indices de la tabla `market_reviews`
--
ALTER TABLE `market_reviews`
  ADD PRIMARY KEY (`id`),
  ADD KEY `market_reviews_user_id_foreign` (`user_id`),
  ADD KEY `market_reviews_market_id_foreign` (`market_id`);

--
-- Indices de la tabla `media`
--
ALTER TABLE `media`
  ADD PRIMARY KEY (`id`),
  ADD KEY `media_model_type_model_id_index` (`model_type`,`model_id`);

--
-- Indices de la tabla `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `model_has_permissions`
--
ALTER TABLE `model_has_permissions`
  ADD PRIMARY KEY (`permission_id`,`model_id`,`model_type`),
  ADD KEY `model_has_permissions_model_type_model_id_index` (`model_type`,`model_id`);

--
-- Indices de la tabla `model_has_roles`
--
ALTER TABLE `model_has_roles`
  ADD PRIMARY KEY (`role_id`,`model_id`,`model_type`),
  ADD KEY `model_has_roles_model_type_model_id_index` (`model_type`,`model_id`);

--
-- Indices de la tabla `notifications`
--
ALTER TABLE `notifications`
  ADD PRIMARY KEY (`id`),
  ADD KEY `notifications_notifiable_type_notifiable_id_index` (`notifiable_type`,`notifiable_id`);

--
-- Indices de la tabla `options`
--
ALTER TABLE `options`
  ADD PRIMARY KEY (`id`),
  ADD KEY `options_product_id_foreign` (`product_id`),
  ADD KEY `options_option_group_id_foreign` (`option_group_id`);

--
-- Indices de la tabla `option_groups`
--
ALTER TABLE `option_groups`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`),
  ADD KEY `orders_user_id_foreign` (`user_id`),
  ADD KEY `orders_order_status_id_foreign` (`order_status_id`),
  ADD KEY `orders_driver_id_foreign` (`driver_id`),
  ADD KEY `orders_delivery_address_id_foreign` (`delivery_address_id`),
  ADD KEY `orders_payment_id_foreign` (`payment_id`);

--
-- Indices de la tabla `order_statuses`
--
ALTER TABLE `order_statuses`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indices de la tabla `payments`
--
ALTER TABLE `payments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `payments_user_id_foreign` (`user_id`);

--
-- Indices de la tabla `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`),
  ADD KEY `products_market_id_foreign` (`market_id`),
  ADD KEY `products_category_id_foreign` (`category_id`);

--
-- Indices de la tabla `product_orders`
--
ALTER TABLE `product_orders`
  ADD PRIMARY KEY (`id`),
  ADD KEY `product_orders_product_id_foreign` (`product_id`),
  ADD KEY `product_orders_order_id_foreign` (`order_id`);

--
-- Indices de la tabla `product_order_options`
--
ALTER TABLE `product_order_options`
  ADD PRIMARY KEY (`product_order_id`,`option_id`),
  ADD KEY `product_order_options_option_id_foreign` (`option_id`);

--
-- Indices de la tabla `product_reviews`
--
ALTER TABLE `product_reviews`
  ADD PRIMARY KEY (`id`),
  ADD KEY `product_reviews_user_id_foreign` (`user_id`),
  ADD KEY `product_reviews_product_id_foreign` (`product_id`);

--
-- Indices de la tabla `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `role_has_permissions`
--
ALTER TABLE `role_has_permissions`
  ADD PRIMARY KEY (`permission_id`,`role_id`),
  ADD KEY `role_has_permissions_role_id_foreign` (`role_id`);

--
-- Indices de la tabla `slides`
--
ALTER TABLE `slides`
  ADD PRIMARY KEY (`id`),
  ADD KEY `slides_product_id_foreign` (`product_id`),
  ADD KEY `slides_market_id_foreign` (`market_id`);

--
-- Indices de la tabla `uploads`
--
ALTER TABLE `uploads`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD UNIQUE KEY `users_api_token_unique` (`api_token`);

--
-- Indices de la tabla `user_markets`
--
ALTER TABLE `user_markets`
  ADD PRIMARY KEY (`user_id`,`market_id`),
  ADD KEY `user_markets_market_id_foreign` (`market_id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `app_settings`
--
ALTER TABLE `app_settings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=180;

--
-- AUTO_INCREMENT de la tabla `carts`
--
ALTER TABLE `carts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;

--
-- AUTO_INCREMENT de la tabla `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `coupons`
--
ALTER TABLE `coupons`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `currencies`
--
ALTER TABLE `currencies`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT de la tabla `custom_fields`
--
ALTER TABLE `custom_fields`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `custom_field_values`
--
ALTER TABLE `custom_field_values`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=74;

--
-- AUTO_INCREMENT de la tabla `delivery_addresses`
--
ALTER TABLE `delivery_addresses`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT de la tabla `discountables`
--
ALTER TABLE `discountables`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `drivers`
--
ALTER TABLE `drivers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `drivers_payouts`
--
ALTER TABLE `drivers_payouts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `earnings`
--
ALTER TABLE `earnings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `faqs`
--
ALTER TABLE `faqs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT de la tabla `faq_categories`
--
ALTER TABLE `faq_categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `favorites`
--
ALTER TABLE `favorites`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT de la tabla `fields`
--
ALTER TABLE `fields`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `galleries`
--
ALTER TABLE `galleries`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT de la tabla `markets`
--
ALTER TABLE `markets`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT de la tabla `markets_payouts`
--
ALTER TABLE `markets_payouts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `market_reviews`
--
ALTER TABLE `market_reviews`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT de la tabla `media`
--
ALTER TABLE `media`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=102;

--
-- AUTO_INCREMENT de la tabla `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;

--
-- AUTO_INCREMENT de la tabla `options`
--
ALTER TABLE `options`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=71;

--
-- AUTO_INCREMENT de la tabla `option_groups`
--
ALTER TABLE `option_groups`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;

--
-- AUTO_INCREMENT de la tabla `order_statuses`
--
ALTER TABLE `order_statuses`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `payments`
--
ALTER TABLE `payments`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT de la tabla `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=195;

--
-- AUTO_INCREMENT de la tabla `products`
--
ALTER TABLE `products`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=63;

--
-- AUTO_INCREMENT de la tabla `product_orders`
--
ALTER TABLE `product_orders`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;

--
-- AUTO_INCREMENT de la tabla `product_reviews`
--
ALTER TABLE `product_reviews`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT de la tabla `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `slides`
--
ALTER TABLE `slides`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `uploads`
--
ALTER TABLE `uploads`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=49;

--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
