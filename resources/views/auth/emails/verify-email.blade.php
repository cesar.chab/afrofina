Gracias por registrarse

Para poder activar tu cuenta, necesitamos comprobar tu dirección de correo. Para ello, pincha en el botón siguiente:<br>


Activar mi cuenta


Si el enlace no funciona, copia esta dirección en tu buscador:<br>
{{$url}}

Thanks,<br>
{{ config('app.name') }}

