<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Afroeat</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:300,400,400italic,600,600italic,700,700italic' rel='stylesheet' type='text/css' />
    <style type="text/css">
        .ExternalClass,
        .ExternalClass p,
        .ExternalClass span,
        .ExternalClass font,
        .ExternalClass td,
        .ExternalClass div {
            line-height: 100%;
        }

        body {
            -webkit-text-size-adjust: none;
            -ms-text-size-adjust: none;
        }

        body,
        img,
        div,
        p,
        ul,
        li,
        span,
        strong,
        a {
            margin: 0;
            padding: 0;
        }

        table {
            border-spacing: 0;
        }

        table td {
            border-collapse: collapse;
        }

        body,
        #body_style {
            width: 100% !important;
            color: #888888;
            font-size: 11px;
            line-height: 1;
            font-family: 'Open Sans', sans-serif;
            font-weight: 400;
            background: #e3e7e9;
        }

        a {
            color: #888888;
            text-decoration: none;
            outline: none;
        }

        a:link {
            color: #888888;
            text-decoration: none;
        }

        a:visited {
            color: #888888;
            text-decoration: none;
        }

        a:hover {
            text-decoration: none !important;
        }

        a[href^="tel"],
        a[href^="sms"] {
            text-decoration: none;
            color: #888888 !important;
            pointer-events: none;
            cursor: default;
        }

        img {
            border: none !important;
            outline: none !important;
            text-decoration: none;
            height: auto;
            max-width: 100%;
            display: block;
        }

        a {
            border: none !important;
            outline: none !important;
        }

        table {
            border-collapse: collapse;
            mso-table-lspace: 0pt;
            mso-table-rspace: 0pt;
        }

        tr,
        td {
            margin: 0;
            padding: 0;
        }

        @media screen and (max-width: 679px) {
            body[yahoo] .wrapper {
                width: 100% !important;
            }

            body[yahoo] .device-width {
                width: 100% !important;
            }

            body[yahoo] .device-gutter {
                width: 15px !important;
            }

            body[yahoo] .height-100 {
                height: 100px !important;
            }

            body[yahoo] .img-height {
                width: 100% !important;
                height: auto !important;
            }

            body[yahoo] .txt-center td {
                text-align: center !important;
            }

            body[yahoo] .font-40 {
                font-size: 40px !important;
            }

            body[yahoo] .font-32 {
                font-size: 32px !important;
            }

            body[yahoo] .font-25 {
                font-size: 25px !important;
            }

            body[yahoo] .font-18 {
                font-size: 18px !important;
            }

            body[yahoo] .font-16 {
                font-size: 16px !important;
            }
        }

        @media screen and (max-width: 360px) {
            body[yahoo] .device-width-360 {
                width: 100% !important;
            }

            body[yahoo] .margin-btm-15 {
                margin-bottom: 15px;
            }

            body[yahoo] .gutter-5 {
                width: 5px !important;
            }
        }

    </style>

</head>
<body style="width:100% !important; color:#888888; font-family: 'Open Sans', sans-serif, Arial; font-weight: 400; font-size:11px; line-height:1;" alink="#4a4a4a" link="#4a4a4a" text="#4a4a4a" yahoo="fix">
    <br> <br><br>

    <!-- top headerCntr-->

    <!-- top radius -->
    <table width="100%" border="0" cellpadding="0" cellspacing="0" align="center">
        <tr>
            <td>
                <table cellpadding="0" cellspacing="0" border="0" width="680" align="center" class="wrapper">
                    <tr>
                        <td>
                            <table cellpadding="0" cellspacing="0" border="0" width="100%" style="border-radius: 5px 5px 0 0; background: #ffffff;">
                                <tr>
                                    <td height="20"><img border="0" src="https://app.afrofinaeat.com/public/images/template-email/spacer.gif" alt="" width="1" height="1" /></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <!-- top radius -->

    <!-- logoBox -->
    <table width="100%" border="0" cellpadding="0" cellspacing="0" align="center">
        <tr>
            <td>
                <table cellpadding="0" cellspacing="0" border="0" width="680" align="center" class="wrapper">
                    <tr>
                        <td>
                            <table cellpadding="0" cellspacing="0" border="0" width="100%" style="background: #ffffff;">
                                <tr>
                                    <td width="40" class="device-gutter">&nbsp;</td>
                                    <td>
                                        <table cellpadding="0" cellspacing="0" border="0" width="100%" style="border-bottom: 1px solid #e7e7e7;">
                                            <tr>
                                                <td height="10"><img border="0" src="https://app.afrofinaeat.com/public/images/template-email/spacer.gif" alt="" width="1" height="1" /></td>
                                            </tr>
                                            <tr>
                                                <td align="center">
                                                    <table cellpadding="0" cellspacing="0" border="0" align="center">
                                                        <tr>
                                                            <td align="center">
                                                                <a href="#" style="display: block;"><img border="0" src="https://app.afrofinaeat.com/public/images/template-email/logo.png" alt="" style="display: block;" /></a>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td height="13"><img border="0" src="https://app.afrofinaeat.com/public/images/template-email/spacer.gif" alt="" width="1" height="1" /></td>
                                            </tr>

                                            <tr>
                                                <td height="27"><img border="0" src="https://app.afrofinaeat.com/public/images/template-email/spacer.gif" alt="" width="1" height="1" /></td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td width="40" class="device-gutter">&nbsp;</td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <!-- logoBox -->

    <!-- welcomeBox -->
    <table width="100%" border="0" cellpadding="0" cellspacing="0" align="center">
        <tr>
            <td>
                <table cellpadding="0" cellspacing="0" border="0" width="680" align="center" class="wrapper">
                    <tr>
                        <td>
                            <table cellpadding="0" cellspacing="0" border="0" width="100%" style="background: #ffffff;">
                                <tr>
                                    <td width="40" class="device-gutter">&nbsp;</td>
                                    <td align="center">
                                        <table cellpadding="0" cellspacing="0" border="0" width="500" align="center" class="device-width">
                                            <tr>
                                                <td height="42"><img border="0" src="https://app.afrofinaeat.com/public/images/template-email/spacer.gif" alt="" width="1" height="1" /></td>
                                            </tr>
                                            <tr>
                                                <td align="center" style="padding: 0px; margin: 0px; font-family: 'Open Sans', sans-serif; line-height: 1.2; font-weight: 400; color: #4C913C; font-size: 50px; letter-spacing: -0.02em;" class="font-32">¡Thank you for registering with Afro Eat! </td>
                                            </tr>
                                            <tr>
                                                <td height="12"><img border="0" src="https://app.afrofinaeat.com/public/images/template-email/spacer.gif" alt="" width="1" height="1" /></td>
                                            </tr>
                                            <tr>
                                                <td align="center" style="padding: 0px; margin: 0px; font-family: 'Open Sans', sans-serif; line-height: 1.5; font-weight: 400; color: #4C913C; font-size: 20px;" class="font-18">You need to activate your account to log in</td>
                                            </tr>
                                            <tr>
                                                <td align="center">
                                                    <table cellspacing="0" cellpadding="0" border="0" align="center" style="background: #4c913c; border-radius: 5px;">
                                                        <tr>
                                                            <td height="14"><img border="0" width="1" height="1" alt="" src="https://app.afrofinaeat.com/public/images/template-email/spacer.gif" /></td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <table cellspacing="0" cellpadding="0" border="0" width="100%">
                                                                    <tbody>
                                                                        <tr>
                                                                            <td width="30">&nbsp;</td>
                                                                            <td style="padding: 0px; margin: 0px;"><a style="font-family: 'Open Sans', sans-serif; line-height: 1.2; font-weight: 700; color: #ffffff; font-size: 16px; text-transform: uppercase; text-decoration: none;"  href="{{url('register/verify/'.$code)}}">Activate account</a></td>
                                                                            <td width="30">&nbsp;</td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td height="14"><img border="0" width="1" height="1" alt="" src="https://app.afrofinaeat.com/public/images/template-email/spacer.gif" /></td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        <tr>
                                                <td height="30"><img border="0" src="https://app.afrofinaeat.com/public/images/template-email/spacer.gif" alt="" width="1" height="1" /></td>
                                            </tr>

                                            <tr>
                                                <td height="15"><img border="0" src="https://app.afrofinaeat.com/public/images/template-email/spacer.gif" alt="" width="1" height="1" /></td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td width="40" class="device-gutter">&nbsp;</td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <!-- welcomeBox -->

    <!-- appBox -->
    <table width="100%" border="0" cellpadding="0" cellspacing="0" align="center">
        <tr>
            <td>
                <table cellpadding="0" cellspacing="0" border="0" width="680" align="center" class="wrapper">
                    <tr>
                        <td>
                            <table cellpadding="0" cellspacing="0" border="0" width="100%" style="background-repeat: no-repeat; background-position: center center; background-size: 100% 100%; background-color: #4C913C;">
                                <tr>
                                    <td>
                                        <img width="100%" src="https://app.afrofinaeat.com/public/images/template-email/bg-img1.jpg" alt="">
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <table cellpadding="0" cellspacing="0" border="0" width="680" align="center" class="wrapper">
                                            <tr>
                                                <td width="40" class="device-gutter">&nbsp;</td>
                                                <td align="center">
                                                    <table cellpadding="0" cellspacing="0" border="0" width="500" align="center" class="device-width">
                                                        <tr>
                                                            <td height="176" class="height-100"><img border="0" src="https://app.afrofinaeat.com/public/images/template-email/spacer.gif" alt="" width="1" height="1" /></td>
                                                        </tr>
                                                        <tr>
                                                            <td align="center" style="padding: 0px; margin: 0px; font-family: 'Open Sans', sans-serif; line-height: 1.2; font-weight: 800; color: #ffffff; font-size: 65px; letter-spacing: -0.04em;" class="font-40">Afroeat Apps</td>
                                                        </tr>
                                                        <tr>
                                                            <td height="10"><img border="0" src="https://app.afrofinaeat.com/public/images/template-email/spacer.gif" alt="" width="1" height="1" /></td>
                                                        </tr>
                                                        <tr>
                                                            <td align="center" style="padding: 0px; margin: 0px; font-family: 'Open Sans', sans-serif; line-height: 1.2; font-weight: 600; color: #ffffff; text-transform: uppercase; font-size: 32px;" class="font-25">Watch how it works</td>
                                                        </tr>
                                                        <tr>
                                                            <td height="13"><img border="0" src="https://app.afrofinaeat.com/public/images/template-email/spacer.gif" alt="" width="1" height="1" /></td>
                                                        </tr>
                                                        <tr>
                                                            <td align="center"><a href="https://vimeo.com/476600946"><img border="0" src="https://app.afrofinaeat.com/public/images/template-email/play-1.png" alt="" style="display: block;" /></a></td>
                                                        </tr>
                                                        <tr>
                                                            <td height="136" class="height-100"><img border="0" src="https://app.afrofinaeat.com/public/images/template-email/spacer.gif" alt="" width="1" height="1" /></td>
                                                        </tr>
                                                    </table>
                                                </td>
                                                <td width="40" class="device-gutter">&nbsp;</td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <!-- appBox -->

    <!-- featureBox -->
    <table width="100%" border="0" cellpadding="0" cellspacing="0" align="center">
        <tr>
            <td>
                <table cellpadding="0" cellspacing="0" border="0" width="680" align="center" class="wrapper">
                    <tr>
                        <td>
                            <table cellpadding="0" cellspacing="0" border="0" width="100%" style="background: #ffffff;">
                                <tr>
                                    <td width="40" class="device-gutter">&nbsp;</td>
                                    <td align="center">
                                        <table cellpadding="0" cellspacing="0" border="0" width="100%" align="center" class="device-width">
                                            <tr>
                                                <td height="10"><img border="0" src="https://app.afrofinaeat.com/public/images/template-email/spacer.gif" alt="" width="1" height="1" /></td>
                                            </tr>
                                            <tr>
                                                <td align="center" style="padding: 0px; margin: 0px; font-family: 'Open Sans', sans-serif; line-height: 1.2; font-weight: 400; color: #4C913C; font-size: 24px; text-transform: uppercase; letter-spacing: 0.05em;">Join the afroeat team </td>
                                            </tr>
                                            <tr>
                                                <td height="5"><img border="0" src="https://app.afrofinaeat.com/public/images/template-email/spacer.gif" alt="" width="1" height="1" /></td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td width="40" class="device-gutter">&nbsp;</td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>

    <table width="100%" border="0" cellpadding="0" cellspacing="0" align="center">
        <tr>
            <td>
                <table cellpadding="0" cellspacing="0" border="0" width="680" align="center" class="wrapper">
                    <tr>
                        <td>
                            <table cellpadding="0" cellspacing="0" border="0" width="100%" style="background: #ffffff;">
                                <tr>
                                    <td width="40" class="device-gutter">&nbsp;</td>
                                    <td align="center">
                                        <table cellpadding="0" cellspacing="0" border="0" width="100%" align="center" class="device-width">
                                            <tr>
                                                <td height="22"><img border="0" src="https://app.afrofinaeat.com/public/images/template-email/spacer.gif" alt="" width="1" height="1" /></td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <table align="left" cellpadding="0" cellspacing="0" border="0" width="48%" class="device-width-360 margin-btm-15">
                                                        <tr>
                                                            <td><img border="0" src="https://app.afrofinaeat.com/public/images/template-email/img3.jpg" alt="" style="display: block; width: 100%;" /></td>
                                                        </tr>
                                                        <tr>
                                                            <td height="15"><img border="0" src="https://app.afrofinaeat.com/public/images/template-email/spacer.gif" alt="" width="1" height="1" /></td>
                                                        </tr>
                                                        <tr>
                                                            <td align="center" style="padding: 0px; margin: 0px; font-family: 'Open Sans', sans-serif; line-height: 1.2; font-weight: 600; color: #4C913C; font-size: 19px;">Become A Rider</td>
                                                        </tr>
                                                        <tr>
                                                            <td height="12"><img border="0" src="https://app.afrofinaeat.com/public/images/template-email/spacer.gif" alt="" width="1" height="1" /></td>
                                                        </tr>
                                                        <tr>
                                                            <td align="center" style="padding: 0px; margin: 0px; font-family: 'Open Sans', sans-serif; line-height: 1.6; font-weight: 400; color: #888888; font-size: 14px;">Be your own boss and take a leap to make some extra cash delivering meals & groceries with your bicycle, motor, or vehicle around your neighborhood and other places.</td>
                                                        </tr>
                                                    </table>
                                                    <table align="right" cellpadding="0" cellspacing="0" border="0" width="48%" class="device-width-360 margin-btm-15">
                                                        <tr>
                                                            <td><img border="0" src="https://app.afrofinaeat.com/public/images/template-email/img4.jpg" alt="" style="display: block; width: 100%;" /></td>
                                                        </tr>
                                                        <tr>
                                                            <td height="15"><img border="0" src="https://app.afrofinaeat.com/public/images/template-email/spacer.gif" alt="" width="1" height="1" /></td>
                                                        </tr>
                                                        <tr>
                                                            <td align="center" style="padding: 0px; margin: 0px; font-family: 'Open Sans', sans-serif; line-height: 1.2; font-weight: 600; color: #4C913C; font-size: 19px;">Become A Vendor</td>
                                                        </tr>
                                                        <tr>
                                                            <td height="12"><img border="0" src="https://app.afrofinaeat.com/public/images/template-email/spacer.gif" alt="" width="1" height="1" /></td>
                                                        </tr>
                                                        <tr>
                                                            <td align="center" style="padding: 0px; margin: 0px; font-family: 'Open Sans', sans-serif; line-height: 1.6; font-weight: 400; color: #888888; font-size: 14px;">Run your restaurant or grocery shop business at the comfort of your home using our apps & reach out to thousands of potential clients around you ready to purchase your products .</td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td height="5"><img border="0" src="https://app.afrofinaeat.com/public/images/template-email/spacer.gif" alt="" width="1" height="1" /></td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td width="40" class="device-gutter">&nbsp;</td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <table width="100%" border="0" cellpadding="0" cellspacing="0" align="center">
        <tr>
            <td>
                <table cellpadding="0" cellspacing="0" border="0" width="680" align="center" class="wrapper">
                    <tr>
                        <td>
                            <table cellpadding="0" cellspacing="0" border="0" width="100%" style="background: #ffffff;">
                                <tr>
                                    <td width="40" class="device-gutter">&nbsp;</td>
                                    <td align="center">
                                        <table cellpadding="0" cellspacing="0" border="0" width="100%" align="center" class="device-width">
                                            <tr>
                                                <td height="22"><img border="0" src="https://app.afrofinaeat.com/public/images/template-email/spacer.gif" alt="" width="1" height="1" /></td>
                                            </tr>
                                            <tr>
                                                <td align="center">
                                                    <table cellspacing="0" cellpadding="0" border="0" align="center" style="background: #4c913c; border-radius: 5px;">
                                                        <tr>
                                                            <td height="14"><img border="0" width="1" height="1" alt="" src="https://app.afrofinaeat.com/public/images/template-email/spacer.gif" /></td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <table cellspacing="0" cellpadding="0" border="0" width="100%">
                                                                    <tbody>
                                                                        <tr>
                                                                            <td width="30">&nbsp;</td>
                                                                            <td style="padding: 0px; margin: 0px;"><a style="font-family: 'Open Sans', sans-serif; line-height: 1.2; font-weight: 700; color: #ffffff; font-size: 16px; text-transform: uppercase; text-decoration: none;" href="#">Get Started</a></td>
                                                                            <td width="30">&nbsp;</td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td height="14"><img border="0" width="1" height="1" alt="" src="https://app.afrofinaeat.com/public/images/template-email/spacer.gif" /></td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td width="40" class="device-gutter">&nbsp;</td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <!-- featureBox -->

    <!-- empBox -->
    <table width="100%" border="0" cellpadding="0" cellspacing="0" align="center">
        <tr>
            <td>
                <table cellpadding="0" cellspacing="0" border="0" width="680" align="center" class="wrapper">
                    <tr>
                        <td>
                            <table cellpadding="0" cellspacing="0" border="0" width="100%" style="background-repeat: no-repeat; background-position: center bottom; background-size: 100% auto; background-color: #4C913C;">
                                <tr>
                                    <td><img width="100%" src="https://app.afrofinaeat.com/public/images/template-email/triangle-top-blank.png" alt=""></td>
                                </tr>
                                <tr>
                                    <td><img border="0" src="https://app.afrofinaeat.com/public/images/template-email/triangle-bottom.png" alt="" style="display: block; width: 100%;" /></td>
                                </tr>
                                <tr>
                                    <td>
                                        <table width="100%" border="0" cellpadding="0" cellspacing="0" align="center">
                                            <tr>
                                                <td width="40" class="device-gutter">&nbsp;</td>
                                                <td align="center">
                                                    <table cellpadding="0" cellspacing="0" border="0" align="center" class="device-width">
                                                        <tr>
                                                            <td height="35"><img border="0" src="https://app.afrofinaeat.com/public/images/template-email/spacer.gif" alt="" width="1" height="1" /></td>
                                                        </tr>
                                                        <tr>
                                                            <td align="center" style="padding: 0px; margin: 0px; font-family: 'Open Sans', sans-serif; line-height: 1.2; font-weight: 400; color: #ffffff; font-size: 42px; letter-spacing: -0.03em;" class="font-32">Empower your Business!</td>
                                                        </tr>
                                                        <tr>
                                                            <td height="12"><img border="0" src="https://app.afrofinaeat.com/public/images/template-email/spacer.gif" alt="" width="1" height="1" /></td>
                                                        </tr>
                                                        <tr>
                                                            <td align="center" style="padding: 0px; margin: 0px; font-family: 'Open Sans', sans-serif; line-height: 1.5; font-weight: 400; color: #ffffff; font-size: 20px;" class="font-18"> Reach out to thousands of potential clients around you and earn from the comfort of your home.</td>
                                                        </tr>
                                                        <tr>
                                                            <td height="18"><img border="0" src="https://app.afrofinaeat.com/public/images/template-email/spacer.gif" alt="" width="1" height="1" /></td>
                                                        </tr>
                                                    </table>
                                                </td>
                                                <td width="40" class="device-gutter">&nbsp;</td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <table width="100%" border="0" cellpadding="0" cellspacing="0" align="center">
                                            <tr>
                                                <td align="center">
                                                    <table cellpadding="0" cellspacing="0" border="0" align="center" width="100%" class="device-width">
                                                        <tr>
                                                            <td><img border="0" src="https://app.afrofinaeat.com/public/images/template-email/img3.png" alt="" style="display: block; width: 100%;" width="680" height="408" class="img-height" /></td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <!-- empBox -->

    <!-- iconBox -->

    <!-- iconBox -->

    <!-- playBox -->
    <table width="100%" border="0" cellpadding="0" cellspacing="0" align="center">
        <tr>
            <td>
                <table cellpadding="0" cellspacing="0" border="0" width="680" align="center" class="wrapper">
                    <tr>
                        <td>
                            <table cellpadding="0" cellspacing="0" border="0" width="100%" style="background: #ffffff;">
                                <tr>
                                    <td width="40" class="device-gutter">&nbsp;</td>
                                    <td align="center">
                                        <table cellpadding="0" cellspacing="0" border="0" align="center" width="100%" class="device-width">
                                            <tr>
                                                <td height="10"><img border="0" src="https://app.afrofinaeat.com/public/images/template-email/spacer.gif" alt="" width="1" height="1" /></td>
                                            </tr>
                                            <br><br><br><br>

                                            <tr>
                                                <td align="center">
                                                    <table cellpadding="0" cellspacing="0" border="0" align="center">
                                                        <tr>
                                                            <td align="center"><img border="0" src="https://app.afrofinaeat.com/public/images/template-email/google-play.png" alt="" style="display: block; width: 100%;" /></td>
                                                            <td align="center" width="10" class="gutter-5"><img border="0" src="https://app.afrofinaeat.com/public/images/template-email/spacer.gif" alt="" width="1" height="1" /></td>
                                                            <td align="center"><img border="0" src="https://app.afrofinaeat.com/public/images/template-email/app-store.png" alt="" style="display: block; width: 100%;" /></td>
                                                            <td align="center" width="10" class="gutter-5"><img border="0" src="https://app.afrofinaeat.com/public/images/template-email/spacer.gif" alt="" width="1" height="1" /></td>

                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td height="35"><img border="0" src="https://app.afrofinaeat.com/public/images/template-email/spacer.gif" alt="" width="1" height="1" /></td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td width="40" class="device-gutter">&nbsp;</td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <!-- iconBox -->

    <!-- prifileBox -->
    <table width="100%" border="0" cellpadding="0" cellspacing="0" align="center">
        <tr>
            <td>
                <table cellpadding="0" cellspacing="0" border="0" width="680" align="center" class="wrapper">
                    <tr>
                        <td>
                            <table cellpadding="0" cellspacing="0" border="0" width="100%" style="background: #ffffff;">
                                <tr>
                                    <td width="40" class="device-gutter">&nbsp;</td>
                                    <td align="center">
                                        <table cellpadding="0" cellspacing="0" border="0" align="center" width="100%" class="device-width">
                                            <tr>
                                                <td height="10"><img border="0" src="https://app.afrofinaeat.com/public/images/template-email/spacer.gif" alt="" width="1" height="1" /></td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <table cellpadding="0" cellspacing="0" border="0" align="center">
                                                        <tr>
                                                            <td>
                                                                <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                                    <tr>
                                                                        <td align="center" style="padding: 0px; margin: 0px; font-family: 'Open Sans', sans-serif; line-height: 1.4; font-weight: 200; font-style: bold; color: #4C913C; font-size: 17px;" class="font-18">Download the afroeat apps today & enjoy the freedom of shopping groceries and food items right from the comfort of your home whiles still being able to earn vending with us. </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td height="22"><img border="0" src="https://app.afrofinaeat.com/public/images/template-email/spacer.gif" alt="" width="1" height="1" /></td>
                                                        </tr>
                                                        <tr>
                                                            <td align="center">

                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td height="40"><img border="0" src="https://app.afrofinaeat.com/public/images/template-email/spacer.gif" alt="" width="1" height="1" /></td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td width="40" class="device-gutter">&nbsp;</td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <!-- prifileBox -->

    <!-- wideBox -->
    <table width="100%" border="0" cellpadding="0" cellspacing="0" align="center">
        <tr>
            <td>
                <table cellpadding="0" cellspacing="0" border="0" width="680" align="center" class="wrapper">
                    <tr>
                        <td>
                            <table cellpadding="0" cellspacing="0" border="0" width="100%" style="background: #4C913C; border-radius: 0 0 5px 5px;">
                                <tr>
                                    <td width="40" class="device-gutter">&nbsp;</td>
                                    <td align="center">
                                        <table cellpadding="0" cellspacing="0" border="0" align="center" width="100%" class="device-width">
                                            <tr>
                                                <td height="20"><img border="0" src="https://app.afrofinaeat.com/public/images/template-email/spacer.gif" alt="" width="1" height="1" /></td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <table cellpadding="0" cellspacing="0" border="0" width="100%" align="center">
                                                        <tr>
                                                            <td>
                                                                <table cellpadding="0" cellspacing="0" border="0" align="left" class="device-width txt-center">
                                                                    <tr>
                                                                        <td height="7"><img border="0" src="https://app.afrofinaeat.com/public/images/template-email/spacer.gif" alt="" width="1" height="1" /></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="padding: 0px; margin: 0px; font-family: 'Open Sans', sans-serif; line-height: 1.4; font-weight: 400; color: #ffffff; font-size: 19px;">Over <b style="color: #fffff; font-weight: 600;">5000+ downloads</b> worldwide.</td>
                                                                    </tr>
                                                                </table>
                                                                <table cellpadding="0" cellspacing="0" border="0" align="left" width="30" class="device-width">
                                                                    <tr>
                                                                        <td height="20"><img border="0" src="https://app.afrofinaeat.com/public/images/template-email/spacer.gif" alt="" width="1" height="1" /></td>
                                                                    </tr>
                                                                </table>
                                                                <table cellpadding="0" cellspacing="0" border="0" align="right" class="device-width">
                                                                    <tr>
                                                                        <td>
                                                                            <table cellspacing="0" cellpadding="0" border="0" align="center" style="background: #ffffff; border-radius: 5px;">
                                                                                <tr>
                                                                                    <td height="9"><img border="0" width="1" height="1" src="https://app.afrofinaeat.com/public/images/template-email/spacer.gif" alt="" /></td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        <table cellspacing="0" cellpadding="0" border="0" width="100%">
                                                                                            <tr>
                                                                                                <td width="20">&nbsp;</td>
                                                                                                <td style="padding: 0px; margin: 0px;"><a href="#" style="font-family: 'Open Sans', sans-serif; line-height: 1.2; font-weight: 700; color: #4C913C; font-size: 19px; text-transform: uppercase; text-decoration: none;">Get Started</a></td>
                                                                                                <td width="20">&nbsp;</td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td height="9"><img border="0" width="1" height="1" src="https://app.afrofinaeat.com/public/images/template-email/spacer.gif" alt="" /></td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td height="20"><img border="0" src="https://app.afrofinaeat.com/public/images/template-email/spacer.gif" alt="" width="1" height="1" /></td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td width="40" class="device-gutter">&nbsp;</td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>

    <br> <br><br>
    <!-- wideBox -->

    <!-- footerCntr -->

    <!-- footerCntr -->

</body>
</html>
