<?php 

return [
    'agree' => 'Ich stimme den Bedingungen zu',
    'already_member' => 'Ich habe bereits eine Mitgliedschaft',
    'email' => 'E-Mail-Addresse',
    'failed' => 'Diese Anmeldeinformationen stimmen nicht mit unseren Unterlagen überein.',
    'forgot_password' => 'Ich habe mein Passwort vergessen',
    'login' => 'Anmeldung',
    'login_facebook' => 'Melden Sie sich mit Facebook an',
    'login_google' => 'Mit Google+ anmelden',
    'login_title' => 'Melden Sie sich an, um Ihre Sitzung zu beginnen',
    'login_twitter' => 'Mit Twitter anmelden',
    'logout' => 'Ausloggen',
    'name' => 'Vollständiger Name',
    'password' => 'Passwort',
    'password_confirmation' => 'Bestätigen Sie Ihr Passwort',
    'register' => 'Einloggen',
    'register_new_member' => 'Registrieren Sie eine neue Mitgliedschaft',
    'remember_me' => 'Erinnere dich an mich',
    'remember_password' => 'Ich erinnere mich an mein Passwort, um zum Login zurückzukehren',
    'reset_password' => 'Zurücksetzen',
    'reset_password_title' => 'Gib dein neues Passwort ein',
    'reset_title' => 'E-Mail zum Zurücksetzen des Passworts',
    'send_password' => 'Link zum Zurücksetzen des Passworts senden',
    'throttle' => 'Zu viele Anmeldeversuche. Bitte versuchen Sie es erneut in: Sekunden Sekunden.',
    'email_verified_at' => 'Sie müssen Ihr Konto bestätigen. Wir haben Ihnen eine Aktivierungs-E-Mail gesendet. Überprüfen Sie Ihre E-Mails!',
    'verification_failed' => 'Fehler beim Aktivieren des Kontos, versuchen Sie es später!',
    'verification_successed' => 'Kontoaktivität erfolgreich, können Sie sich bei der App anmelden',
    'account_activation' => 'Account Aktivierung',
    'send_registration_email' => 'Wir senden Ihnen eine E-Mail, um Ihr Konto zu validieren und zu aktivieren.',
    'mail_register_subject'  => 'Konto aktivieren',
    'mail_register_greeting'  => 'Vielen Dank für Ihre Registrierung bei ',
    'mail_register_line_middle'  => 'Sie müssen Ihr Konto aktivieren, um sich anzumelden',
    'mail_register_line_footer'  => "",
    'mail_register_footer_small' => "Wenn Sie Probleme beim Klicken auf die Schaltfläche 'Konto aktivieren' haben, kopieren Sie die folgende URL und fügen Sie sie in Ihren Webbrowser ein:",
    'mail_register_salutation' => "Schöne Grüße, ",
    "phone" => "Phone",
];
