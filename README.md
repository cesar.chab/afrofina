# Markets Laravel Admin Panel

## Important Links
 [Server Requirements](https://support.smartersvision.com/help-center/articles/7/9/3/introduction).
  
 [How to Update to last version?](https://support.smartersvision.com/help-center/articles/7/9/11/update).
 
 [FAQ](https://support.smartersvision.com/help-center/categories/8/laravel-application-faq).

## Twilio settings env

```TWILIO_AUTH_TOKEN=3d564ee23ba87ba9795772e3698bf3d2```

```TWILIO_ACCOUNT_SID=ACbde3185e2a1e98854b4e83914f99cb30```

```TWILIO_NUMBER=+16692383074```


 ## Twilio Format Number 
```Here’s the same phone number in E.164 formatting: +16692383074```

## Migrations
```php artisan migrate```

## Users
-- For twilio notifications to work, all users must have a phone number

## User register with api 
-- Routes 

-- Route::post('register', 'API\UserAPIController@register');

-- Route::get('register/verify/{code}', 'API\UserAPIController@verifyUser');

-- post register user /api/register: 

{    
    "name": "xxxxx"

    "email": "example@example.com"

    "password": "123456"
}

## Active account
-- To activate the account, follow the link sent to the email and it is automatically redirected to the app login

--get url  active account /api/verify/{code}:

 
