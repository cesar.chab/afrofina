<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Repositories\RoleRepository;
use App\Repositories\UploadRepository;
use App\Repositories\UserRepository;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Laravel\Socialite\Facades\Socialite;
use Prettus\Validator\Exceptions\ValidatorException;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';
    private $userRepository;
    private $uploadRepository;
    private $roleRepository;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(UserRepository $userRepository, UploadRepository $uploadRepository, RoleRepository $roleRepository)
    {
        $this->middleware('guest')->except('logout');
        $this->userRepository = $userRepository;
        $this->uploadRepository = $uploadRepository;
        $this->roleRepository = $roleRepository;
    }

    /**
     * Redirect the user to the Facebook authentication page.
     *
     * @return \Illuminate\Http\Response
     */
    public function redirectToProvider($service)
    {
        return Socialite::driver($service)->redirect();
    }

    /**
     * Obtain the user information from Facebook.
     *
     * @return \Illuminate\Http\Response
     */
    public function handleProviderCallback($service)
    {
        $userSocial = Socialite::driver($service)->user();
        $user = User::where('email',$userSocial->email)->first();
        if(!$user){
            $user = new User;
            $user->name = $userSocial->name;
            $user->email = $userSocial->email;
            $user->password = bcrypt(str_random());
            $user->save();
            $defaultRoles = $this->roleRepository->findByField('default','1');
            $defaultRoles = $defaultRoles->pluck('name')->toArray();
            $user->assignRole($defaultRoles);

            try {
                $upload = $this->uploadRepository->create(['uuid' => $userSocial->token]);
                $upload->addMediaFromUrl($userSocial->avatar_original)
                    ->withCustomProperties(['uuid' => $userSocial->token])
                    ->toMediaCollection('avatar');

                $cacheUpload = $this->uploadRepository->getByUuid($userSocial->token);
                $mediaItem = $cacheUpload->getMedia('avatar')->first();
                $mediaItem->copy($user, 'avatar');
            } catch (ValidatorException $e) {
                Flash::error($e->getMessage());
            }
        }     
        auth()->login($user,true);
        return redirect(route('users.profile'));
    }

    protected function authenticated(Request $request, $user) {
        if($user->roles[0]->id === 3 || $user->roles[0]->id === 4 || $user->roles[0]->id === 5) {
            if (!$user->email_verified_at) {                
                Auth::logout($request);
                session()->flash('error', trans('auth.email_verified_at'));
                return redirect('/login');
            }
        }
   }

    public function verifyUser ($code) {
        $user = User::where('api_token', $code)->first();
        if (!$user) {
            session()->flash('error', trans('auth.verification_failed'));
            return redirect('/')->with('error', trans('auth.verification_failed'));
        }
        $user->email_verified_at = date('Y-m-d H:i:s');
        $user->save();
        session()->flash('success', trans('auth.verification_successed'));
        return redirect('/login');
    }
}
