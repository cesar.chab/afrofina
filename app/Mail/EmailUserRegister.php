<?php

namespace App\Mail;

use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class EmailUserRegister extends Mailable
{
    use Queueable, SerializesModels;

    public $user;
    public $code;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(User $user, $code)
    {
        $this->user = $user;
        $this->code = $code;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $subject = "Account activation";
        $from = setting('mail_username');
        $name = setting('mail_from_name');
        return $this->to($this->user)->subject($subject)->from($from, $name)->
        markdown('auth.emails.register',['code' => $this->code,'user' => $this->user]);
        // return $this->markdown('auth.emails.register', ['url' => $this->verifyUrl, 'user' => $this->user]);
    }
}
