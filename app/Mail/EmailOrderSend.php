<?php

namespace App\Mail;

use App\Models\Order;
use App\Repositories\OrderRepository;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class EmailOrderSend extends Mailable
{
    use Queueable, SerializesModels;

    /** @var  OrderRepository */
    private $orderRepository;

    public $subject = 'Order';
    public $order;
    public $payment;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Order $order, OrderRepository $orderRepo, $payment)
    {
        $this->order = $order;
        $this->orderRepository = $orderRepo;
        $this->payment = $payment;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $order = $this->orderRepository->findWithoutFail($this->order->id);
        $subtotal = 0;
        $taxAmount = 0;
        $total = 0;
        
        foreach ($order->productOrders as $productOrder) {
            foreach ($productOrder->options as $option) {
                $productOrder['price'] += $option['price'];
            }
            $subtotal += $productOrder['price'] * $productOrder['quantity'];
        }

        $total = $subtotal + $order['delivery_fee'];
        $taxAmount = $total * $order['tax'] / 100;
        $total += $taxAmount;

        return $this->markdown('orders.emails.order', [
            "order" => $order, 
            "payment" => $this->payment, 
            "total" => $total, 
            "subtotal" => $subtotal, 
            "taxAmount" => $taxAmount
        ]);
    }
}
